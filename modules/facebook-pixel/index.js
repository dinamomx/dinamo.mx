const { resolve } = require('path')
/** @type {import('@nuxt/types').Module} */
function facebookModule(moduleOptions) {
  const options = this.options['facebook-pixel'] || moduleOptions

  this.addPlugin({
    src: resolve(__dirname, './templates/plugin.template.js'),
    fileName: 'facebook-pixel.js',
    options,
    ssr: false,
  })
}

module.exports = facebookModule
