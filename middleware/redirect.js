/* eslint-disable no-console */
const popularPages = [
  {
    path: /^\/diseno-la-evolucion-del-escudo-del-club-america/,
    year: 2015,
  },
  {
    path: /^\/10-datos-interesantes-sobre-la-psicologia-del-color/,
    year: 2016,
  },
  {
    path: /^\/tipografia-la-historia-de-la-fuente-arial/,
    year: 2015,
  },
  {
    path: /^\/los-comerciales-de-gandhi-que-participaran-en-cannes/,
    year: 2015,
  },
  {
    path: /^\/la-influencia-de-salvador-dali-en-la-publicidad/,
    year: 2015,
  },
  {
    path: /^\/las-marcas-de-la-liga-mx/,
    year: 2015,
  },
]
/**
 * Función que redirecciona el
 *
 * @type {import('@nuxt/types').ServerMiddleware}
 */
const redirectMiddleware = (request, response, next) => {
  if (!request.url) {
    // Si no tenemos url que parece ser posible pues no hacemos nada
    // y dejamos que la cadena de middleware siga
    next()
    return
  }
  // Fragmentamos la url en [WHATWG](https://nodejs.org/api/url.html#url_the_whatwg_url_api)
  const url = new URL(request.url, 'https://www.dinamo.mx')
  // Buscamos en la lista de redirecciones si la url actual aplica para alguna
  const findedPage = popularPages.find((page) => url.pathname.match(page.path))
  if (findedPage) {
    // Si tenemos alguna coincidencia de redirección la aplicamos
    console.info(`Redirecting blogpost ${url.pathname}`)
    // Rompemos la cadena de middleware y escribimos la cabezera de redirección con el
    // código HTTP de redirección permanente y la url absoluta a la cual redirigir
    response.writeHead(301, {
      Location: `${url.origin}/a113/${findedPage.year}${url.pathname}`,
    })
    response.end()
    return
  }
  // Si no encontramos coincidencia permitimos que la petición siga su curso
  next()
}

export default redirectMiddleware
