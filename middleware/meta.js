/** @type {import('@nuxt/types').Middleware} */
const middlewareMeta = ({ store, route }) => {
  if (route.name !== store.state.route) {
    store.dispatch('metaChanged', {
      route: route.name,
      matched: route.meta,
      current: route.meta[route.meta.length - 1] || {},
    })
  }
}

export default middlewareMeta
