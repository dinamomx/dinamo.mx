import Vue from 'vue'
import VueGtag from 'vue-gtag'

/**
 * @type {import('@nuxt/types').Plugin}
 */
const vueGtag = ({ app, env }) => {
  Vue.use(
    VueGtag,
    {
      config: { id: env.gaId },
      /*
      bootstrap: false,
      disableScriptLoad: process.env.NODE_ENV !== 'production',
      */
    },
    app.router
  )
}

export default vueGtag
