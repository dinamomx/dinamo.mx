/** @type {import('@nuxt/types').Plugin} */
const detectWebpPlugin = ({ req, env }) => {
  if (process.static) {return}
  const acceptsWebp = req.headers.accept?.includes('image/webp') || false
  env.hasWebp = acceptsWebp
}

export default detectWebpPlugin
