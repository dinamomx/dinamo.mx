import LogRocket from 'logrocket'
LogRocket.init('wcwn2j/dinamomx')
LogRocket.getSessionURL(function (sessionURL) {
  window.ga?.('send', {
    hitType: 'event',
    eventCategory: 'LogRocket',
    eventAction: sessionURL,
  })
})
export const logRocket = LogRocket
