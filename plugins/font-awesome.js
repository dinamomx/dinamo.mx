import Vue from 'vue'

import { library, config } from '@fortawesome/fontawesome-svg-core'
import {
  faTimes,
  faPlus,
  faAngleDown,
  faMap,
  faEnvelope,
  faAngleUp,
  faFileInvoiceDollar,
  faPhone,
  faMinus,
  faAngleLeft,
  faAngleRight,
} from '@fortawesome/free-solid-svg-icons'
import {
  faFacebookMessenger,
  faFacebook,
  faWhatsapp,
  faTwitter,
  faLinkedin,
  faInstagram,
  faTelegram,
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

config.autoAddCss = false
config.showMissingIcons = true

library.add(
  faTimes,
  faPlus,
  faAngleDown,
  faMap,
  faEnvelope,
  faAngleUp,
  faAngleLeft,
  faAngleRight,
  faFileInvoiceDollar,
  faPhone,
  faMinus,
  faFacebookMessenger,
  faFacebook,
  faWhatsapp,
  faTwitter,
  faLinkedin,
  faInstagram,
  faTelegram
)

Vue.component('FaIcon', FontAwesomeIcon)
