import Vue from 'vue'
import ImageResponsive from '~/components/ImageResponsive.vue'
import Gallery from '~/components/Gallery.vue'
import CaseHero from '~/components/Portafolio/CaseHero.vue'
import CaseSection from '~/components/Portafolio/CaseSection.vue'
import CaseSlider from '~/components/Portafolio/CaseSlider.vue'
import CaseAccordion from '~/components/Portafolio/CaseAccordion.vue'
import NumberCounter from '~/components/Portafolio/NumberCounter.vue'
import VideoPlayer from '~/components/VideoPlayer.vue'

Vue.component(ImageResponsive.name, ImageResponsive)
Vue.component(Gallery.name, Gallery)
// Estos deberíamos encontrar una forma alternativa de importarlos para su contenido
Vue.component(CaseHero.name, CaseHero)
Vue.component(CaseSection.name, CaseSection)
Vue.component(CaseSlider.name, CaseSlider)
Vue.component(CaseAccordion.name, CaseAccordion)
Vue.component(NumberCounter.name, NumberCounter)
Vue.component(VideoPlayer.name, VideoPlayer)
