#!/bin/bash
# Script para evitar errores humanos al subir archivos al servidor
# Hay que definir variables
set -eu # Fail on errors or undeclared variables
printable_colours=256
RUTA_RAIZ="/var/www"
USUARIO="webdev"
SERVIDOR="droplet.dinamo.mx"
DOMINIO="dinamo.mx/source"
CARPETA=".nuxt/"
LOGFILE="rsync.log"

# Mostrando información relevante
printf "Los archivos se subirán a: \e[1;48;97;44m${USUARIO}@${SERVIDOR}:${RUTA_RAIZ}/${DOMINIO}\e[0m\n"
rsync -cuPa --chmod=Du=rwx,Dgo=rx,Fu=rw,Fog=r  --info=progress2 --cvs-exclude --exclude-from='./uploadignore.txt' --delete-after --log-file=$LOGFILE .nuxt $USUARIO@$SERVIDOR:$RUTA_RAIZ/$DOMINIO
rsync -cuPa --log-file=$LOGFILE static/sw.js $USUARIO@$SERVIDOR:$RUTA_RAIZ/$DOMINIO
