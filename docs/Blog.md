# Documentación para la sección de blog

> Esta documentación está en proceso y tiene como objetivo orientar en aspectos
> no discernibles desde el código.

## Tamaño de las imágenes

- `600x760` para el **listado de artículos**
- `1200x630` para **facebook**
- `1920x1080` (desktop) **Portada**
- `1440x900` (tablet) **Portada**
- `630x1120` (cel) **Portada**

Estos últimos 3 se tienen que llamar con cierta nomeclatura de nombres:

- desktop: `[nombre_de_imagen]_xl.[extensión]`
- tablet: `[nombre_de_imagen]_md.[extensión]`
- cel: `[nombre_de_imagen]_xs.[extensión]`

## Datos adicionales de los artículos

- Título
- Subtitulo (opcional)
- Título alternativo para SEO (Opcional)
- Resumen (El resumen que se muestra en el listado del blog y debe - ser
  alrededor de 300 carácteres, funciona como introducción para ciertos
  buscadores ayuda al ranking de "Focus Keyword")
- Descripción (Obligatorio, texto de 155-160 carácteres para resumen en
  buscadores, escencial para "Focus Keyworkd")
- keywords: (Obligatorio, una lista de palabras o frases que irían en
  potenciales búsquedas)
