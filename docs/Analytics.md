# Eventos de la página de dínamo

| Acción | Nombre del Evento | Categoría | Etiqueta |
| --- | --- | --- | --- |
| Click en el hero del index | click_cta | Navegation | Hero/Portafolio |
| Click en tajeta del index | click_card | Navegation | Portafolio/Diseño |
| Registro de newsletter | sign_up_newsletter | engagment | Footer/Index o  Footer/Index |
| Envío de formulario del botón cotiza que se encuentra en el  | generate_lead | engagment | Flotado/Index |
| Click en tajeta del index | generate_lead | engagment | Flotado/Index |
