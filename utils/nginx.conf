# Configuración base de nginx para uso en droplets de digitalocean
# Tiene ejemplos de uso para cuando es estático o cuando es dinámico
# Antes de usar esta configuración tienes que revisarla cuidadosamente

map $sent_http_content_type $expires {
    "text/html"                 epoch;
    "text/html; charset=utf-8"  epoch;
    default                     off;
}


server {
    ##################
    # General config #
    ##################

    listen 80;
    listen [::]:80;

    # Activar manualmente solo si se compra un certificado
    # listen 443 ssl http2;
    # ssl_certificate /etc/ssl/certs/server.crt;
    # ssl_certificate_key /etc/ssl/private/server.key;

    index index.html index.htm index.nginx-debian.html;

    # Domain name
    server_name nuxt.dinamo.mx www.nuxt.dinamo.mx;

    # Files root
    set $root_path /var/www/nuxt.dinamo.mx;
    root $root_path/source;
    # Logs
    access_log /var/www/nuxt.dinamo.mx/logs/access.log;
    error_log /var/www/nuxt.dinamo.mx/logs/error.log;
    autoindex off;

    # Rediección de no www a www
    if ($host = nuxt.dinamo.mx) {
        return 301 $scheme://www.nuxt.dinamo.mx$request_uri;
    }

    ###################################
    # Nuxt setup with nodejs #
    ###################################
    location @nuxt {
      proxy_pass http://localhost:3000;
    }

    location / {
        # Snippet or granular, you choose
        include snippets/proxy.conf;
        # Cache Setup (Optional)
        expires $expires;
        # Especifica el puerto
        proxy_pass http://localhost:3000;
    }

    # Intentar no pasar los
     location ~* ^.+\.(jpg|png|jpeg|gif|webp|mp4|pdf|svg|bmp|ico|apng|ogg|mp3|webm)$ {
        root $root_path/source/static;
        try_files $uri @nuxt;
    }

    location ~* ^/_nuxt/.+\.(jpg|png|jpeg|gif|webp|css|js|mp4|svg|apng|ogg|mp3|webm) {
        root $root_path/source/.nuxt/dist;
        try_files $uri @nuxt;
    }

    # Allow certbot to work
    location /.well-known {
      root $root_path/source/static;
    }

    # Never cache the service worker
    location = /sw.js {
      expires off;
      add_header Cache-Control no-cache;
      root $root_path/source/static;
    }


    ####################################
    # Static files setup (php or html) #
    ####################################

    # location / {
    #     # First attempt to serve request as file, then
    #     # as directory, then fall back to displaying a 404.
    #     try_files $uri $uri/ =404;
    #     # Usando laravel
    #     # try_files $uri $uri/ /index.php?$query_string;
    #     # Usando Nuxt en modo SPA
    #     # try_files $uri $uri/ /200.html?$query_string;
    # }

    # PHP Setup
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.1-fpm.sock;
    }

    # Para uso estático con nuxt
    # error_page 404 /200.html;
    # location = /200.html {
    #     root /var/www/plantilla.dinamo.mx/html;
    #     internal;
    # }

}
