/**
 * Usando opciones de clases y otros atributos
 * http://pandoc.org/MANUAL.html#extension-header_attributes
 */

// Usado en conjunto con markdown-it-attrs
/**
 * Merge attributes from markdownItAttrs.
 *
 * @param {Array.<string[]>} attrs - Array of attributes.
 * @param {string} defaultClass - La clase por defecto.
 *
 * @returns {string} The class attribute.
 */
const mergeAttributes = (attributes, defaultClass) => {
  if (!attributes) {
    return `class="${defaultClass}"`
  }
  let result = ''
  attributes.forEach((attribute) => {
    const [attributeName] = attribute
    const values = attribute.slice(1)
    if (attributeName === 'class') {
      // Clase por defecto
      values.push(defaultClass)
    }
    // Añadiendo un espacio si ya hay atributos
    if (result.length > 0) {
      result += ' '
    }
    result += `${attributeName}`
    // Si tiene valores el atribito, ponlos
    if (values.length > 0) {
      result += '="'
      result += values.join(' ')
      result += '"'
    }
  })
  return result
}

/** @typedef {{render: import('markdown-it').TokenRender}} SectionRenderer */
/** @type {SectionRenderer} */
const section = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttributes = ''
      stringAttributes = mergeAttributes(attrs, 'section')
      // Opening tag
      return `<section ${stringAttributes}>`
    }
    // closing tag
    return '</section>'
  },
}

/** @type {SectionRenderer} */
const column = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttributes = ''
      stringAttributes = mergeAttributes(attrs, 'column')
      // Opening tag
      return `<div ${stringAttributes}>`
    }
    // closing tag
    return '</div>'
  },
}

/** @type {SectionRenderer} */
const columns = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttributes = ''
      stringAttributes = mergeAttributes(attrs, 'columns')
      // Opening tag
      return `<div ${stringAttributes}>`
    }
    // closing tag
    return '</div>'
  },
}

/** @type {SectionRenderer} */
const notification = {
  render(tokens, idx) {
    if (tokens[idx].nesting >= 1) {
      const { attrs } = tokens[idx]
      let stringAttributes = ''
      stringAttributes = mergeAttributes(attrs, 'notification')
      // Opening tag
      return `<div ${stringAttributes}>`
    }
    // closing tag
    return '</div>'
  },
}

module.exports = {
  section,
  column,
  columns,
  notification,
}
