import {
  META_CHANGED,
  SET_BROWSER_DATA,
  SET_BLOG_POSTS,
} from '~/utils/mutations'

/** @typedef {import('types').VuexState} State */

/** @type {State} */
const rootState = {
  route: null,
  current: {},
  matched: [],
  currentSlide: 0,
  browserData: null,
  blogPosts: [],
}
export const state = () => rootState

/**
 * @type {import("vuex").GetterTree<State, State>}
 */
export const getters = {}

/**
 * @type {import("vuex").MutationTree<State>}
 */
export const mutations = {
  [META_CHANGED](currentState, { route, current, matched }) {
    currentState.route = route
    currentState.current = current
    currentState.matched = matched
  },
  [SET_BROWSER_DATA](currentState, parsedUA) {
    currentState.browserData = parsedUA
  },
  [SET_BLOG_POSTS](st, list) {
    st.blogPosts = list
  },
}

/**
 * @type {import("vuex").ActionTree<State, State>}
 */
export const actions = {
  metaChanged({ commit }, meta) {
    commit(META_CHANGED, meta)
  },
  nuxtServerInit({ commit }) {
    const files = require.context('~/assets/content/blog/', false, /\.md$/)
    const blogPosts = files.keys().map((key) => {
      const { title, image } = files(key)
      return {
        title,
        image,
        slug: key.slice(2, -5),
      }
    })
    commit(SET_BLOG_POSTS, blogPosts)
  },
}
