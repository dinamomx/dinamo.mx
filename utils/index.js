/**
 * @description Procesa opciones que puede ser una funcion o un objeto
 *
 * @exports
 * @param {{callback: Function}|{[x: string]: any}} value - La función o objeto.
 * @returns  {{callback: Function}|{[x: string]: any}} El objeto o el resultado del callback.
 */
export function processOptions(value) {
  let options
  if (typeof value === 'function') {
    // Simple options (callback-only)
    options = {
      callback: value,
    }
  } else {
    // Options object
    options = value
  }
  return options
}

/**
 * @description Compara dos objetos de forma recursiva
 *
 * @exports
 * @param {{[x: string]: any}} object1 - El primero objeto a comparar.
 * @param {{[x: string]: any}} object2 - El segundo objeto a comparar.
 * @returns {boolean} Si en efecto los dos objetos son iguales.
 */
export function deepEqual(object1, object2) {
  if (object1 === object2) {
    return true
  }
  if (typeof object1 === 'object') {
    const keys = Object.keys(object1)
    for (const key of keys) {
      if (!deepEqual(object1[key], object2[key])) {
        return false
      }
    }
    return true
  }
  return false
}

/**
 * @description Genera un número aleatorio
 *
 * @exports
 * @returns {number} - Un número aleatorio.
 */
export function randomNumber() {
  return Math.floor((1 + Math.random()) * 0x10000)
}

/**
 * Genera la información meta de la página para usarse con vue-meta
 *
 * @param {import('types').SeoObject} data La clave del objeto que contien la información meta
 * @returns {import('vue-meta').MetaInfo}
 */
export function commonHeadTags(data) {
  const { title } = data
  const domain = `https://${process.env.productionDomain}`
  /** @type {import('vue-meta').MetaInfo} */
  const head = {}
  head.meta = []
  if (title) {
    head.title = title
    head.meta.push({ hid: 'og:title', property: 'og:title', content: title })
  }

  /** @type {?string} */
  let ogImage
  let ogImageHeight = ''
  let ogImageWidth = ''
  let ogImageType = 'image/jpeg'
  let ogImageAlt = 'Imágen descriptiva del tema de esta entrada de blog'
  if (data.og_image) {
    if (typeof data.og_image === 'string') {
      ogImage = data.og_image
    } else {
      ogImage = data.og_image.src
      ogImageHeight = data.og_image.height
      ogImageWidth = data.og_image.width
      ogImageType = data.og_image.type || 'image/jpeg'
      ogImageAlt = data.og_image.alt || ogImageAlt
    }
  } else {
    ogImage = data.image_cover || data.image_card || null
  }
  if (ogImage) {
    ogImage = `${domain}${ogImage}`
    /** @type {import('vue-meta/types/vue-meta').MetaPropertyProperty[]} */
    const ogImageProperties = [
      {
        hid: 'og:image:url',
        property: 'og:image:url',
        content: ogImage,
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: ogImage,
      },
      {
        hid: 'og:image:type',
        property: 'og:image:type',
        content: ogImageType,
      },
      {
        hid: 'og:image:height',
        property: 'og:image:height',
        content: ogImageHeight,
      },
      {
        hid: 'og:image:width',
        property: 'og:image:width',
        content: ogImageWidth,
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: ogImageAlt,
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: ogImageAlt,
      },
    ]
    head.meta = head.meta.concat(ogImageProperties)
  }
  const canonical = data.url
    ? data.url.startsWith('http')
      ? data.url
      : `${domain}/${data.url}`
    : domain
  if (canonical) {
    head.link = []
    head.link.push({ rel: 'canonical', href: canonical })
    head.meta.push({
      hid: 'og:url',
      property: 'og:url',
      content: canonical,
    })
  }
  const { description } = data
  if (description) {
    head.meta.push.apply([
      {
        hid: 'og:description',
        property: 'og:description',
        content: description,
      },
      {
        hid: 'description',
        name: 'description',
        content: description,
      },
    ])
  }

  return head
}

/** @type {AddEventListenerOptions} */
export const PASSIVE_EVENT_OPTIONS = {
  passive: true,
  capture: false,
}
