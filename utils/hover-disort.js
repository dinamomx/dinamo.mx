/* eslint-disable no-console */
import {
  WebGLRenderer,
  Scene,
  OrthographicCamera,
  TextureLoader,
  LinearFilter,
  VideoTexture,
  Vector4,
  PlaneBufferGeometry,
  Mesh,
  ShaderMaterial,
  NearestFilter,
  NearestMipmapNearestFilter,
  ClampToEdgeWrapping,
} from 'three'
import { TweenMax, Expo } from 'gsap'
// NOTE: Intentar este esquema para renderizar muchas imagenes https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html

export default class HoverDisort {
  vertex = `
varying vec2 vUv;
void main() {
  vUv = uv;
  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}`

  fragment = `
varying vec2 vUv;

uniform float dispFactor;
uniform float dpr;
uniform sampler2D disp;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float angle1;
uniform float angle2;
uniform float intensity1;
uniform float intensity2;
uniform vec4 res;
uniform vec2 parent;

mat2 getRotM(float angle) {
  float s = sin(angle);
  float c = cos(angle);
  return mat2(c, -s, s, c);
}

void main() {
  vec4 disp = texture2D(disp, vUv);
  vec2 dispVec = vec2(disp.r, disp.g);

  vec2 uv = 0.5 * gl_FragCoord.xy / (res.xy) ;
  vec2 myUV = (uv - vec2(0.5))*res.zw + vec2(0.5);


  vec2 distortedPosition1 = myUV + getRotM(angle1) * dispVec * intensity1 * dispFactor;
  vec2 distortedPosition2 = myUV + getRotM(angle2) * dispVec * intensity2 * (1.0 - dispFactor);
  vec4 _texture1 = texture2D(texture1, distortedPosition1);
  vec4 _texture2 = texture2D(texture2, distortedPosition2);
  gl_FragColor = mix(_texture1, _texture2, dispFactor);
}
`

  /**
   *
   * @param {import('types').HoverDisortArguments} param0 Parámetros de hover
   */
  constructor({
    parent,
    displacementImage,
    image1,
    image2,
    imagesRatio = 1,
    intensity = 1,
    intensity1,
    intensity2,
    commonAngle = Math.PI / 4,
    angle1,
    angle2,
    speedIn,
    speedOut,
    userHover = true,
    easing = Expo.easeOut,
    speed,
    video = false,
  }) {
    this.parent = parent
    this.dispImage = displacementImage
    this.image1 = image1
    this.image2 = image2
    this.imagesRatio = imagesRatio
    this.intensity1 = intensity1 || intensity
    this.intensity2 = intensity2 || intensity
    this.commonAngle = commonAngle // 45 degres
    this.angle1 = angle1 || this.commonAngle
    this.angle2 = angle2 || this.commonAngle * 3
    this.speedIn = speedIn || speed || 1.6
    this.speedOut = speedOut || speed || 1.2
    this.userHover = userHover
    this.easing = easing
    this.isVideo = video
    // Validando opciones
    if (!this.parent) {
      console.warn('HoverDisort', 'Parent missing')
      return
    }
    if (!(this.image1 && this.image2 && this.dispImage)) {
      console.warn('HoverDisort', 'One or more images are missing')
      return
    }
    this.scene = new Scene()
    this.camera = new OrthographicCamera(
      this.parent.offsetWidth / -2,
      this.parent.offsetWidth / 2,
      this.parent.offsetHeight / 2,
      this.parent.offsetHeight / -2,
      1,
      1000
    )
    this.camera.position.z = 1

    this.renderer = new WebGLRenderer({
      antialias: false,
      alpha: true,
    })

    this.renderer.setPixelRatio(2)
    this.renderer.setClearColor(0xFFFFFF, 0)
    this.renderer.setSize(parent.offsetWidth, parent.offsetHeight)
    parent.append(this.renderer.domElement)

    this.loadTextures()
    if (this.isVideo) {
      this.setupVideo()
    } else {
      this.setupImage()
    }
    this.setupMat()
    this.addHoverEvents()
    // this.addResizeEvent()
    // this.render()
  }

  loadTextures() {
    this.loader = new TextureLoader()
    // this.loader.crossOrigin = ''
    this.disp = this.loader.load(this.dispImage, this.render.bind(this))
    this.disp.minFilter = LinearFilter
    this.disp.magFilter = LinearFilter
  }

  setupImage() {
    this.texture1 =
      this.loader &&
      this.loader.load(this.image1, (texture) => {
        console.log(texture)
        texture.magFilter = NearestFilter
        texture.minFilter = NearestMipmapNearestFilter
        texture.wrapS = ClampToEdgeWrapping
        texture.wrapT = ClampToEdgeWrapping
        this.render()
      })
    this.texture2 =
      this.loader && this.loader.load(this.image2, this.render.bind(this))
  }

  setupVideo() {
    const animate = () => {
      requestAnimationFrame(animate)
      this.render()
    }
    animate()

    const video = document.createElement('video')
    video.autoplay = true
    video.loop = true
    video.src = this.image1
    video.load()

    const video2 = document.createElement('video')
    video2.autoplay = true
    video2.loop = true
    video2.src = this.image2
    video2.load()

    this.texture1 = new VideoTexture(video)
    this.texture2 = new VideoTexture(video2)
    this.texture1.magFilter = LinearFilter
    this.texture2.magFilter = LinearFilter
    this.texture1.minFilter = LinearFilter
    this.texture2.minFilter = LinearFilter

    video2.addEventListener(
      'loadeddata',
      () => {
        video2.play()

        this.texture2 = new VideoTexture(video2)
        this.texture2.magFilter = LinearFilter
        this.texture2.minFilter = LinearFilter
        if (this.mat) {
          this.mat.uniforms.texture2.value = this.texture2
        }
      },
      false
    )

    video.addEventListener(
      'loadeddata',
      () => {
        video.play()

        this.texture1 = new VideoTexture(video)

        this.texture1.magFilter = LinearFilter
        this.texture1.minFilter = LinearFilter
        if (this.mat) {
          this.mat.uniforms.texture1.value = this.texture1
        } else {
          console.warn('HoverDisort.setupVideo', 'Mat Missing')
        }
      },
      false
    )
  }

  setupMat() {
    const { offsetHeight, offsetWidth } = this.parent
    const { a1, a2 } = this.calculateRatios()
    console.log({ a1, a2, offsetHeight, offsetWidth })
    this.mat = new ShaderMaterial({
      uniforms: {
        intensity1: {
          type: 'f',
          value: this.intensity1,
        },
        intensity2: {
          type: 'f',
          value: this.intensity2,
        },
        dispFactor: {
          type: 'f',
          value: 0,
        },
        angle1: {
          type: 'f',
          value: this.angle1,
        },
        angle2: {
          type: 'f',
          value: this.angle2,
        },
        texture1: {
          type: 't',
          value: this.texture1,
        },
        texture2: {
          type: 't',
          value: this.texture2,
        },
        disp: {
          type: 't',
          value: this.disp,
        },
        res: {
          type: 'vec4',
          value: new Vector4(offsetWidth, offsetHeight, a1, a2),
        },
        dpr: {
          type: 'f',
          value: window.devicePixelRatio,
        },
      },

      vertexShader: this.vertex,
      fragmentShader: this.fragment,
      transparent: true,
      opacity: 0.8,
    })

    const geometry = new PlaneBufferGeometry(offsetWidth, offsetHeight, 1)
    this.object = new Mesh(geometry, this.mat)
    this.scene.add(this.object)
  }

  transitionIn() {
    if (!this.mat) {
      console.warn('HoverDisort', 'Map not setted')

      return
    }
    TweenMax.to(this.mat.uniforms.dispFactor, this.speedIn, {
      value: 1,
      ease: this.easing,
      onUpdate: () => this.render(),
      onComplete: () => this.render(),
    })
  }

  transitionOut() {
    if (!this.mat) {
      console.warn('HoverDisort', 'Map not setted')

      return
    }

    TweenMax.to(this.mat.uniforms.dispFactor, this.speedOut, {
      value: 0,
      ease: this.easing,
      onUpdate: () => this.render(),
      onComplete: () => this.render(),
    })
  }

  addHoverEvents() {
    this.parent.addEventListener('mouseenter', this.transitionIn.bind(this))
    this.parent.addEventListener('touchstart', this.transitionIn.bind(this))
    this.parent.addEventListener('mouseleave', this.transitionOut.bind(this))
    this.parent.addEventListener('touchend', this.transitionOut.bind(this))
  }

  calculateRatios() {
    const { offsetHeight, offsetWidth } = this.parent
    let a1
    let a2
    if (offsetHeight / offsetWidth < this.imagesRatio) {
      a1 = 1
      a2 = offsetHeight / offsetWidth / this.imagesRatio
    } else {
      a1 = (offsetWidth / offsetHeight) * this.imagesRatio
      a2 = 1
    }
    return { a1, a2 }
  }

  addResizeEvent() {
    window.addEventListener('resize', () => {
      const { offsetHeight, offsetWidth } = this.parent

      const { a1, a2 } = this.calculateRatios()
      this.object.material.uniforms.res.value = new Vector4(
        offsetWidth,
        offsetHeight,
        a1,
        a2
      )
      this.renderer.setSize(offsetWidth, offsetHeight)

      this.render()
    })
  }

  render() {
    this.renderer.render(this.scene, this.camera)
  }

  destroy() {
    this.texture1.dispose()
    this.texture2.dispose()
    this.mat.dispose()
    this.scene.dispose()
    this.object.remove()
    this.renderer.dispose()
  }
}
