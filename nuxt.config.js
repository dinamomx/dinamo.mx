/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
// @ts-check
import { localBussiness, organization, website } from './utils/schema'

const isProduction = process.env.NODE_ENV === 'production'
const enableGrid = true
/**
 * El dominio final del proyecto
 *
 * @type {string}
 */
const dominioFinal = 'www.dinamo.mx'
/**
 * Headers for the page
 *
 * @type {import('vue-meta').MetaInfo}
 */
const head = {
  meta: [
    {
      hid: 'keywords',
      name: 'keywords',
      content:
        'Dinamo, Agencia de comunicación, ¿cómo hago que mi página aparezca en primera posición? ¿Cómo vendo en facebook?, diseño de logotipo, tienda en línea, dínamo, agencia de comunicación, agencia de publicidad, marketing digital, diseño y desarrollo web, front end, google ADS, Facebook ADS, UX Design, UI, usabilidad, navegabilidad.',
    },
    {
      hid: 'description',
      name: 'description',
      content:
        'dínamo somos una agencia de comunicación, la formamos un grupo de personas con perfiles complementarios, estamos ubicados al sur de la CDMX. Nos apasiona trabajar en proyectos que desafien nuestra creatividad y nos exijan dar lo mejor de nosotros en cada momento.',
    },
    {
      hid: 'og:title',
      property: 'og:title',
      content: 'dínamo agencia de publicidad',
    },
    {
      hid: 'og:url',
      property: 'og:url',
      content: 'https://www.dinamo.mx/',
    },
    {
      hid: 'og:description',
      property: 'og:description',
      content:
        'dínamo somos una agencia de comunicación, la formamos un grupo de personas con perfiles complementarios, estamos ubicados al sur de la CDMX. Nos apasiona trabajar en proyectos que desafien nuestra creatividad y nos exijan dar lo mejor de nosotros en cada momento.',
    },
    {
      hid: 'og:image:url',
      property: 'og:image:url',
      content: 'https://www.dinamo.mx/files/images/og-image.jpg',
    },
    {
      hid: 'og:image:type',
      property: 'og:image:type',
      content: 'image/jpeg',
    },
    {
      hid: 'og:image:width',
      property: 'og:image:width',
      content: '1500',
    },
    {
      hid: 'og:image:height',
      property: 'og:image:height',
      content: '785',
    },
    {
      hid: 'og:image:alt',
      property: 'og:image:alt',
      content:
        'Montaje fotográfico de una persona en cuclillas con cabeza de perro.',
    },
  ],
  link: [],
  titleTemplate: (titleChunk = '') =>
    titleChunk
      ? `${titleChunk} - dínamo agencia de comunicación y diseño`
      : 'dínamo agencia de comunicación y diseño',
  script: [
    {
      innerHTML: JSON.stringify(website),
      type: 'application/ld+json',
    },
    {
      innerHTML: JSON.stringify(localBussiness),
      type: 'application/ld+json',
    },
    {
      innerHTML: JSON.stringify(organization),
      type: 'application/ld+json',
    },
  ],
  __dangerouslyDisableSanitizers: ['script'],
}

/**
 * CSS que solo es pensado para desarrollo
 */
const developmentCss = [
  '~assets/styles/overlay-grid.css',
  // '~assets/styles/super-debug.scss',
]

/**
 * CSS para la página
 */
let css = [
  '@fortawesome/fontawesome-svg-core/styles.css',
  '~assets/styles/transitions.scss',
  '~assets/styles/base.scss', // Resets de estilos
  '~assets/styles/components.scss', // Componentes
  '~assets/styles/utilities.scss', // Componentes
]

/**
 * Configuración para postCSS
 *
 * @type {import('@nuxt/types/config/build').PostcssConfiguration}
 */
const postCSSConfiguration = {
  plugins: {
    'postcss-import': {},
    'postcss-url': {},
    tailwindcss: {},
    'postcss-color-function': {},
    // Solo habilitar si se tiene que soportar ie11
    // 'postcss-object-fit-images': {},
    // SEE: https://preset-env.cssdb.org/
    'postcss-preset-env': {
      cascade: false,
      grid: isProduction,
      stage: 0,
      // SEE: https://github.com/postcss/autoprefixer#grid-autoplacement-support-in-ie
      autoprefixer: {
        grid: 'autoplace',
      },
    },
    'postcss-reporter': {
      clearReportedMessages: true,
    },
    cssnano: isProduction
      ? {
          preset: 'default',
          discardComments: {
            removeAll: true,
          },
          zindex: 100,
        }
      : false,
  },
}
/**
 * Modules de nuxt para usarse solo en producción
 */
const productionModules = [
  /**
   * Limpiador de CSS
   */
  'nuxt-purgecss',
  /**
   * Faebook pixel
   * Desactivado por defecto
   */
  // require.resolve('./modules/facebook-pixel'),
  /** Cuando se busca un service worker */
  '@nuxtjs/workbox',
]

/**
 * Modulos pensados a usarse solo en desarrrollo
 *
 * @type {string[]}
 */
const developmentModules = []

/**
 * Modulos que siempre se cargan
 */
let modules = [
  '@nuxtjs/style-resources',
  './modules/draft-pages',
  [
    '@dinamomx/nuxt-fbpixel-module',
    { id: '661561957280518', debug: !isProduction, disable: true },
  ],
  [
    'nuxt-i18n',
    {
      locales: [
        {
          code: 'es',
          iso: 'es-MX',
          file: 'es-MX.json',
        },
        {
          code: 'en',
          iso: 'en-US',
          file: 'en-US.json',
        }
      ],
      langDir: '~/lang/',
      defaultLocale: 'es',
      vueI18nLoader: true,
      vueI18n: {
        fallbackLocale: 'es',
        messages: {
          en: {
            welcome: 'ENG Welcome'
          },
          es: {
            welcome: 'ESP Bienvenido'
          }
        }
      }
    }
  ]
]

/**
 * Plugins de webpack
 */
const webPackPlugins = isProduction
  ? []
  : [new (require('lodash-webpack-plugin'))()]

/**
 * Acomodamos la configuración de acuerdo a producción o desarrollo
 */
if (isProduction) {
  modules = modules.concat(productionModules)
} else if (enableGrid) {
  css = css.concat(developmentCss)
} else {
  modules = modules.concat(developmentModules)
}

/** @type {import('@nuxt/types').Configuration} */
const nuxtConfig = {
  /* para netlify
  // mode: 'spa',
  // para dinamo.mx */
  mode: 'universal',
  buildModules: modules,
  modules: ['@nuxt/content'],
  head,
  /**
   * Variables de entorno para la página
   */
  env: {
    productionDomain: dominioFinal,
    /** Configuración de google-analytics */
    gaId: 'GTM-MQRRCD5',
  },
  serverMiddleware: ['./middleware/redirect'],
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    middleware: ['meta'],
    extendRoutes(routs, nuxtResolve) {
      const component = nuxtResolve(
        __dirname,
        'pages',
        'a113',
        '_year',
        'index.vue'
      )
      for (const route of routs) {
        if (route.path === '/a113' && route.children) {
          for (let i = 0; i < route.children.length; i++) {
            const childRoute = route.children[i]
            if (childRoute.path === '' || childRoute.path === ':year') {
              childRoute.component = component
            }
          }
          // No necesitamos iterar más una vez que hayamos encontrado este elemento
          break
        }
      }
    },

    scrollBehavior: async function(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      }

      const findEl = async (hash, x = 0) => {
        return (
          document.querySelector(hash) ||
          new Promise(resolve => {
            if (x > 50) {
              return resolve(document.querySelector("#app"));
            }
            setTimeout(() => {
              resolve(findEl(hash, ++x || 1));
            }, 100);
          })
        );
      };

      if (to.hash) {
        let el = await findEl(to.hash);
        if ("scrollBehavior" in document.documentElement.style) {
          return window.scrollTo({ top: el.offsetTop, behavior: "smooth" });
        } else {
          return window.scrollTo(0, el.offsetTop);
        }
      }

      return { x: 0, y: 0 };
    },

  },





  /**
   * Transición por defecto
   */
  pageTransition: 'page',
  /**
   * Crea un set para navegadores más vergras
   * https://nuxtjs.org/api/configuration-modern#the-modern-property
   */
  modern: isProduction,
  /**
   * CSS global
   */
  css,
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070',
  },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~/plugins/global-components.js',
    '~/plugins/font-awesome.js',
    '~/plugins/global-components.js',
    '~/plugins/vue-gtag.client.js',
    '~/plugins/vue-gtag.server.js',
    '~/plugins/detect-webp.client.js',
    '~/plugins/detect-webp.server.js',
    '~/plugins/tracking.client.js',
    '~/plugins/tracking.server.js',
    '~/plugins/logrocket.js',
    // '~/plugins/uaparser.server.js', // Activar solo si se usa vuex
    {
      src: '~plugins/scroll-track.js',
      ssr: false,
    },
  ],

  watch: ['./tailwind.config.js'],
  generate: {
    fallback: true,
  },
  /*
   ** Build configuration
   */
  build: {
    transpile: ['vue-carousel', 'vue-social-sharing'],
    /**
     * La configuración de postcss es necesaria tenerla aquí más que en su
     * propio archivo por que puede ser quitar todas las características de nuxt
     * teniendo efectos adversos.
     */

    postcss: postCSSConfiguration,

    /**
     * Enable thread-loader in webpack building
     */
    parallel: !isProduction,
    /**
     * Enable cache of terser-webpack-plugin and cache-loader
     */
    cache: false,
    /**
     * Es necesario sobreescribir lo que hace babel por defecto
     */
    babel: {
      presets: ({ isServer }) => [
        [
          require.resolve('@nuxt/babel-preset-app'),
          {
            corejs: { version: 3 },
            buildTarget: isServer ? 'server' : 'client',
            // Incluir polyfills globales es mejor que no hacerlo
            useBuiltIns: 'usage',
            // Un poco menos de código a cambio de posibles errores
            loose: true,
            // Nuxt quiere usar ie 9, yo no.
            targets: isServer
              ? {
                  node: 10,
                }
              : {},
          },
        ],
      ],
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        '@babel/plugin-proposal-optional-chaining',
      ],
    },
    // Hace el css cacheable
    extractCSS: isProduction,
    // Alias el ícono de buefy a uno que soporta los íconos de font-awesome
    plugins: webPackPlugins,
    extend(config, { isDev, isClient }) {
      if (config.module) {
        // Añade loader para contenido en markdown con front-matter
        config.module.rules.push({
          test: /\.md$/,
          exclude: /(^\/~)?content/,
          use: require.resolve('./utils/content-loader.js'),
        })
        // Añade un loader básico para yaml
        config.module.rules.push({
          test: /\.ya?ml$/,
          use: 'js-yaml-loader',
        })
        // Evita conflictos con el bloque de documentación
        config.module.rules.push({
          resourceQuery: /blockType=docs/,
          loader: require.resolve('./utils/documentation-tag-loader.js'),
        })
        // Run ESLINT on save
        if (isDev && isClient) {
          config.module.rules.push({
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /node_modules/,
            options: {
              formatter: require('eslint/lib/cli-engine/formatters/stylish'),
            },
          })
        }
      }
      return config
    },
  },
  /**
   * Configuraciones para los modulos
   */
  styleResources: {
    scss: ['~/assets/styles/_variables.scss', '~/assets/styles/_mixins.scss'],
  },
  /**
   * Donde se encuentren alojados los íconos
   */
  svgSprite: {
    input: '~/assets/icons/svg/',
  },
  workbox: {},
  content: {
    // Only search in title and description
    fullTextSearchFields: ['title', 'description', 'slug', 'text', 'autor'],
    markdown: {
      plugins: ['remark-attr'],
    },
  },
  /**
   * Configuración para PurgeCSS
   */
  purgeCSS: {
    whitelistChildren: ['is-top', 'VueCarousel', 'access__tag'],
    whitelist: [
      'access__tag',
      'svg-inline--fa',
      'is-active',
      'text-white',
      'text-black',
      'video',
      'is-active-section',
      'is-desactive-section',
      'sr-only',
      'aspect-ratio-3/2',
      'nuxt-progress',
      'md\\:float-right',
      'md\\:float-left',
    ],
    whitelistPatterns: [
      /^btn-burger/,
      /[\w|-]+-(enter|leave|move)-?(active|to)?/,
      /^fa-/,
      /-fa$/,
      /^btn-burger/,
      /float-right$/,
    ],
    whitelistPatternsChildren: [/nav--[\w|-]/, /^VueCarousel-.*/],
  },
}

export default nuxtConfig
