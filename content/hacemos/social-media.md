---
title: 'Social media'
type: 'servicios'
img: hacemos/socialMediaDiego.png
main: false
schema:
  "@context": http://schema.org
  "@graph":
    - "@type": Service
      serviceType: Social Media
      provider:
        "@type": LocalBusiness
        name: dínamo
      areaServed:
        "@type": State
        name: Mexico
      hasOfferCatalog:
        "@type": OfferCatalog
        name: Comunitiy manager
        itemListElement:
        - "@type": OfferCatalog
          name: Redes sociales
          itemListElement:
          - "@type": Offer
            itemOffered:
              "@type": Service
              name: Generación de contenido
          - "@type": Offer
            itemOffered:
              "@type": Service
              name: Publicidad en medios digitales
          - "@type": Offer
            itemOffered:
              "@type": Service
              name: Fotografía
        - "@type": OfferCatalog
          name: marketing digital
        - "@type": OfferCatalog
          name: Gestión de redes sociales
        - "@type": OfferCatalog
          name: Diseño web

---

Dinos lo que quieres comunicar y nosotros nos encargamos de cómo comunicarlo. Diseñamos la estrategia y generamos el contenido que acerca a tu marca con los usuarios (tus clientes).
Además, medimos y analizamos estadísticas para garantizar una buena continuidad y resultados.
