---
title: 'Imagen corporativa'
type: 'servicios'
img: hacemos/logo_jio.png
main: false
schema:
  "@context": http://schema.org
  "@graph":
    - "@type": Service
      serviceType: Imagen corporativa
      provider:
        "@type": LocalBusiness
        name: dínamo
      areaServed:
        "@type": State
        name: Mexico
      hasOfferCatalog:
        "@type": OfferCatalog
        name: Imagen gráfica de marca
        itemListElement:
        - "@type": OfferCatalog
          name: Manual de uso de marca
        - "@type": OfferCatalog
          name: Identidad gráfica
        - "@type": OfferCatalog
          name: Diseño de logotipo
        - "@type": OfferCatalog
          name: Guía de pantone
        - "@type": OfferCatalog
          name: Color RGB y CMYK
        - "@type": OfferCatalog
          name: Reticulado de logotipo
        - "@type": OfferCatalog
          name: Construcción de marca




---

Juntos crearemos la identidad de tu empresa.
Platiquemos de tu proyecto y hablemos de su carácter.
