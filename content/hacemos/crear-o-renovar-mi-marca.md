---
title: 'Creación de marca'
type: 'category'
schema:
  "@context": http://schema.org
  "@graph":
    - "@type": Service
      serviceType: Diseño de mi marca
      provider:
        "@type": LocalBusiness
        name: dínamo
      areaServed:
        "@type": State
        name: Mexico
      hasOfferCatalog:
        "@type": OfferCatalog
        name: Imagen corporativa
        itemListElement:
        - "@type": OfferCatalog
          name: SEO
          itemListElement:
          - "@type": Offer
            itemOffered:
              "@type": Service
              name: Diseño de logotipo
          - "@type": Offer
            itemOffered:
              "@type": Service
              name: Manual de uso de marca
          - "@type": Offer
            itemOffered:
              "@type": Service
              name: Gia de colores rgn, cmyk y pantone
        - "@type": OfferCatalog
          name: marketing digital
        - "@type": OfferCatalog
          name: Gestión de redes sociales
        - "@type": OfferCatalog
          name: Diseño web
        - "@type": OfferCatalog
          name: Servicio de impresión
        - "@type": OfferCatalog
          name: Gestión de redes sociales

---

Podemos ayudarte a rediseñar tu marca, juntos podemos lograr grandes cosas.
