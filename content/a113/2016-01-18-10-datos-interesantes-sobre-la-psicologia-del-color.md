---
title: '10 Datos interesantes sobre la Psicología del Color.'
subtitle:
  En pocas palabras la psicología del color es un estudio que habla sobre el
  comportamiento y la conducta que asume inconscientemente nuestro cerebro al
  percibir los colores.
formato: B
tag: Cultura y Arte
autor: Shamed Hernández
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/psicologia-color.jpg'
headline: El color, necesidad para la creatividad y para tu vida.
abstract:
  Los colores no solo sirven para saber si nuestro oufit está bien combinado, si
  tenemos inclinaciones o preferencias por algo que pueda determinar nuestras
  características, va más allá de la colorimetría de una película o la
  decoración de nuestro hogar ¿sabes que hay detrás de cada color?
description:
  Psicología del color es un estudio que habla sobre el comportamiento y la
  conducta que asume inconscientemente nuestro cerebro al percibir los colores.
url: https://www.dinamo.mx/a113/2016/10-datos-interesantes-sobre-la-psicologia-del-color
keywords:
  - 'color'
  - 'psicología'
  - 'psicología del color'
  - 'estudio'
  - 'comportamiento'
  - 'cerebro'
publishedAt: 2016-01-18 20:20:20
---

1. Las personas que visten de color rojo, pueden lucir hasta 3 veces más
   atractivas, llamando así la atención de muchas miradas.

2. Para Llamar la atención en los niños y estimular el apetito en ellos utiliza,
   en utensilios como platos, cucharas y vasos, el color naranja a la hora de
   comer.

3. El amarillo es uno de los colores más utilizados para estimular el apetito,
   grandes empresas de comidas rápidas lo utilizan, ya que aparte de abrir el
   apetito, el exceso de este color causa fatiga en las personas, haciendo que
   se marchen y desocupen el lugar para nuevos clientes.

4. Si hiciste algo malo y te agarraron infraganti, será bueno que vistas de
   color marrón, así causarás lástima, compasión o piedad hacia ti.

5. Vestir de color negro, te hará ver delgado, elegante y muy sofisticado.

6. Si el espacio donde habitas es algo pequeño, aplicar color blanco es la
   elección apropiada para dar sensación de espacio y profundidad a tus ambiente
   no muy amplios.

7. Cuando algo nos llama la atención, hemos usado un 92% el sentido de la vista,
   un 6% el sentido del tacto y un 2% el sentido auditivo.

8. Los 3 colores preferidos por la mayoría de los hombres son el azul, el verde
   y el negro. Los tres preferidos para la mayoría de las mujeres son el azul,
   el púrpura y el verde.

9. El color azul es el más utilizado por las marcas más importantes del mundo.
   El color del Logotipo de Facebook es azul, debido a que su creador Mark
   Zuckerberg sufre de daltonismo y el color azul es el que mejor distingue.

10. Para llamar la atención y crear un llamado al acción en tu sitio web, los
    colores más indicados son el rojo y verde. Un experimento en un sitio web,
    detectó que un botón de color rojo es un 21% más cliqueado que un botón de
    color verde.

Fuentes:
[Publicidad pixel](http://www.publicidadpixel.com/significado-de-los-colores/) |
[s3](http://s3.accesoperu.com/wp6/includes/htmlarea/mezclador/ayuda/epc.htm)
