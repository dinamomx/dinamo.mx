---
title:
  'OCULUS y Mapillary, las adquisiciones de Facebook y el paso hacia la realidad
  virtual.'
abstract:
  La realidad virtual está por llegar a Facebook con la compra de Oculus y
  Mapillary, pero esto es solo una parte del plan de Zuckerberg, las posibles
  ganancias con la publicidad de las Fan pages que muestran una experiencia
  previa para adquirir sus productos o servicios, promete ser el negocio que se
  está buscando.
description: Conoce las posibles ventajas de la experiencia virtual en Facebook.
autor: Emilia Silis.
tag: Ciencia y Tecnología
main: false
header_color: 'alternate'
autor_detalle: 'emilia@wdinamo.com'
image_card: 'blog/facebook-oculus/cover.jpg'
image_cover: 'blog/facebook-oculus/cover%.jpg'
og_image:
  src: '/files/images/blog/facebook-oculus/cover_1200.jpg'
  type: 'image/jpeg'
  height: 1200
  width: 630
url: https://www.dinamo.mx/a113/2020/oculus-facebook-adquisiciones-realidad-virtual
keywords:
  - Facebook
  - Realidad virtual
  - Realidad aumentada
  - Publicidad
  - Tecnología
  - Oculus
  - Mapillary
  - My Business
  - Fan Page

publishedAt: 2020-07-14 14:09:00.152
publisher: 'https://www.dinamo.mx/'
---

-Sí Mark, ya sabemos que eres dueño de todo…

Hace unas semanas al iniciar tu sesión en Facebook, Mark Zuckerberg nos mostró
un integrante más en esta red social, OCULUS, que tan solo le costó 2 mil
millones de dólares, pero esta adquisición no es nada nueva, pues esta compra la
hizo en marzo de 2014. Entonces, ¿por qué agregar hasta ahora el ícono?

En algún momento Mark comentó -después de adquirir Instagram y WhatsApp- que no
haría una nueva compra sino hasta que llegara un candidato interesante (o
amenazante). Pero, ¿qué de interesante tiene OCULUS para haber captado la
atención de Mark?

![Gráfica animada](/files/images/blog/facebook-oculus/facebook-network.gif){width="500px"
height="500px" loading="lazy" .block .mx-auto .mb-4}

## Qué es OCULUS y por qué debería interesarte.

**Oculus es la empresa líder en el mercado de realidad virtual** gracias a sus
gafas **Oculus Rift**. A finales de mayo, Zuckerberg anunció su nueva plataforma
Oculus for Business, razón por la que integró recientemente el ícono al iniciar
sesión en Facebook.

Esta adquisición es un gran paso hacia la **realidad virtual** y será parte de
la cotidianidad en la sociedad, y es que ya vimos que no estamos tan lejos de
ello, la contingencia por esta pandemia (covid-19) y la prudente comunicación de
**Oculus for Business**, nos recuerda que Mark sigue siendo un visionario y sus
plataformas “nos conocen tan bien” gracias a todo lo que compartimos. En este
contexto actual, entendemos la **importancia de conexiones más reales** y de la
interacción, entonces, las fotos, llamadas, videollamadas, ya no son
suficientes.

> “Creemos que la realidad virtual será definida fundamentalmente para las
> experiencias sociales…” Brendan Iribe, Co-fundador y CEO de Oculus VR.

<!-- prettier-ignore -->
  ![Gráfica animada](/files/images/blog/facebook-oculus/ortogonal.jpg){width="500px" height="500px" loading="lazy" .block .mx-auto .mb-4}

## Relación de OCULUS y Mapillary, el sistema de mapas y competencia de Google Street View.

Seguramente, así como yo, están cada vez más intrigados en qué está planeando
Zuckerberg para nosotros. **Mapillary**, su otra compra interesante que fue
anunciada el 18 de junio de este año, recopila imágenes de usuarios en más de
190 países, crea un **sistema de cartografía automatizado**, entonces, el
resultado esperado es, agregar **mapas virtuales en Facebook**. ¿Y qué podemos
hacer con esto? Viene lo interesante:

**Experiencias 360º**, ¿que tal te vendría presumir en tus redes sociales ese
viaje soñado, mostrar tal cual tú lo viste y disfrutaste, paseos, hospedaje,
comida, actividades y demás?...

Hablando de la **conveniencia para negocios**, eso podrá en un futuro, **generar
ingresos a través de publicidad**, aquí puede ser el gran interés de Mark en la
compra de estas empresas (oculus y mapillary), el negocio de Facebook, que está
pasando a ser más que solo una red social.

Esta pandemia, nos ha acercado aceleradamente a la realidad virtual y aumentada,
el trabajo, consumo, relaciones y experiencias, van a cambiar más pronto de lo
que imaginábamos.

<!-- prettier-ignore -->
  ![Gráfica animada](/files/images/blog/facebook-oculus/oculus.gif){width="500px" height="500px" loading="lazy" .block .mx-auto .mb-4}

No sabemos exactamente lo que busca lograr Facebook, lo cierto es que, para
quienes estamos inmersos en redes sociales y la comunicación digital, entendemos
que vienen grandes cambios no solo para las cuentas personales, sino para Fan
Pages de diversos sectores como salud, educación, turismo, entre otras.

Como consumidores, apostamos a quienes nos ofrezcan la mejor experiencia, la
suma de estas grandes empresas a Facebook prometen que estar en esta red social
tendrá resultados positivos. Ya veremos.

<!-- prettier-ignore -->
**Referencias**

https://elpais.com/tecnologia/2014/03/26/actualidad/1395796446_034242.html

https://www.marketingdirecto.com/digital-general/digital/por-que-facebook-ha-comprado-oculus-rift-por-2-000-millones-de-dolares-solo-hay-dos-explicaciones-posibles

https://www.infobae.com/2014/07/22/1582484-facebook-completo-la-compra-oculus/

https://www.expansion.com/economia-digital/companias/2020/04/22/5e9fea17e5fdea382d8b4574.html
https://www.cnbc.com/2020/06/23/facebook-to-end-oculus-go-vr-headset-sales.html

https://hardwaresfera.com/noticias/software/facebook-compra-mapillary/

Google Street View:

https://www.google.com/intl/es-419_mx/streetview/contributors/

http://obregondigital.mx/2020/06/21/mercado-realidad-aumentada-y-realidad-virtual-para-2024-analisis-mundial-con-los-mejores-jugadores-tasa-de-crecimiento-ingresos-regiones/

https://www.lanacion.com.ar/tecnologia/coronavirus-realidad-virtual-aumentada-estan-cambiando-nuestra-nid2382522

https://www.mediapost.com/publications/article/351778/facebook-makes-oculus-for-business-readily-availab.html

https://www.oculus.com/experiences/gear-vr/1342440455828247/?locale=es_ES

Oculus Fb

https://www.eluniversal.com.mx/techbit/facebook-compra-mapillary-competidor-de-google-street-view
https://web.facebook.com/business/news/insights/3-things-marketers-need-to-know-about-ar-and-vr?_rdc=1&_rdr

https://skarredghost.com/2020/05/22/oculus-business-vr-price-availability/?fbclid=IwAR3QykZaPaO1VTm8IxBbweFZMWXXJVtUVNRPuDRmGb18ptIUSxPVhD7-2WU

https://blog.mapillary.com/news/2020/06/18/Mapillary-joins-Facebook.html
