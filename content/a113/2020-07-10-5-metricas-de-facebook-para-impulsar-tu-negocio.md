---
title:
  '5 Métricas básicas de Facebook que debes conocer para impulsar tu negocio'
abstract: 5 Métricas básicas de Facebook
description: Conoce las métricas básicas de Facebook para tus campañas
autor: Diego Villamar.
tag: Redes Sociales
main: false
header_color: 'alternate'
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/5-datos-facebbok/5-datos_sm.jpg'
image_cover: 'blog/5-datos-facebbok/5-datos%.jpg'
og_image:
  src: '/files/images/blog/5-datos-facebbok/1200x630.jpg'
  type: 'image/jpeg'
  height: 1200
  width: 630
url: https://www.dinamo.mx/a113/2020/5-metricas-de-facebook-para-impulsar-tu-negocio
keywords:
  - Redes sociales
  - Métricas
  - Campañas digitales
  - CTR
  - Estratégia
  - Retorno de inversion
publishedAt: 2020-07-10 09:03:00.152
publisher: 'https://www.dinamo.mx/'
---

El lado oscuro de la comunicación y publicidad digital: Los números no mienten,
los datos duros han sido clave en la historia del marketing, la diferencia es
que hoy no se parte de un estudio de mercado en una sala con un cristal donde
podías conocer la experiencia de tus clientes al enfrentarse a tus productos,
hoy llegamos más a fondo haciendo análisis de información. Así cuándo se empieza
a poner atención a estos temas las cifras pueden ser interesantes o frustrantes,
un laberinto de datos, pero con la visión y objetivos correctos, esta
información puede ayudarnos a tomar decisiones sobre nuestra estrategia digital
de mejor forma.

A continuación te compartimos algunas métricas básicas de Facebook que debes
conocer para impulsar tu negocio.

## FACEBOOK

### INTERACCIONES

Esta métrica es muy general y sencilla, pero esa sencillez es lo que la hace
compleja. Las interacciones miden **cualquier tipo de acción que el usuario haya
realizado con tu publicación, estado o campaña.** ¿Por qué es compleja? Porque
abarca, **reacciones, compartidos, comentarios, clics en la imagen, click en ver
más, ocultar publicación, denunciar, etc.**

Para obtener los datos que sean relevantes para medir nuestros objetivos tenemos
que indagar de manera más profunda cada uno de los aspectos antes mencionados.
Incluso esto se debe cruzar con mensajes enviados vía inbox, comentarios
respondidos, y en caso de enlaces externos, cotejar con Google analytics…

<!-- prettier-ignore -->
  ![Gráfica animada](/files/images/blog/5-datos-facebbok/interaccion-.jpg){width="500px" height="500px" loading="lazy" .block .mx-auto .mb-4}

### ALCANCE

Aquí no hay más, **el alcance es el número de usuarios que vieron tu publicación
o campaña**. Aquí viene lo interesante, existen dos tipos de alcance: el
**orgánico** y el **pagado**.

Cuando hablamos de **alcance orgánico** nos referimos a todo usuario que ve tu
publicación sin necesidad de que inviertas un solo centavo en promocionarla, sin
embargo tenemos una mala noticia, el **alcance orgánico promedio aproximado en
Facebook es de 2%**.

**Matemáticas rápidas, si tienes 100 seguidores y realizas una publicación solo
2 usuarios la verán de manera orgánica.**

Ahora, el **alcance pagado** es el número de usuarios que verán tu publicación o
campaña **a partir de la inversión de publicidad que realices**. En este sentido
ganarás mayor visibilidad entre los usuarios.

Antes de continuar, es importante señalar que tener un gran alcance no significa
tener mayor número de seguidores, interacciones, leads, etc. Y es **muy
importante que te preguntes, ¿En realidad quiero llegar a los 2.449 millones de
usuarios que existen en Facebook? ¿Mi producto o servicio es relevante para
todos los usuarios en Facebook?**

Si quieres conocer más cómo funciona el alcance y el algoritmo de Facebook te
recomendamos ver nuestra entrada nuevo algoritmo de Facebook.

<!-- prettier-ignore -->
  ![Gráfica animada](/files/images/blog/5-datos-facebbok/alcance.jpg){width="500px" height="500px" loading="lazy" .block .mx-auto .mb-4}

### IMPRESIONES

Las impresiones son el **número de veces que se muestra tu publicación o
campaña**, sin importar si se realizó alguna acción en ella y no debemos
confundirla con el alcance. Un usuario puede ver más de una vez tu publicación,
por lo tanto tus impresiones pueden ser más que tus usuarios alcanzados, veamos
la siguiente tabla

| Tu Fan Page | Impresiones (número de veces que se mostró la publicación | Alcance (usuarios alcanzados) |
| ----------- | --------------------------------------------------------- | ----------------------------- |
| Imagen      | 4                                                         | 2                             |
| Video       | 6                                                         | 4                             |
| Total       | 10                                                        | 6                             |

En la tabla anterior vemos cómo se relacionan el alcance y las impresiones. A
continuación veremos cómo las impresiones nos ayudan en la medición de objetivos
de nuestras campañas.

### CTR (click through rate)

En este punto las cosas se tornan un poco más complejas. El CTR nos permite
determinar qué tan exitosa fue nuestra campaña publicitaria y lo obtenemos
**dividiendo el número de clics de tu anuncio / entre las impresiones obtenidas
x 100**.

Es vital prestar atención a este porcentaje, ya que es un excelente indicador de
si nuestra campaña es relevante para el público al que llegamos. En dínamo
tenemos esta regla para determinar un buen CTR:

> “...So, as a rule of thumb, a good AdWords click-through rate is 4-5%+ on the
> search network or 0.5-1%+ on the display network”.

Entonces con el CTR podemos determinar el % de clicks que nuestra campaña
obtuvo.

### CTR LINK

En Facebook existe algo llamado CTR LINK, por si no fuera poco tener que
preocuparnos por el CTR, esta métrica es la que realmente importa y en la que
debes poner todo tu esfuerzo.

El CTR LINK nos indica el porcentaje de clicks que nuestro objetivo de campaña
obtuvo. Sea cual sea tu objetivo de campaña (mensaje a WhatsApp, mensaje a
Messenger, Llenar formulario, Visitar sitio web) el CTR LINK nos indicará que
tan buena interacción tiene el usuario con la acción que esperamos realice.

Tener un CTR LINK de 1.5 a 2% es genial, pero obtenerlo requiere de mucha
paciencia, capacitación y sobretodo análisis de información.

Cómo bonus por llegar hasta este punto te compartimos dos métricas que son
importantes para tu sitio web. Ten en cuenta que para poder analizar estos datos
es necesario instalar un código de Google Analytics en tu sitio.

### CONVERSIONES

Las conversiones son todas aquellas acciones de valor que esperamos que el
usuario realice dentro de nuestro sitio web. Pueden ser visitar ciertas
secciones del sitio, descargar un PDF o incluso llenar un formulario.

Las conversiones se categorizan en dos tipos: micro y macro conversiones. Sin
embargo es un tema más complejo y que requiere de mayor tiempo para ser
abordado, pero si quieres saber más
[suscríbete a nuestro newsletter](#form-mailchimp) y con gusto te compartiremos
más contenido relacionado.
<a href="/a113/2020/MVP-de-un-desarrollo-web-exitoso" target="_blank">También
puedes leer MVP de mi sitio web.</a>

Te preguntarás: ¿cómo saber cuáles son las acciones de valor para mi negocio
dentro de mi sitio web? Bueno, realiza un análisis a profundidad de lo que
esperas que tu usuario haga al llegar a tu sitio y si tienes dudas con gusto
podemos asesorarte.

<!-- prettier-ignore -->
  ![Gráfica animada](/files/images/blog/5-datos-facebbok/conversion.jpg){width="500px" height="500px" loading="lazy" .block .mx-auto .mb-4}

### % DE REBOTE

El famoso porcentaje de rebote, temido por todos. Es el porcentaje de usuarios
que llegan a tu sitio web y permanecen sin realizar alguna acción dentro del
mismo. Alguna vez has abierto alguna página y pensado: **“lo revisaré
después”**; y ese **“después”** nunca llegó. Ese es el porcentaje de rebote. Y
es muy importante prestar atención a este porcentaje, ya que lo que siempre
queremos es que nuestros usuarios interactúen con nuestro sitio, naveguen por él
y regresen.

Para finalizar, estas solo fueron algunas métricas básicas que debes conocer
para impulsar tu negocio en Facebook, sin embargo las métricas más importantes
son las que tú elijas. Las que después de un análisis a profundidad determines
que sirven para medir si alcanzaste tus objetivos o no.

Lo importante no es conocer la definición de cada una de las métricas, sino
combinarlas y realizar un análisis más detallado, que involucre estrategia,
definición de objetivos y mucha visión de hacia dónde quieres llegar con tu
negocio.

Si requieres asesoría para tu proyecto, no dudes en escribirnos, seguro podemos
colaborar contigo y seguir aprendiendo en conjunto.

**Referencias**

https://www.juancmejia.com/marketing-digital/estadisticas-de-redes-sociales-usuarios-de-facebook-instagram-linkedin-twitter-whatsapp-y-otros-infografia/

https://www.wordstream.com/click-through-rate
