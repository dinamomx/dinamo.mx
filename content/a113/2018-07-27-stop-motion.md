---
title: El arte del ‘stop motion’
subtitle: Movimientos llenos de vida
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/stop-motion/portada.jpg'
headline: El arte del ‘stop motion’
description:
  Nos apasionan las producciones que involucran esta técnica de animación, el
  stop motion nos sorprende en cada cuadro.
abstract:
  Nos adentramos en las películas que usan esta técnica de animación, te
  explicamos cómo funciona y mostramos algunos ejemplos.
url: https://www.dinamo.mx/a113/2018/stop-motion
keywords:
  - 'Stop motion'
  - 'Producción de stop motion'
  - 'producciones cuadro por cuadro'
  - 'Agenda stop motion'
publishedAt: 2018-18-27 20:20:20
---

El _stop motion_ o animación cuadro por cuadro se considera la técnica de
animación más antigua, seguro lo has notado en alguna de las películas o series
que más adelante comentaremos, hay varias versiones sobre la primer película
creada con esta técnica, la que más destaca entre ellas es ‘Gumbasia’ creada por
Art Clokey

Esta animación, es diferente a los dibujos animados o animación por computadora,
consiste en una detallada y meticulosa captura de movimiento de objetos
inanimados, a través de una serie de fotografías para dar la impresión de
movimiento en la postproducción, dichas secuencias son filmadas con el apoyo de
un director de secuencia quien se encarga de darle vida a estas criaturas.

En el siguiente video del detrás de escenas de ‘Kubo’ podemos ver parte de este
proceso.

<figure class="image">
<iframe width="871" height="490" src="https://www.youtube.com/embed/JncuykDwT8A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<figcaption>Fuente: BBC Click.</figcaption>
</figure>

Su diseño ha evolucionado de tal manera con el paso de los años al crear
personajes con características más detalladas y movimientos cada vez más
naturales, mismos que podrían dar la impresión haber sido creados por
computadora.

Cabe resaltar que el stop motion es una técnica que requiere mucho trabajo pero
que da resultados increíbles y fascina a todos con sus personajes y todo lo que
son capaces de hacer para contarnos historias.

A continuación te dejamos algunas que han usado esta técnica. ¿Cuál es tu
película o serie favorita?

### STAR WARS: The Empire Strikes Back. (El Imperio Contraataca)

**Creador de personajes:** Phil Tibbet.  
**Dirección:** George Lucas.  
**Año:** 1980.

![Fotografía del detras de camaras de STAR WARS: The Empire Strikes Back. (El Imperio Contraataca)](/files/images/blog/stop-motion/star-wars.jpg)

### Wallace y Gromit.

**Direcció:** Nick Park.  
**Año:** 1990

![Captura de "Wallace y Gromit"](/files/images/blog/stop-motion/wallace.jpg)

### The Night Before Christmas (El Extraño Mundo de Jack)

**Dirección:** Tim Burton  
**Año:** 1993

![Captura de "El extraño mundo de jack"](/files/images/blog/stop-motion/jack.jpg)

### The Fantastic Mr. Fox (El Fantástico Sr. Zorro)

**Dirección:** Wes Anderson  
Isla de Perros 2009

![Captura de "El Fantástico Sr. Zorro"](/files/images/blog/stop-motion/fox.jpg)

### Frankenweenie

**Dirección:** Tim Burton  
**Año:** 2012

![Captura de "Frankenweenie"](/files/images/blog/stop-motion/franken.jpg)

### Kubo And The Two Strings (Kubo y la búsqueda samurái)

**Dirección:** Travis Knight  
**Año:** 2016

![Captura de "Kubo y la búsqueda samurái"](/files/images/blog/stop-motion/kubo.jpg)

### Isle of Dogs (Isla de Perros)

**Dirección:** Wes Anderson  
**Año:** 2018

![Captura de "Isla de Perros"](/files/images/blog/stop-motion/perros.jpg)
