---
title: Logos del Rock
subtitle: Imágenes que suenan y cuentan historias
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/logos-rock/portada.jpg'
headline: Logos del Rock
abstract:
  Nos dimos una vuelta por los logos de rock de varias de nuestras bandas
  favoritas del género, en conmemoración por el Día Mundial Del Rock
description:
  Compartimos la historia de algunos de los logos más icónicos en la historia
  del rock.
url: https://www.dinamo.mx/a113/2018/logos-del-rock
keywords:
  - 'logos del rock'
  - 'Día del rock'
  - 'Historia del rock'
  - 'bandas de rock'
  - 'Diseño logos'
  - 'Rock y diseño'
publishedAt: 2018-07-18 20:20:20
---

El rock desde su llegada al mundo de la música generó una revolución que hasta
la fecha sigue vigente. El estilo, moda y sub corrientes alternas que han
derivado del género ha influenciado al mundo al dar una perspectiva totalmente
distinta a la que se acostumbraba antes de su llegada.

La creación de un logo para muchas de las bandas de rock que forman parte de su
gran historia se ha convertido en una manera, adicional a la música, de ser
identificadas. Combinaciones entre letras y otros símbolos, que se han
transformado en íconos históricos del género.

Muchas de estas bandas tienen un gran legado, no solo en el género, también en
la música.

A continuación te presentamos un recuento del origen de algunos de ellos.

## ACDC

AC DC es sin duda una de las bandas más legendarias del género, su nombre al
igual que su música es conocida en todo el mundo. Su logo consiste en las letras
de la agrupación con una tipografía, que se considera como un sello de la banda.

Las letras que integran su nombre, cuyo significado es _​Alternating Current_
​(Corriente Alterna) y _Direct Current_ ​(Corriente Continua) son separadas por
un rayo.

Se sabe que la idea de darse a conocer como AC DC surgió gracias a que los
integrantes consideraban que el sonido de la banda es enérgico y crudo.

![Logotipo de ACDC](/files/images/blog/logos-rock/acdc.jpg)

## Rolling Stones

Si se habla sobre rock británico esta banda sin lugar a dudas levanta la mano
para ser reconocida como una de las más icónicas, con más de 52 años de
trayectoria y aún en activo ocupan un lugar muy importante en la historia del
género.

Su icónico logotipo de una lengua descarada y burlona ha recaudado grandes
recursos al ser parte del gran merchandising que gira alrededor de la banda.

Debido a la similitud del diseño con la corriente de la cultura pop, se pensaba
que había sido creado por el famoso artista Andy Warhol, sin embargo, se comenta
que la idea del diseño fue de Mick Jagger y finalmente fue creada por el
diseñador John Pasche, en 1971.

![Logotipo de los Rolling Stones](/files/images/blog/logos-rock/rollling-stones.jpg)

## Queen

Queen representa un gran exponente del rock británico, sus canciones y la
mística de la agrupación ha sido parte de incontables referencias en la cultura
pop.

Su logo fue diseñado por el mismo Freddie Mercury quien, tras haber estudiado en
la Escuela de Artes de Londres, tenía gran afinidad por el diseño.

Dentro de sus elementos podemos ver la imagen de los signos zodiacales de los
integrantes: Leo, Virgo y Cáncer; además de la figura de un ave fénix que indica
el resurgir de sus integrantes.

Destacan los elemento de la letra “Q” en medio del logo y la corona, símbolo de
realeza y del amor de los ingleses por la reina, mismo que se representa en el
himno de su país.

![Logotipo de Queen](/files/images/blog/logos-rock/queen.jpg)

## Guns N’ Roses

Para muchos cuentan con las canciones más icónicas del género, la voz de Axl
Rose y la guitarra de Slash hicieron de este grupo de los mejores en la
historia.

Su nombre surgió de la combinación entre dos bandas, anteriores a su origen, que
reunieron a sus integrantes L.A. Guns y Hollywood Rose, lo que posteriormente
derivó en Guns N’ Roses.

Dentro de los elementos del logo se pueden apreciar las estilizadas pistolas,
convertidas en piezas centrales de la imagen de la banda, usada en otras
variantes del mismo logotipo.

Así como las rosas rojas, ​que suelen ser comunes en tatuajes con motivo del
color similar a la sangre y se interpreta como un corte suave frente a la
violencia que representan las armas, a su vez la gota de sangre se asume como
sinónimo de dolor, amenaza o peligro.

![Logotipo de Guns N' Roses](/files/images/blog/logos-rock/guns-n-roses.jpg)

## Ramones

Uno de los más icónicos logos en la historia del género, presente en playeras y
todo tipo de mercancía fue diseñado por el mexicano Arturo Vega y apareció por
primera vez en la contraportada del álbum “​Leave Home”.

Surgió a partir de una visita del grupo a Washington, donde la imagen del águila
de Estados Unidos es predominante al ser la capital del país. Al llegar al
departamento de estado, y ver el escudo surgió en el artista la idea de aplicar
modificaciones y cambiar las flechas por un bat de beisbol y el escudo del país
por flechas

Además de que se incluyeron los nombres de los integrantes: Johnny, Joey, Dee
Dee y Tommy; también se cambió la frase que acompaña el lienzo en el pico del
águila por : “Look Out Below” cuyo significado es Mira hacia abajo.

Existen otras versiones que establecen la frase de la conocida canción “Hey Oh
Let’s Go”

![Logotipo de los ramones](/files/images/blog/logos-rock/ramones.jpg)

## Soda Estereo

Considerada la mejor banda del género en latinoamérica, con gran influencia en
el rock en español, liderados por el vocalista Gustavo Cerati, agigantaron su
leyenda tras su muerte para dejar un legado de innumerables canciones que los
fans nunca olvidarán.

Tuvo diferentes logos a lo largo de su trayectoria, uno de lo más icónicos fue
el del álbum “Doble vida” (1988) que parece emula una cara sonriente con la
palabra “Soda”, misma que fue reutilizada para Languis (1989), ya con colores
integrados en la imagen.

<div class="flex">

![Logotipo de Soda Estereo](/files/images/blog/logos-rock/soda-estereo.jpg)

![Logotipo de Soda Estereo a color](/files/images/blog/logos-rock/soda-colores.jpg)

</div>

Es así como el género ha marcado la historia de la música con inolvidables
canciones y logos que engrandecen su historia y generan identidad con sus
fanáticos.
