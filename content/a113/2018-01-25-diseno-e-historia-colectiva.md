---
title: Sobre la alerta de misiles en Hawaii y cómo el diseño puede salvar vidas.
subtitle: 'Hawai vivió el pánico de ataque nuclear que nunca sucedió.'
formato: B
tag: Ciencia y Tecnología
main: true
autor: César Valadez
autor_detalle: 'cesar@wdinamo.com'
image_card: 'blog/alerta-hawaii-portada.png'
headline:
  Sobre la alerta de misiles en Hawaii y cómo el diseño puede salvar vidas.
abstract: Hawai vivió el pánico de ataque nuclear que nunca sucedió.
description: ''

url: https://www.dinamo.mx/a113/2018/diseno-e-historia-colectiva
keywords:
  - 'Ataque nuclear'
  - 'Problemas de interefaz'
  - 'UI'
  - 'UX'
  - 'Diseños de interfaz'
  - 'Magnifique'
  - 'Alerta de misiles'
publishedAt: 2018-01-25 20:20:20
---

El 13 de enero del 2018, Hawai vivió el pánico de un ataque nuclear que nunca
sucedió, omitiendo la situación política que da lugar a la necesidad de tener
alertas nucleares, y la serie de situaciones que llevó a la emisión equivocada
de una alerta de misiles atacando una zona urbana, hay un elemento en la cadena
donde el error humano es casi inevitable, la interfaz.

Es normal culpar al empleado que provocó esta catástrofe, pero la raíz del
problema no radica en la naturaleza humana de equivocarse, sino en una interfaz
confusa y propensa a detonar este tipo de errores. A través de un buen diseño de
interfaz y experiencia (UI y UX) podemos mitigar y en la medida de lo posible
corregir el error antes de que este se magnifique.

![Alerta misiles](/files/images/blog/alerta-hawai.png)

En la imagen anterior vemos la interfaz que se utiliza para enviar la alerta de
misiles. La persona encargada de realizar las pruebas de estos sistemas
accidentalmente hizo click en “PACOM (CDW) - STATE ONLY” en lugar de
“DRILL-PACOM (DEMO) STATE ONLY”, esta mínima diferencia puede confundir a
cualquier persona y el error es latente.

Por supuesto este escenario es extremo, pero en el día a día las interfaces de
usuario hacen la diferencia entre una operación exitosa (sea una compra o salvar
al mundo) o un error en ocasiones irreversible. En cada interacción
humano-máquina existe la posibilidad de mejorar y hacer procedimientos más
sencillos y amigables.

En **dínamo** **sabemos que el papel del diseño en la interfaz
humano-computadora es crucial** para poder evitar situaciones adversas, por eso
tenemos un compromiso de brindar legibilidad, eficiencia y satisfacción en la
experiencia de cada uno de los proyectos en los que trabajamos.

<small>Imágen de portada: Kyla Gifford / U.S. Air Force / Reuters</small>

Fuente:
https://hackernoon.com/redesigning-hawaiis-emergy-alert-interface-in-the-open-91c6318a7045
