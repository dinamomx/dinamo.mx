---
title: Tiempos de metamorfosis
subtitle: ''
autor: Seth Gonzalez
tag: Noticias
main: true
autor_detalle: 'seth@wdinamo.com'
image_card: 'blog/tiempos-de-metamorfosis_2/featured_lg.jpg'
image_cover: 'blog/tiempos-de-metamorfosis_2/cover%.jpg'
og_image:
  src: '/files/images/blog/tiempos-de-metamorfosis_2/ogimage.jpg'
  type: 'image/jpeg'
  height: 1200
  width: 630

headline:
abstract:
  El tema en cuestión es la pandemia por Covid 19. Los datos duros que vemos en
  redes sociales no son del todo fiables. Asimismo hemos estado bombardeados de
  cuestionamientos de todo tipo, que si el gobierno de López Obrador está siendo
  lo suficientemente responsable, los gobiernos de China y Estados unidos
  culpándose uno a otro por el virus, los más poderosos están en una carrera por
  tener primero una vacuna, pero donde residen la mayoría de consecuencias es en
  la gente de a pie, que somos casi todos.
description:
  El tema en cuestión es la pandemia por Covid 19. Los datos duros que vemos en
  redes sociales no son del todo fiables.
url: https://www.dinamo.mx/a113/2020/tiempos-de-metamorfosis
keywords:
  - covid-19
  - Slavoj Žižek
  - Byung-Chul Han
  - Alfredo Jalife Rahme
  - pandemia
  - pandemia por covid-19

publishedAt: 2020-03-13 20:20:20
---

El tema en cuestión es la pandemia por COVID-19. Los datos duros que vemos con
mayor detenimiento en redes sociales hablan de números de casos, países que
manejan de una mejor o peor forma la pandemia, vemos pérdidas millonarias a
nivel mundial.

Estos días hemos estado bombardeados de cuestionamientos de todo tipo, que si el
gobierno de López Obrador está siendo lo suficientemente responsable, los
gobiernos de China y Estados Unidos culpándose uno a otro por el virus, los más
poderosos están en una carrera por tener primero una vacuna, asimismo la gente
de a pie, que somos casi todos, opinamos con base en lo que se lee en Facebook,
Twitter, etc., es decir, una marea de desinformación, al final una gran división
y mucho comportamiento lleno de superioridad moral.

El filósofo esloveno Slavoj Žižek dice:

> “No soy utópico, no apelo a la solidaridad entre los pueblos. Al contrario,
> creo que la actual crisis demuestra que la solidaridad y la cooperación
> responden al instinto de supervivencia de cada uno de nosotros, y que es la
> única respuesta racional y egoísta que existe. No sólo para el coronavirus […]
> Como ha dicho Owen Jones, la crisis del clima mata a más gente que el
> coronavirus, sin que sintamos pánico por ello”.

Byung-Chul Han, el filósofo surcoreano dice:

> “Los países asiáticos están gestionando mejor esta crisis que Occidente.
> Mientras allí se trabaja con datos y mascarillas, aquí se llega tarde y se
> cierran fronteras… Sospechan que en el big data podría encerrarse un potencial
> enorme para defenderse de la pandemia. Se podría decir que en Asia las
> epidemias no las combaten solo los virólogos y epidemiólogos, sino sobre todo
> también los informáticos y los especialistas en macrodatos. Un cambio de
> paradigma del que Europa todavía no se ha enterado…”

El Geopolitólogo Alfredo Jalife Rahme por otro lado publica:

> “El Covid 19 no trastoca el nuevo (des)orden global tripolar (China, Rusia y
> Estados Unidos); sólo lo profundiza.”

En su artículo de la jornada el Dr. Jalife cita un artículo
de [Nahal Toosi](https://www.politico.com/staff/nahal-toosi) y [Adam Behsudi](https://www.politico.com/staff/adam-behsudi):

> “Virus pushes U.S.-Chinese relationship toward fracture”.

Aunado a lo que dicen y puntualizan estas mentes privilegiadas, es clave
observar lo que vivimos en países con tanta desigualdad social como México, raya
en lo imposible pensar que haya un aislamiento total, observemos que el año
pasado del 100% de la población laboralmente activa, el 27.5% se encuentra en el
negocio informal, asimismo el 99.7% de las empresas son pymes (micro, pequeñas y
medianas). En conjunto, generan 42% del Producto Interno Bruto (PIB) y 64% del
empleo del país (Secretaría de Economía, 2001).  
Kant sobre las leyes del Estado: *“Obedezca, pero piense, mantenga la libertad
de pensamiento”*.

![Gente doblemente vulnerable, por su condición social y por el Covid-19.](/files/images/blog/tiempos-de-metamorfosis_2/ogimage.jpg)

En lo personal creo que la construcción parte de la crítica que se acompaña de
propuestas y acciones, es entonces que en este mar de información sugiero ser
selectivos con lo que leemos, dar doble clic y llegar a las fuentes, asimismo
dar crédito a la formación académica de los informantes.

Para los ciudadanos de a pie, en nuestra cotidianidad, no nos queda del todo
claro el impacto de la caída de la bolsa de valores, lo que está claro es si hay
dinero o no para llevar a casa, para pagar los recibos, entre otras cosas, es
entonces donde hay que ejercer la libertad de pensamiento, es evidente que los
monopolios y millonarios tendrán pérdidas bastante considerables, pero les
aseguro que no va faltar comida en sus mesas, la diferencia para ellos está en
cuantas generaciones podrán vivir con la fortuna que hoy tienen, en nuestro
caso, los de abajo es un tema de vida o muerte.

Por otro lado toda construcción se inicia de abajo hacia arriba, primero hay
planos, se analiza el contexto y se construye.

Hagamos lo mismo, si consumimos desde abajo permitiremos que ese recurso se
distribuya donde más hace falta.

Hace días había una gran polémica respecto a las empresas que dieron días libres
sin goce de sueldo por la pandemia actual, entonces la sociedad reaccionó
enojada haciendo mención que no consumirían más en esos establecimientos, a mi
parecer, que mucha gente tenga como única opción trabajar en estas franquicias
monopólicas es ya una consecuencia de no ejercer nuestra libertad de pensamiento
y elegir dónde y por qué consumir en determinados establecimientos.

¿Cuántos de ustedes conocen a gente con mucho talento que han tenido que
desistir de sus sueños pues no pueden vivir de lo que aman?, ¿qué pasaría si la
gente se dedicara lo que realmente aman en sus vidas?, ¿cuánta gente ni siquiera
tiene la oportunidad de descubrir sus talentos pues no logran satisfacer lo más
básico?

Es momento de cambiar, de reinventarnos, reeducarnos, de volver a empezar; Lydia
Cacho en su libro Ellos hablan, dice *“…el camino para dejar de ser nunca
termina…”* es clave empezar.

Entonces concluyo invitando a que estos días de aislamiento mantengamos activa
nuestra libertad de pensamiento y actuemos en consecuencia.

**Fuentes**:

- **_<u>[El Pais](https://elpais.com)</u>_**: [La emergencia viral y el mundo de mañana. Byung-Chul Han, el filósofo surcoreano que piensa desde Berlín](https://elpais.com/ideas/2020-03-21/la-emergencia-viral-y-el-mundo-de-manana-byung-chul-han-el-filosofo-surcoreano-que-piensa-desde-berlin.html)
- **_<u>[culto.latercera.com](https://culto.latercera.com)</u>_**: [La columna en que Slavoj Žižek plantea que el Coronavirus “es un golpe a lo Kill Bill al sistema capitalista”](https://culto.latercera.com/2020/03/03/slavoj-zizek-coronavirus/)
- **_<u>[El nacional.cat](https://www.elnacional.cat)</u>_**: [Slavoj Žižek publica ‘Pandemic!’, el primer ensayo sobre el coronavirus.](https://www.elnacional.cat/es/cultura/coronavirus-zizek-libro-pandemic_484875_102.html)
- **_<u>[politico.com](https://www.politico.com)</u>_**: [Virus pushes U.S.-Chinese relationship toward fracture](https://www.politico.com/news/2020/03/18/coronavirus-china-trade-136188)
- **_<u>[La jornada.com](https://www.lajornada.com)</u>_**: [Putin felicita a China por su Ruta Sanitaria Global de la Seday Foreign Affairs se preocupa por EU](https://www.jornada.com.mx/2020/03/22/opinion/012o1pol)
