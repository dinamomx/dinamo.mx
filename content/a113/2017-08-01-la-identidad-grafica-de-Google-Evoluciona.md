---
title: 'La identidad gráfica de Google Evoluciona'
subtitle: 'Google ha dejado claro que no tiene intenciones de quedarse quieto.'
formato: B
tag: Cultura y Arte
autor: Diego Villamar
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/google1.jpg'
headline: Google evoluciona.
abstract:
  Google ha dejado claro que no tiene intenciones de quedarse quieto, han
  decidido evolucionar cada aspecto de su identidad gráfica, como ellos mismos
  lo explican en su detallado informe “Evolving the google identity”, el diseño
  es sólo una parte del esfuerzo y un paso en camino que están trazando hacia
  nuevos proyecto.
description:
  'Con los cambios realizados en los últimos, Google ha dejado claro que no
  tiene intenciones de quedarse quieto, han decidido evolucionar cada de su
  identidad gráfica.'

url: https://www.dinamo.mx/a113/2017/la-identidad-grafica-de-google-evoluciona
keywords:
  - 'google'
  - 'nuevos proyectos'
  - 'identidad'
  - 'grafica'
publishedAt: 2017-08-01 20:20:20
---

Con los cambios de los últimos meses, Google ha dejado claro que no tiene
intenciones de quedarse quieto, han decidido evolucionar cada aspecto de su
identidad gráfica, como ellos mismos lo explican en su detallado informe
“Evolving the google identity”, el diseño es sólo una parte del esfuerzo y un
paso en camino que están trazando hacia nuevos proyecto.

![Google](/files/images/blog/google1.jpg)
