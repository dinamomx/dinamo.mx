---
title: Quiero vender por internet, ¿cómo le hago?
subtitle: ''
autor: Diego Villamar
tag: Marketing
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/vender-internet/listado-600x760.jpg'
image_cover: 'blog/vender-internet/cover%.jpg'
og_image:
  src: '/files/images/blog/vender-internet/facebook-1200x630.jpg'
  type: 'image/jpeg'
  height: 1200
  width: 630

headline: Venta de artículos por internet
abstract:
  Quieres vender por internet y no tienes ni idea de por donde empezar.¡aquí te
  decimos como!
description:
  Sabemos que quieres vender por internet y vender en internet no es lo más
  complejo del mundo. Solo requiere orden y las herramientas indicadas.
url: https://www.dinamo.mx/a113/2020/como-vender-por-internet
keywords:
  - 'venta por internet'
  - 'marketing'
  - 'internet'
  - 'ventas'
publishedAt: 2020-03-13 20:20:20
---

Sabemos que quieres vender por internet, ahora esto nos genera más preguntas,
¿tienes objetivos claros a corto, mediano y largo plazo, conoces a tu cliente?,
tu venta es a nivel nacional?

Ahora déjame decirte que vender en internet no es lo más complejo del mundo.
Solo requiere orden, además de las herramientas indicadas, una tienda en línea,
un stock mínimo, un proceso de entrega definido además de big data del
comportamiento de los posibles clientes en tu tienda, aquí es donde nos
concentramos en esta entrada: Google Analytics.

<video class="block mb-4 mx-auto" autoplay muted loop frameborder="0" playsinline poster="/files/images/blog/vender-internet/content-b.jpg">
  <source type="video/mp4" src="/files/images/blog/vender-internet/content-b.mp4"></source>
  <source type="video/webm" src="/files/images/blog/vender-internet/content-b.webm"></source>
</video>

### Un código miles de datos duros.

Es verdad que contar con un sitio web puede ayudar a tu negocio a crecer, tanto
en ventas como en posicionamiento. Pero detrás de tu página se esconde una gran
cantidad de estándares y datos duros que es importante cuidar.

¿Pero cómo podemos acceder a este tesoro de información? Muy sencillo con Google
Analytics. Si no estás muy familiarizado con estos términos te explico un poco.
Es una herramienta que se inserta en tu página web a través de un código
Javascript. Este permite ver el comportamiento de los usuarios en tu sitio,
desde su posición geográfica, navegador, duración de sesión en el sitio, entre
muchos otros detalles que te permitirán hacer modificaciones a tu página con la
confianza que dan los datos duros.

<video class="block mb-4 mx-auto" autoplay muted loop frameborder="0" playsinline poster="/files/images/blog/vender-internet/content-a.jpg">
  <source type="video/mp4" src="/files/images/blog/vender-internet/content-a.mp4"></source>
  <source type="video/webm" src="/files/images/blog/vender-internet/content-a.webm"></source>
</video>

### Las estadísticas no son lo mismo que información.

Y es aquí donde todo se complica. Tener los datos duros del sitio solo es el
inicio de una labor mucho más robusta. No sirve de nada tener números y
estadísticas sin algo esencial: análisis. El análisis de estos datos duros nos
permitirá obtener información valiosa y relevante que nos ayude a tomar
decisiones con SUSTENTO.

> “Google Analytics arroja datos que al ser analizados se transforman en
> información”.

<!-- gráfica circular -->

### Preguntar y preguntar hasta encontrar la respuesta.

El contar con tanta información es el principio, ahora viene el trabajo duro en
el análisis de datos. ¿Recuerdas que te preguntaba si tenías claro el objetivo
de tu sitio? Bueno el tener claro qué quieres lograr con tu sitio, permitirá
definir qué métricas son importantes y cuáles no. Un ejemplo muy sencillo, si
cuentas con un carrito de compras dentro de tu sitio, querrás saber cuántos
usuarios abandonan el carrito sin realizar una compra. Analytics te dará este
número.

Pero, para encontrar una respuesta con mayor valor, más allá de decir que de 100
posibles compradores 50 abandonaron el carrito sin hacer la compra, tendrás que
profundizar más. Deberás preguntarle a la herramienta de qué página llegaron tus
usuarios, qué edad tienen, en qué lugar del mundo se encuentran, qué dispositivo
móvil utilizan y un largo etc.

Y entonces la información que vas obteniendo te permite ver que quizá no sabías
que la mayoría de tus usuarios se encuentran en México, tienen 25 años y
utilizan sistema operativo Android.

### Planificando una estrategia.

En este muy breve repaso vimos cómo esta herramienta de Google te permite saber
desde cuánto tiempo los usuarios pasan en una determinada sección de tu página,
hasta qué acciones realizaron en la misma.

Pero toda esta información debe tener un sentido no podemos olvidar que este
análisis nos debe permitir tomar decisiones estratégicas de manera online y
offline, en cuanto al código y estructura del sitio, en la operación del
negocio, el costo de los productos, puntos de venta, inventario por
temporalidad...etc.

En nuestra experiencia el análisis siempre es positivo. En un inicio podemos
identificar que acciones dieron mejores resultados y cuáles no. Esto nos
permitirá romper mitos, ideas incluso prejuicios sobre el comportamiento del
usuario a cambio de datos reales.

Y es en esta parte del blog en la que si tienes alguna duda o requieres asesoría
te pido nos eches un [mail](mailto:hola@wdinamo.com) y nos platiques de tu
proyecto. Contamos con más de 8 años de experiencia en el análisis de
información y optimización de campañas digitales. Así que no te preocupes, en
dínamo estás en buenas manos.

<a class="btn btn--orange" style="color: #fff;" href="contacto">Contiza hoy
mismo</a>
