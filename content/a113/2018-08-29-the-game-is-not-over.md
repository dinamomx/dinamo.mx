---
title: The game is not over
subtitle: ¿Quiénes son los verdaderos gamers?
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/game-over/esports.jpg'
headline: The game is not over
abstract:
  Los gamers, aquellos que dejan su vida para avanzar el siguiente nivel,
  apasionados de los clásicos, las nuevas historias y la evolución que cuentan
  el mundo de los videojuegos.
description:
  La pasión que sentimos por los videojuegos solo puede ser comparada con lo que
  significa pasar al siguiente nivel en tu juego favorito.
url: https://www.dinamo.mx/a113/2018/the-game-is-not-over
keywords:
  - 'gamers'
  - 'día del gamer'
  - 'top de videojuegos'
  - 'mejores gamers'
publishedAt: 2018-08-29 20:20:20
---

¿En qué momento alguien se convierte en un gamer? ¿qué es lo que implica? La
respuesta es disfrutar el juego, hacerlo al máximo y siempre recordar que se
trata de eso, algo que puede parecer simple pero tiene una trascendencia
importante, así que no, los juegos en línea no se pausan y los clásicos, como
Mario Bross, King of Fighter o Donkey Kong serán recordados por siempre.

![Control de nintendo classic](/files/images/blog/game-over/nintendo-controller.jpg)

A su vez uno se podría imaginar que un gamer goza de los pequeños placeres de la
vida, el avanzar de nivel/misión siempre se contempla como un reto, los juegos
que cuentan historias generan intriga o las mejoras en los gráficos representan
una imagen visual totalmente grata, al igual que la dirección de arte que los ha
llevado a ganar premios, todos estos factores nos llevan a la pregunta ¿Qué es
lo que realmente hace exitoso a un videojuego? La realidad es que, los
videojuegos desde su concepción llegaron para cambiar el mundo y ofrecer una
experiencia virtual que nunca antes nos hubiéramos podido imaginar y
precisamente el ser gamer puede estar relacionado con eso, y basándonos en el
ejemplo del libro de Ernest Cline Ready Player One, posteriormente película
dirigida por Steven Spielberg, el ser gamer es adentrarse en esa experiencia y
hacerla propia.

![Captura del climax de la película "Ready Player One"](/files/images/blog/game-over/ready-player.jpg)

Los videojuegos han evolucionado con el transcurso del tiempo, así como la forma
de jugarlos, antes se limitaba a una pantalla, un televisor y el control
conectado a la consola, ahora las posibilidades son infinitas con consolas
portátiles, controles inalámbricos, partidas en línea através de un usuario y en
cualquier parte del mundo, hasta una de las más recientes los juegos en realidad
virtual y realidad aumentada como Pokemon Go. No hay ninguna duda en que los
juegos están en constante evolución y lo gamers estarán ahí para conquistar
cualquier aventura y llegar al siguiente nivel.
