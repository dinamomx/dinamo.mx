---
title:
  El hombre que cayó a la Tierra, vuelve a las estrellas. David Bowie
  (1947-2016)
subtitle:
  David Bowie marcó a generaciones y fue uno de los íconos más importantes de la
  cultura pop.
slug: Bowie Vuelve a las estrellas
formato: B
tag: Cultura y Arte
autor: Diego Villamar
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/davidBowie.jpg'
headline: Legado de Bowie
abstract:
  David Bowie marcó a generaciones y fue uno de los íconos más importantes de la
  cultura pop, fue también un gran símbolo de la experimentación musical y como
  todo gran artista, no dejó de crear hasta su muerte, esto lo demuestra con la
  canción Lazarus del álbum Blackstar, una especie de auto-epitafio ante su
  muerte.
description:
  'Lazarus del álbum blackstar el auto-epitafio de David Bowie, uno de los
  iconos más importantes marcando generaciones dentro de la cultura pop.'

url: https://www.dinamo.mx/a113/2017/bowie-vuelve-a-las-estrellas
keywords:
  - 'David Bowie'
  - 'Lazarus'
  - 'Blackstar'
  - 'álbum'
  - 'icono'
  - 'pop'
publishedAt: 2017-06-01 20:20:20
---

David Bowie marcó a generaciones y fue uno de los íconos más importantes de la
cultura pop, fue un gran símbolo de la experimentación musical y de la estética
a partir de sus ‘reencarnaciones’ en personajes tan surrealistas como Ziggy
Stardust, Thin White Duke o el legendario Aladdin Sane.

Y como todo gran artista, no dejó de crear hasta su muerte; Tres días antes de
su fallecimiento, salió a la luz el álbum Blackstar. En Lazarus, uno de los
temas de este disco, reflexiona sobre su muerte y la resurrección desde una cama
donde reposa enfermo. Q.E.P.D David Bowie.

![David Bowie](/files/images/blog/davidBowie2.jpg)
