---
title: '¿Es Banksy un inglés llamado Robin Gunningham?'
subtitle: 'La identidad de este artista está en jaque.'
formato: B
tag: Cultura y Arte
autor: Seth González
autor_detalle: '@sethroan'
image_card: 'blog/bansky1.jpg'
headline: ¿Es Banksy un inglés llamado Robin Gunningham?
abstract: La identidad de este artista está en jaque.
description: ''

url: https://www.dinamo.mx/a113/2017/es-banksy-robin-gunningham
keywords:
  - 'Universidad Queen Mary'
  - 'Banksy'
  - 'Robin Gunningham'
  - 'Perfil geografico'
  - 'Daily Mail'
  - 'Steve Le Comber'
  - 'Spencer Chainey'
  - 'University College Londres'
  - 'Calais Francia'
  - 'Cosette'
  - 'Los Miserables'
publishedAt: 2017-08-01 20:20:20
---

Un estudio elaborado por investigadores de la Universidad Queen Mary de Londres,
basado en técnicas estadísticas utilizadas por la policía para buscar
criminales, sugiere que el misterioso artista callejero Banksy es en realidad un
inglés de unos 42 años llamado Robin Gunningham.

El estudio se basa en un análisis a más de 140 localizaciones en las que el
grafitero ha dejado alguna de sus cotizadas obras. Con esta información los
científicos crearon un mapa con “puntos calientes”, o lugares por los que Banksy
se mueve de forma recurrente.

Al comparar los datos obtenidos en ese “perfil geográfico” con la información
pública disponible, comprobaron que varias direcciones relacionadas con
Gunningham se repetían de forma constante.

Según los investigadores, Banksy acude repetidamente a un pub, un parque y un
apartamento en Bristol, así como a tres residencias de Londres.

Esta no es la primera vez que las sospechas sobre la identidad del artista
callejero se centran en Gunningham, puesto que en 2008 el diario Daily Mail ya
apuntó que ese es el nombre que se esconde tras las pinturas hechas con spray y
llenas de carga política de Banksy.

El biólogo responsable de esta investigación, Steven Le Comber, afirmó a la BBC
que se “sorprendería” si el hombre en cuestión resulta que no es el artista
callejero. “Si uno va a Google y escribe Banksy y Gunningham, le saldrán hasta
43.500 respuestas”.

En contraste a esta información Spencer Chainey, experto en Seguridad y Ciencias
Criminales en la University College de Londres, le quitó credibilidad al estudio
al afirmar a la BBC que la aplicación de esta técnica es “legítima”, pero apuntó
que no cumple con los estándares habituales de las ciencias forenses al no haber
tenido en cuenta factores como las fechas en las que se hicieron las pinturas.

La criminología sostiene que los homicidios y otros incidentes tienen lugar
cerca de donde viven los delincuentes, una hipótesis que ha sido probado
recientemente en otros campos como el rastreo de brotes de enfermedades
infecciosas.

En enero, Banksy dejó la que hasta ahora es su última huella pública, un mural
en una pared frente a la embajada francesa en Londres en el que criticó el uso
de gas lacrimógeno en un campo de refugiados cerca de Calais (Francia) por medio
de un dibujo de Cosette, una de las protagonista de la obra “Los Miserables”.

<div class="full-video"><iframe class="iframe" src="https://www.youtube.com/embed/BL3XjvhUIHc" frameborder="0" allowfullscreen></iframe></div>

![Banksy Cine](/files/images/blog/bansky2.jpg)
