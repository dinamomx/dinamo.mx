---
title: 'El océano, fuente de inspiración inagotable.'
subtitle: '#DíaMundialDeLosOcéanos'
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/oceanos_portada.jpg'
headline: El océano, fuente de inspiración inagotable.
abstract:
  Los océanos representan históricamente una fuente de creatividad que nos
  regala múltiples referencias en diferentes campos artísticos.
description:
  El océano transmite fuerza, creatividad y es motivo de creación de
  innumerables piezas artísticas.
url: https://www.dinamo.mx/a113/2018/dia-de-los-oceanos
keywords:
  - 'día del océano'
  - 'oceanos'
  - 'creatividad'
  - 'inspiración del mar'
publishedAt: 2018-06-05 20:20:20
---

El mar tiene una importancia inigualable, forma parte de industrias tan
importantes como la pesquera y la petrolera, mismas que lo contaminan y dañan su
biodiversidad al sobreexplotar sus recursos, no obstante, también es el origen
de la vida y una fuente inagotable de inspiración en la historia del arte.

A decir verdad, no hay una respuesta concreta de por qué no nos preocupamos en
cuidar algo tan preciado; perderse en su inmensidad, ver el horizonte en su
atardecer y preguntarse todos los misterios que oculta, ha sido un recurso usado
por artistas en todos los ámbitos.

Acompáñanos a darnos un chapuzón en su influencia a la música; legendarios
grupos como Led Zepellin, The Beatles, Richard Hawley y la Sonora Matancera con
su icónico tema de “En el mar la vida es más sabrosa”, que no puede faltar
cuando en México se habla de ir a la playa o visitar los bellos puertos que este
gigante azul nos ofrece a la vista.

Aquí te dejamos algunos ejemplos.

## Led Zeppelin - The Ocean

> Singing to an ocean, I can hear the ocean's roar  
> Play for free, I play for me and play a whole lot more, more!  
> Singing about the good things and the sun that lights the day  
> I used to sing on the mountains, has the ocean lost its way

<figure class="image">
<iframe class="iframe" width="1905" height="734" src="https://www.youtube.com/embed/mqgyD_yTWCU?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<figcaption>Led Zeppelin - The Ocean</figcaption>
</figure>

## Richard Hawley - The Ocean.

> You lead me down, to the ocean  
> So lead me down, by the ocean
>
> You know it's been a long time, You always leave me tongue tied  
> And all this time is for us  
> I love you just because
>
> You lead me down, to the ocean  
> The world is fine, by the ocean

<figure class="image">
<iframe class="iframe" width="1905" height="734" src="https://www.youtube.com/embed/wYoNrmJe9LA?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<figcaption>Richard Hawley - The Ocean</figcaption>
</figure>

## The Beatles - Yellow Submarine

> In the town where I was born  
> Lived a man who sailed to sea  
> And he told us of his life  
> In the land of submarines  
> So we sailed up to the sun  
> Till we found a sea of green  
> And we lived beneath the waves  
> In our yellow submarine
>
> We all live in a yellow submarine  
> Yellow submarine, yellow submarine  
> We all live in a yellow submarine  
> Yellow submarine, yellow submarine

<figure class="image">
<iframe class="iframe" width="1905" height="734" src="https://www.youtube.com/embed/m2uTFF_3MaA?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<figcaption>The Beatles - Yellow Submarine</figcaption>
</figure>

<hr>

Por otra parte como se mencionó al principio, el mar no solo nos ha sumergido en
la música, también la poesía encuentra en él una musa inalcanzable, llena de
misterios, que representa distancia por su inmensidad y puede llenarse de
elogios por todo lo que ofrece.

Pablo Neruda, uno de los poetas más importantes en latinoamérica, habla de él en
“El Mar”, mientras que el filósofo alemán Friedrich Nietzsche ofrece una
perspectiva de descubrir nuevas cosas y Nicanor Parra le canta al mar.

## Pablo Neruda - El Mar

Necesito del mar porque me enseña:  
no sé si aprendo música o conciencia:  
no sé si es ola sola o ser profundo  
o sólo ronca voz o deslumbrante  
suposición de peces y navios.  
El hecho es que hasta cuando estoy dormido  
de algún modo magnético circulo  
en la universidad del oleaje.  
No son sólo las conchas trituradas  
como si algún planeta tembloroso  
participara paulatina muerte,  
no, del fragmento reconstruyo el día,  
de una racha de sal la estalactita  
y de una cucharada el dios inmenso.

Lo que antes me enseñó lo guardo! Es aire,  
incesante viento, agua y arena.

Parece poco para el hombre joven  
que aquí llegó a vivir con sus incendios,  
y sin embargo el pulso que subía  
y bajaba a su abismo,  
el frío del azul que crepitaba,  
el desmoronamiento de la estrella,  
el tierno desplegarse de la ola  
despilfarrando nieve con la espuma,  
el poder quieto, allí, determinado  
como un trono de piedra en lo profundo,  
sustituyó el recinto en que crecían  
tristeza terca, amontonando olvido,  
y cambió bruscamente mi existencia:  
di mi adhesión al puro movimiento.

## Friedrich Nietzsche - Hacia nuevos mares

Allí quiero ir; aún confío  
en mi aptitud y en mí.  
En torno, el mar abierto, por el azul  
navega plácida mi barca.  
Todo resplandece nuevo y renovado,  
dormita en el espacio y el tiempo el mediodía.  
Sólo tu ojo — desmesurado  
me contempla ¡oh Eternidad!

## Nicanor Parra - Se canta al mar

Nada podrá apartar de mi memoria  
La luz de aquella misteriosa lámpara,  
Ni el resultado que en mis ojos tuvo  
Ni la impresión que me dejó en el alma.  
Todo lo puede el tiempo, sin embargo  
Creo que ni la muerte ha de borrarla.  
Voy a explicarme aquí, si me permiten,  
Con el eco mejor de mi garganta.  
Por aquel tiempo yo no comprendía  
Francamente ni cómo me llamaba,  
No había escrito aún mi primer verso  
Ni derramado mi primera lágrima;  
Era mi corazón ni más ni menos  
Que el olvidado kiosko de una plaza.  
Mas sucedió que cierta vez mi padre  
Fue desterrado al sur, a la lejana  
Isla de Chiloé donde el invierno  
Es como una ciudad abandonada.  
Partí con él y sin pensar llegamos  
A Puerto Montt una mañana clara.  
Siempre había vivido mi familia  
En el valle central o en la montaña,  
De manera que nunca, ni por pienso,  
Se conversó del mar en nuestra casa.  
Sobre este punto yo sabía apenas  
Lo que en la escuela pública enseñaban  
Y una que otra cuestión de contrabando  
De las cartas de amor de mis hermanas.  
Descendimos del tren entre banderas  
Y una solemne fiesta de campanas  
Cuando mi padre me cogió de un brazo  
Y volviendo los ojos a la blanca,  
Libre y eterna espuma que a lo lejos  
Hacia un país sin nombre navegaba,  
Como quien reza una oración me dijo  
Con voz que tengo en el oído intacta:  
"Este es, muchacho, el mar". El mar sereno,  
El mar que baña de cristal la patria.  
No sé decir por qué, pero es el caso  
Que una fuerza mayor me llenó el alma  
Y sin medir, sin sospechar siquiera,  
La magnitud real de mi campaña,  
Eché a correr, sin orden ni concierto,  
Como un desesperado hacia la playa  
Y en un instante memorable estuve  
Frente a ese gran señor de las batallas.  
Entonces fue cuando extendí los brazos  
Sobre el haz ondulante de las aguas,  
Rígido el cuerpo, las pupilas fijas,  
En la verdad sin fin de la distancia,  
Sin que en mi ser moviérase un cabello,  
¡Como la sombra azul de las estatuas!  
Cuánto tiempo duró nuestro saludo  
No podrían decirlo las palabras.  
Sólo debo agregar que en aquel día  
Nació en mi mente la inquietud y el ansia  
De hacer en verso lo que en ola y ola  
Dios a mi vista sin cesar creaba.  
Desde ese entonces data la ferviente  
Y abrasadora sed que me arrebata:  
Es que, en verdad, desde que existe el mundo,  
La voz del mar en mi persona estaba.

<hr>

La pintura refleja la realidad desde la mirada del artista, el mar en ella se ha
convertido en un paisaje recurrente, su indomabilidad y su calma han estado
presentes en este campo interminable de imaginación que puede girar en torno a
él.

A continuación te dejamos algunos ejemplos.

<figure class="image">
<img src="/files/images/blog/claude-monet-la-costa-salvaje-de-belle-ile.jpg" />
<figcaption><strong>Claude Monet</strong> con su pintura <strong>La costa salvaje de Belle-Ile</strong></figcaption>
</figure>

<figure class="image">
<img src="/files/images/blog/rembrandt-harmenszoon-la-tormenta-en-el-mar-de-galilea.jpg" />
<figcaption><strong>Rembrandt Harmenszoon</strong> con su obra <strong>La tormenta en el mar de Galilea.</strong> </figcaption>
</figure>

<figure class="image">
<img src="/files/images/blog/andre-derain-barcos-en-collioure.jpg" />
<figcaption><strong>Andre Derain</strong> con su cuadro <strong>Barcos en Collioure</strong> del año 1905.</figcaption>
</figure>

---Así como la pintura, la poesía y la pintura, el universo cinematográfico
también ha incluido al mar como el contexto o el protagonista de sus películas.
Lo inesperado y peligroso que puede ser lo podemos disfrutar en la cinta
dirigida por Guillermo del Toro, Pacific Rim; uno de los depredadores más
letales dentro del mar lo encontramos en Jaws; y cómo sobrevivir a un naufragio
al enfrentarte al mar en toda su inmensidad se disfruta gracias a la aventura
que se atraviesa en Life of Pi.

<figure class="image">
<img src="/files/images/blog/titanes-pacifico.jpg" />
<figcaption>
Titanes del Pacífico (2013)
</figcaption>
</figure>
<figure class="image">
<img src="/files/images/blog/jaws.jpg" />
<figcaption>
Tiburón (1975)
</figcaption>
</figure>

<figure class="image">
<img src="/files/images/blog/life-of-pi.jpg" />
<figcaption>
Life of Pi (2012) 
</figcaption>
</figure>

En conclusión, no cabe espacio para dudar que el mar es un recurso inagotable de
inspiración reflejada en la obra de todos estos artistas que lo han descrito y
se han enamorado de él, perdidos en su inmensidad y belleza; es por eso que el
#DíaMundialDeLosOcéanos hace un llamada para protegerlo y así nos siga brindando
magia en cada lugar que lo encontremos.
