---
title: 'Las mascotas del mundial.'
subtitle: 'Animadores de una misma pasión'
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/mascotas_portada.jpg'
headline: Las mascotas del mundial.
abstract:
  Te damos un breve resumen sobre la historia y las mascotas de los
  mundiales  de fútbol que han trascendido a través de la historia de este
  deporte.
url: https://www.dinamo.mx/a113/2018/mascotas-del-mundial
description:
  Las mascotas del World Cup FIFA han funcionado como una estrategia de
  marketing para los mundiales, ahora las recordamos.
keywords:
  - 'World Cup FIFA'
  - 'mascotas de los mundiales'
  - 'copa del mundo'
  - 'marketing de mundiales'
  - 'mundial'
  - 'fútbol'
publishedAt: 2018-06-05 20:20:20
---

La copa del mundo de fútbol se ha convertido en uno de los eventos deportivos
más importantes, al reunir representativos nacionales de cada continente, en una
competencia que se organiza cada cuatro años, y es capaz de paralizar y unir a
todo el mundo desde su primera celebración en Uruguay 1930.

Dicho torneo ha evolucionado con el transcurso del tiempo, desde la cantidad de
selecciones a competir, el balón de juego y las sedes, pero eso no es todo, en
1966 se dio un gran paso en la comercialización, al presentarle al mundo la
primera mascota para un evento de este tipo.

Los creadores del juego, tal y como lo conocemos ahora, no se quisieron quedar
atrás al sumar innovaciones al torneo más importante del deporte, en su primera
organización de la Copa del Mundo, sorprendieron a todos con la aparición de
“Willie”, quien consistía en un león, el cual forma parte del escudo de la
Football Association.

![“Willie”](/files/images/blog/mascotasmundial/willie.jpg)

Dicho personaje, creado por el ilustrador británico de libros infantiles, Reg
Hoye, se vistió con una playera representativa del Reino Unido y jugó al fútbol,
anecdóticamente en ese mundial la selección inglesa, fue campeona del mundo por
primera y única vez en su historia al derrotar a su similar de Alemania Federal.

Cuatro año más tarde, en México 1970, en el primer mundial televisado a color y
de manera satelital, “Juanito”, diminutivo de Juan, sentó un precedente al ser
la primera mascota humana, al presentar a un niño vestido con el uniforme de la
selección y un sombrero tradicional del país.

![“Juanito”](/files/images/blog/mascotasmundial/juanito.jpg)

Posteriormente en Argentina 1978 se acusó a la organización de plagiar el diseño
de “Juanito” por mostrarle al mundo un diseño casi idéntico de un niño de nombre
“Gauchito” con la playera albiceleste, un balón del mismo color y un sombrero
típico del país, en donde los anfitriones se coronaron campeones del mundo.

En los eventos posteriores, tanto en España 1982, como en México 1986, se
dejaron atrás las imágenes infantiles; y la comida típica del organizador pasó a
ser protagonista, Naranjito para el país ibérico se trataba de una fruta del
mismo nombre y Pique fue un picoso chile verde encargado de animar a las
selecciones competidoras.

<div class="flex">

![Pique](/files/images/blog/mascotasmundial/pique.jpg)

![Naranjito](/files/images/blog/mascotasmundial/naranjito.jpg)

</div>

En la década de los noventa el caricaturesco perro “Striker” en EUA 1994 y el
animado gallo Footix, vestido con los colores de la bandera gala, en el mundial
de Francia 1998 introdujeron personajes mejor realizados, con diseños que sin
duda muestran la evolución y la importancia que ha tenido estos personajes a lo
largo de la historia en la comercialización y marketing de dicho evento.

<div class="flex">

![“Striker”](/files/images/blog/mascotasmundial/striker.jpg)

![“Footix”](/files/images/blog/mascotasmundial/footix.jpg)

</div>

Durante el nuevo milenio también se han tenido diseños que han sorprendido, por
sumar algunos ejemplos están Ato, Kaz y Nik, criaturas extraterrestres que
aparecieron en Corea-Japón 2002, el primer mundial con sede compartida y por
otra parte están los muy recordados Goleo VI el apasionado león y su inseparable
amigo el balón parlanchín, Pille que rebotaba de emoción cada que se gritaba
¡GOL!.

<div class="flex">

![“Ato, Kay y Nik](/files/images/blog/mascotasmundial/corea-japon.jpg)

![Goleo](/files/images/blog/mascotasmundial/goleo.jpg)

</div>

En los últimos mundiales Zakumi y Fuleco, personajes creados para Sudáfrica 2010
y Brasil 2014, han aportado no solo su buen ánimo para el evento mundialista,
sino que también invitan a hacer conciencia sobre el medio ambiente, la
naturaleza, las criaturas en peligro de extinción y la importancia de atender
cuanto antes estos factores.

<div class="flex">

![“Zakumi y Fuleco”](/files/images/blog/mascotasmundial/zakumi.jpg)

![“Zakumi y Fuleco”](/files/images/blog/mascotasmundial/fuleco.jpg)

</div>

El diseño más reciente que por su actualidad encabeza la lista y los aparadores
de las tiendas departamentales es Zabivaka para Rusia 2018, consiste en un lobo
con anteojos deportivos y ropa con los colores del país sede, su diseño corrió a
cargo de la estudiante Ekaterina Bocharova y su elección fue a través de una
encuesta por internet.

![“Zabivaka”](/files/images/blog/mascotasmundial/zabivaka.jpg)

No cabe duda que estos personajes desde el león Willie hasta el lobo Zubivaka
llegaron para quedarse en la memoria de todos quienes aman el fútbol,el cual
puede considerarse el deporte más popular del mundo.
