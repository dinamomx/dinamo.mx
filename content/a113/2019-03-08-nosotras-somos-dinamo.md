---
title: Nosotras
subtitle: Día Internacional de la Mujer
formato: B
tag: Dinamo
autor: Zuly, Daniela, Viri y Tanya
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/8m-card.jpg'
image_cover: 'blog/8m.jpg'
headline: Trabajo y mujeres
abstract: Día Internacional de la mujer.
description:
  Ser mujer no debería de significar ser menos en cualquier ámbito, ser mujer
  ser valiosa y única. Aquí te compartimos nuestras ideas y sentimientos de lo
  que significa para nosotras ser mujer y formar parte de dínamo.
url: https://www.dinamo.mx/a113/2019/nosotras-somos-dinamo
keywords:
  - 'dia internacional de la mujer'
  - 'mujeres'
  - 'trabajo'
  - 'dia'
  - 'internacional'
publishedAt: 2019-03-08 20:20:20
---

En conmemoración del Día Internacional de la Mujer, compartimos nuestras ideas y
sentimientos de lo que significa para nosotras ser mujer y formar parte de
**dínamo**.

## Daniela Quiñonez - _Social Media Manager_

"¿Otra vieja? No chingues, ya tenemos muchas aquí".

Ésta fue la frase con la que me recibieron a los 10 años cuando comenzó la
temporada de fútbol americano en Cuernavaca y regresaba a entrenar con el
equipo. Tiempo atrás mi papá me preguntó sobre mis gustos deportivos, teniendo
apenas 7 años y sin saber mucho, decidí que quería jugar este deporte porque mi
hermano mayor lo hacía, él era mi ejemplo a seguir. Nunca me lo prohibieron, al
contrario, les sorprendía mi decisión y la apoyaban.

Aunque al principio fue difícil, mis esfuerzos, ganas, coraje y orgullo en el
campo me convirtieron durante 4 años en la mejor de la línea ofensiva. ¡Amaba
este deporte!

Mis coaches no tuvieron consideraciones a la hora de entrenarme; corría,
brincaba, pegaba y sudaba igual que cualquier otro niño. No miento, fue difícil.
Desde el entrenamiento hasta el partido, siempre escuchando detrás de mí:
"Recuerda esconderte el cabello dentro del casco para que no sepan que eres
niña", "¡Al 50 denle duro, mándenle dos para que lo tiren!". Es curioso que el
equipo en vez de sentirse orgulloso, me hicieran sentir que ser niña y practicar
este deporte estaba mal, que pegar como mujer no era permitido y que aparentar
ser niño haría las cosas más fácil.

Qué orgullo quitarme el casco y dejar notar mi cabello largo, donde todos
sorprendidos gritaban ¡el 50 es niña, bien jugado! Poco a poco y con esfuerzo me
gané un lugar dentro del equipo, demostré que ser mujer en un deporte catalogado
como exclusivo para hombres no era un límite. Más o menos me imagino que es así
dentro de la historia y sociedad, donde las mujeres han luchado por sus
derechos, por tener voz y un lugar igual que los hombres.

Al recordar esta historia, sobre el equipo y el apoyo, me agrada saber que estoy
en un lugar como **dínamo**. Donde ser mujer no te hace menos capaz de hacer
cosas, de dar tu punto de vista, de participar y levantar la mano cuando no
estás de acuerdo en algo. Porque aquí no existe el acoso ni la desigualdad
laboral; me gusta **dínamo** porque aquí pensar y trabajar como mujer contribuye
al crecimiento de la agencia y enriquecer el contenido que generamos día a día.

...Porque las mujeres pegamos duro, bien jugado.

## Zuleyma Martínez - _Asistente administrativo y Contable_

La maravilla de ser “Mujer en **dínamo**” … A lo largo de nuestra historia, ser
mujer ha sido muy difícil, incluso en algunas culturas castigado hasta con la
muerte.

Algunos podrán decir que eso quedó en el pasado y que los movimientos
feministas, la equidad de género y el reconocimiento a nivel internacional del
día de la mujer, son claro ejemplo de ello; sin embargo, yo que soy mujer les
podría decir que no es así.

Aunque nos cueste creerlo, aún hay mujeres que viven acoso sexual y laboral,
violencia intrafamiliar, económica, e incluso cuántos de nosotros no hemos oído
hablar de tantos feminicidios, en los diferentes estados de nuestro país.

Por lo que en lugar de “festejar” a la mujer cada 08 de marzo, hagámoslo cada
día del año. ¿Por qué esperar una fecha? Si podemos inculcar en nuestra sociedad
el respeto e inclusión de la mujer todos los días, al menos esa es la forma en
que lo veo y lo vivo, en especial desde hace algún tiempo, cuando tuve la
fortuna de encontrar un maravilloso lugar de trabajo, lleno de increíbles
historias, curiosidades, alegría, libertad y respeto.

Este curioso lugar se llama **dínamo**, aquí no sólo me dieron un puesto de
trabajo que tradicionalmente se le daba a un hombre, también me ha dado la
oportunidad de seguir preparándome y capacitándome, en este lugar lo más
importante es la gente, sus emociones, sus vivencias, sus aportaciones y
resultados; lo cual definitivamente dista mucho de las empresas
tradicionalistas.

Sin duda alguna por lugares y gente como la que hay en **dínamo** sigo creyendo
fielmente que ser mujer es maravilloso…

## Tanya Gónzalez - _Estrategia Comercial_

Nuestro género no debería de representar límites y el mundo debería de ser un
lugar en donde todas podamos tener las puertas abiertas para cumplir nuestros
sueños. Donde podamos caminar en la calle sin tener miedo. Un mundo donde ser
niña y ser mujer representa fuerza y sensibilidad y donde no todo es color de
rosa.

Hoy es un pretexto para recordar que juntas hemos logrado cosas grandiosas, que
juntas crecemos, que juntas hacemos lo extraordinario, que somos las que damos
vida y producimos el alimento más valioso que existe…

Hoy me siento feliz por trabajar en **dínamo**, un espacio donde mi voz cuenta,
donde pude alimentar a mi hija y al mismo tiempo trabajar, donde puedo ser yo
misma y decir lo que pienso, donde puedo proponer, donde puedo ser.

## Viridiana García - _Diseñadora gráfica_

Yo pienso que este día no se trata de celebrar a la mujer por el simple hecho de
serlo sino de reconocer, admitir y valorar los logros que pueda tener en sus
diferentes aspectos de la vida. Mi llegada a **dínamo** fue algo positivo para
mí, ya que llegué a integrarme a un equipo en el cual hay un apoyo y crecimiento
constante.
