---
title: 'Las marcas de la Liga MX.'
subtitle:
  Arranca el torneo Apertura 2015 de la Liga MX y los patrocinadores van
  mostrando sus propuestas para vestir a cada equipo.
formato: B
tag: Cultura y Arte
autor: dínamo
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/marcas-liga.jpg'
headline: Las marcas de la Liga MX.
abstract:
  Arranca el torneo Apertura 2015 de la Liga MX y los patrocinadores van
  mostrando sus propuestas para vestir a cada equipo.
description: ''

url: https://www.dinamo.mx/a113/2015/las-marcas-de-la-liga-mx
keywords:
  - 'Liga MX'
  - 'Patrocinadores liga MX'
  - 'Tuzos del pachuca'
  - 'Club deportico Atlas'
  - 'Puma México'
  - 'Manuel Andráde'
  - 'Nike'
  - 'Cruz Azul'
  - 'Charly Futbol'
  - 'Pirma'
publishedAt: 2015-07-24 20:20:20
---

Arranca el torneo Apertura 2015 de la Liga MX y los patrocinadores van mostrando
sus propuestas para vestir a cada equipo. Son 5 las marcas que vestirán a los
equipos de la liga y como cada temporada hay uniformes polémicos, marcas que
destacan por innovación y propuestas limpias que respetan la tradición de cada
equipo. Es bien sabido que en gustos se rompen géneros pero, para ustedes, ¿cuál
es la marca ganadora en este arranque de torneo?

<a href="http://global.puma.com/es_MX/home?locale=es_MX" target="_blank">Puma</a>
es la marca con mas presencia para esta temporada vistiendo a
<a href="http://www.clubsantos.mx/" target="_blank">Santos</a>,
<a href="http://www.clubqueretaro.com/" target="_blank">Querétaro</a>,
<a href="http://www.tuzos.com.mx/" target="_blank">Pachuca</a>,
<a href="http://www.atlasfc.com.mx/" target="_blank">Atlas</a> y
<a href="http://www.rayados.com/" target="_blank">Monterrey</a>.
<a href="http://www.charlyfutbol.com/" target="_blank">Charly</a> se encargará
de <a href="http://clubtiburonesrojos.mx/" target="_blank">Veracruz</a>,
<a href="http://www.puebla-fc.com/" target="_blank">Puebla</a> y
<a href="http://www.doradosfc.com.mx/" target="_blank">Dorados</a>, mientras
<a href="http://www.pirma.com.mx/" target="_blank">Pirma</a> será el
patrocinador oficial de
<a href="http://www.chiapasfc.com/" target="_blank">Jaguares</a>,
<a href="http://www.fuerzamonarca.com/#noticias" target="_blank">Morelia</a> y
<a href="http://www.clubleon-fc.com/" target="_blank">León</a>, Sin cambios
<a href="https://www.underarmour.com/en-mx/" target="_blank">Under Armour</a>
sigue con <a href="http://www.cruzazulfc.com/" target="_blank">Cruz Azul</a> y
<a href="http://www.tolucafc.com/inicio" target="_blank">Toluca</a> mientras
<a href="http://www.nike.com/mx/es_la/c/football" target="_blank">Nike</a>
conserva a <a href="http://pumas.mx/" target="_blank">Pumas</a> y
<a href="http://www.clubamerica.com.mx/" target="_blank">América</a>.&nbsp;<a href="http://www.adidas.mx/on/demandware.store/Sites-adidas-MX-Site/es_MX/Default-Start?cm_mmc=AdieSEM_Google-_-Trademark-Mexico-B-Exact-_-General-Trademark-X-Mexico-_-adidas+Mexico&amp;cm_mmca1=MX&amp;cm_mmca2=e&amp;gclid=CK_YpsiR9MYCFQ6raQod0w0Naw" target="_blank">Adidas</a>
conserva a
<a href="http://www.chivasdecorazon.com.mx/" target="_blank">Chivas</a>,
<a href="http://www.xolos.com.mx/en" target="_blank">Xolos</a> y
<a href="http://www.tigres.com.mx/" target="_blank">Tigres</a>.

## Destacados

La presencia de Puma en la liga mexicana se agradece ya que desde que debutó en
2004 con los Tuzos del Pachuca ha disminuido el exceso de colorido de los
diversos patrocinadores, presentando siempre propuestas limpias. Esta práctica
la han seguido diversas marcas desde entonces. Para esta temporada Atlas es un
ejemplo destacado del trabajo que hace Puma en México.

<figure>
<img src="/files/images/blog/marcas-liga-atlas.jpg" alt="Uniforme del atlas 2015">
<figcaption>El uniforme que presenta para el Apertura 2015.</figcaption>
</figure>

## Polémicos

El uniforme de Pumas para esta temporada ha sido motivo de polémica en redes
sociales.
[Existen usuarios enojados](https://www.facebook.com/Boicotplayeranikepumas?fref=ts)
con la marca norteamericana por “mancillar” la tradición de tener bien claro en
pecho el logotipo diseñado por
[Manuel Andráde](http://www.excelsior.com.mx/adrenalina/2014/04/20/954856)
en 1974.

<figure>
<img src="/files/images/blog/marcas-liga-pumas.jpg" alt="Uniforme del pumas 2015">
<figcaption>La polémica propuesta de Nike para el equipo de la Universidad Autónoma de México.</figcaption>
</figure>

Cruz Azul no se salva y a diferencia del verde brillante de la temporada pasada,
ahora presenta un naranja no menos desagradable a la vista.

<figure>
<img src="/files/images/blog/marcas-liga-cruz.jpg" alt="Uniforme del Cruz Azul 2015">
<figcaption>Cruz Azul continúa con la mala racha de 3er uniforme.</figcaption>
</figure>

## Locales

Es curioso ver como un par de marcas mexicanas se han colado entre la lista de
los grandes patrocinadores: Charly y Pirma de origen Guanajuatense están
presentes en el 30% de los equipos. Desafortunadamente con equipos que no
aparecen directamente relacionados con el éxito, sino con la medianía.

<figure>
<img src="/files/images/blog/marcas-liga-veracruz.jpg" alt="Uniforme de los tiburones 2015">
<figcaption>Charly Futbol es una de las empresas mexicanas que han invertido en la liga local.</figcaption>
</figure>

<figure>
<img src="/files/images/blog/marcas-liga-puebMor.jpg" alt="Uniforme del Puebla y morerlia 2015">
<figcaption>La otra marca mexicana que apuesta por el deporte mas popular en México.</figcaption>
</figure>

¿Tienes algun favorito? [Escribenos](/contacto).

Fuentes:

- [Club Universidad Wikipedia](https://es.wikipedia.org/wiki/Club_Universidad_Nacional)
- [El Financiero](http://eleconomista.com.mx/deportes/2015/01/29/sponsors-no-dejan-clubes-riesgo)
- [Excelsior](http://www.excelsior.com.mx/adrenalina/2014/04/20/954856)
