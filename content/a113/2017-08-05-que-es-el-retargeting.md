---
title: 'Retargeting: ¿qué es? y ¿para qué funciona?'
subtitle:
  El retargeting identifica a las personas que mostraron interés en un producto
  o servicio y en automático muestra publicidad de estos.
formato: B
tag: Ciencia y Tecnología
autor: Diego Villamar
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/retar1.jpg'
headline: ¿Qué es el retargeting?

abstract:
  El retargeting identifica a las personas que mostraron interés en un producto
  o servicio y en automático muestra publicidad de estos. Así los anuncios serán
  desplegados para mostrarles a las personas que la oferta o producto sigue
  vigente, esto se traduce en mayor probabilidad de compra.
description:
  'El retargeting identifica a las personas que mostraron interés en un producto
  o servicio y en automático muestra publicidad de estos.'
url: https://www.dinamo.mx/a113/2017/que-es-el-retargeting
keywords:
  - 'retargetin'
  - 'producto'
  - 'venta'
  - 'servicio'
publishedAt: 2017-08-05 20:20:20
---

Llegas a casa, la siguiente semana empiezan tus vacaciones y en internet buscas
viajes a Cancún, navegas por dos o tres páginas y encuentras la opción que más
te agrada. Después abres Facebook o inicias otra búsqueda, pero para tu sorpresa
ya están ahí, diferentes anuncios con ofertas de viajes a Cancún son mostrados
independientemente de la página en la que te encuentres.

Esta práctica conocida como retargeting identifica a las personas que mostraron
interés en un producto o servicio. Así los anuncios serán desplegados para
mostrarle a las personas que la oferta o producto sigue vigente.

Dentro de las ventajas de esta práctica encontramos que los anuncios son
desplegados a personas que ya mostraron interés en el producto, por lo que están
interesados y son más receptivos a la publicidad, esto se traduce en mayor
probabilidad de compra.

Además genera un retorno de inversión más alto que la publicidad tradicional, ya
que existen diferentes tipos de pago, como costo por click, costo por millar,
costo por acción, entre otros, en los cuales podemos fijar un presupuesto y
tener un mayor control sobre nuestra inversión.

Si lo que buscamos es posicionar nuestra marca, este tipo de publicidad es una
buena opción, ya que las campañas se despliegan una vez que las personas ya
visitaron nuestra página web, por lo que funcionaran como un recordatorio de que
nos visiten de nuevo.

Sin embargo en la práctica de retargeting es muy importante no saturar a nuestro
público objetivo, esto podría generar un rechazo hacia nuestra marca. También
tenemos que hacer un esfuerzo por realizar una segmentación adecuada, si bien
queremos que nuestro anuncio sea visto, tampoco nos beneficia llegar a personas
que no son nuestro público objetivo o que no representarían una fuente de
clientes potenciales, aunque nuestro anuncio se muestre pasará desapercibido.

Un anuncio con una propuesta visual atractiva, con una segmentación adecuada
podrá potencializar nuestro producto y traer nuevos clientes. Si tienes dudas
sobre esta estrategia o quieres implementarla acércate a nosotros y busquemos la
mejor estrategia para tu negocio.

![Público objetivo](/files/images/blog/retar1.jpg)
