---
title: dínamo somos nosotros
formato: B
tag: Dinamo
autor: Seth González
autor_detalle: 'seth@wdinamo.com'
image_card: 'blog/logo2019/sello_1.jpg'
headline: dínamo somos nosotros
abstract:
  Por la energía, por el movimiento, por la flexibilidad al cambio, por la
  fluidez, por no seguir las reglas, por nunca subestimar a nadie.
description: ''

url: https://www.dinamo.mx/a113/2019/logo
keywords:
  - 'dinamo'
  - 'Imagen coorporativa'
  - 'Imagen gráfica'
  - 'Diseño profesional'
  - 'Norverto Chaves'
  - 'Posicionamiento'
  - 'Isotipo'
  - 'Diseño web'
publishedAt: 2019-03-04 20:20:20
---

## Pero ¿por qué dínamo?

**Por la energía, por el movimiento, por la flexibilidad al cambio, por la
fluidez, por no seguir las reglas, por nunca subestimar a nadie.**

Somos un equipo de personas que nos caracterizamos por ser completamente
distintos, es decir, tenemos distintas energías que al fusionarse dan como
resultado un excelente trabajo en equipo.

En casi una década de trabajo hemos tenido cambios que se reflejan en nuestra
imagen gráfica. Nos hemos equivocado, nos hemos vuelto a equivocar, hemos tenido
proyectos difíciles, hemos tenido clientes exigentes. Lo más rico de todo esto
es que hemos aprendido de cada detalle, de cada cliente, de cada integrante del
equipo, hemos aprendido de nuestras coincidencias pero más de nuestras
diferencias.

Entendamos que el diseño está en constante cambio, el comportamiento de los
usuarios/clientes evoluciona, la tecnología juega un papel trascendental en la
comunicación, las relaciones son cada vez más digitales.

En este sentido la historia de nuestro logotipo es el reflejo de nuestra
historia, de nuestro proceso de evolución, de nuestra diversidad cada vez más
notoria, de nuestra experiencia.

> _“El diseño profesional de marcas gráficas es siempre específico, no sujeto a
> normas supuestamente universales ni recetas. O sea, debe administrarse cada
> caso: detectar los condicionantes particulares provenientes del perfil
> estratégico y de las condiciones específicas de comunicación.”_  
> (Norberto Chaves, 2016 -
> [Foro Alfa](https://foroalfa.org/articulos/toda-marca-debe-ser))

La imagen de **dínamo** ha evolucionado, los cambios han sido pocos, hemos
apostado por el posicionamiento, durante años usamos la misma tipografía e
hicimos algunos ajustes en nuestro isotipo.

<img src="/files/images/blog/logo2019/imagotipo-03.svg">

En el comienzo de nuestra historia estuvimos más enfocados al diseño web, de ahí
la “w” que usamos en un inicio, con el tiempo y la experiencia integramos muchos
servicios más, todo lo que hacemos está enfocado a la comunicación.

<img srcset="/files/images/blog/logo2019/logos_dinamo_1.jpg 820w,
             /files/images/blog/logo2019/logos_dinamo.jpg 1200w"
     sizes="(max-width: 650px) 90vw,
            (max-width: 1095px) 640px
            (max-width: 1300px) 830px
            (max-width: 1500px) 1024px"
     src="/files/images/blog/logo2019/logos_dinamo_1.jpg" alt="Elva dressed as a fairy">

En cuanto a color fuimos reservados, hasta fines del 2017 para darle paso a
colores más llamativos.

**En 2019 cambiamos nuestra imagen, decidimos ser congruentes con lo diferentes
que hoy somos, más diversos, con más experiencia, con más conocimiento, con
mejores clientes, con retos más grandes.**

<div class="lg:flex">
  <picture class="m-2 block bg-brand-naranja">
    <img src="/files/images/blog/logo2019/logo_B.svg" alt="Nuevo Logo" class="image">
  </picture>
  <picture class="m-2 block flex-1">
    <img src="/files/images/blog/logo2019/logo_color.svg" alt="Nuevo Logo" class="image">
  </picture>
</div> <!-- Terminan logos en plano -->

<div class="lg:flex">
  <picture class="p-2 block flex-1">
    <img src="/files/images/blog/logo2019/tarjetas_1.jpg" alt="Nuevo Logo" class="image">
  </picture>
  <picture class="p-2 block flex-1">
    <img src="/files/images/blog/logo2019/tarjetas_2.jpg" alt="Nuevo Logo" class="image">
  </picture>
</div>

<div class="p-2">
  <video src="/files/images/blog/logo2019/din.webm" autoplay loop poster="/files/images/blog/logo2019/poster.png"></video>
</div>
