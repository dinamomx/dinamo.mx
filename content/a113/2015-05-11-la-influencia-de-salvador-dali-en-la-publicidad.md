---
title: La influencia de Salvador Dalí en la publicidad.
subtitle:
  Salvador Dalí es una de las mayores influencias artísticas en el mundo de la
  publicidad.
formato: B
tag: Cultura y Arte
autor: Shamed Hernández
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/Chupa-Chups-Dali1.jpg'
headline: La influencia de Salvador Dalí en la publicidad.
abstract:
  Salvador Dalí es uno de los artistas más reconocidos en el mundo entero y tal
  vez muchos no lo sabían, pero este excéntrico y surrealista artista ayudo a la
  publicidad de marcas que ni te imaginas. ¿Te gustaría saber de qué manera
  influyo este excéntrico artista? ¡Aquí te decimos!
description:
  Dalí es uno de los más reconocidos exponentes del surrealismo y no era de
  esperarse sus obras influyeran en la publicidad. Aquí te decimos de qué manera
  lo hizo.
url: https://www.dinamo.mx/a113/2015/la-influencia-de-salvador-dali-en-la-publicidad
keywords:
  - 'Dali'
  - 'Salvador Dali'
  - 'publicidad'
  - 'influencia'
  - 'surrealismo'
  - 'campañas'
publishedAt: 2015-05-11 20:20:20
mostReaded: 5
---

Nacido en Figueiras, España, en 1904, Salvador Dalí es una de las mayores
influencias artísticas en el mundo de la publicidad. Gran participe del
movimiento surrealista, Dalí exploró diversas expresiones artísticas que lo
consagraron como uno de los más interesantes creadores de principio del siglo
XX.

La repercusión del creador influenció el cine, la escultura, la pintura,
fotografía, ilustración y por su puesto, la publicidad. Las piezas creadas por
Dalí han sido repetidas y adaptas en innumerable cantidad de piezas gráficas de
campañas publicitarias; algunas incluso siendo emblemáticas en el campo de la
publicidad gráfica.

## Campañas inspiradas.

### Lexus

Lexus tomó la pieza “La persistencia de la memoria” para reinterpretarla bajo el
eslogan “Cada pieza es una obra de arte”. El afiche gráfico fue parte de la
campaña de posicionamiento de sus automóviles en los Estados Unidos.

<figure>

![Campaña de Autos Lexus](/files/images/blog/lexus.jpg)

</figure>

### Lipton

La comercializadora de té, Lipton, también utilizó la misma pieza para
desarrollar una campaña publicitaria. Nada más refrescante que un té helado para
esos días de calor.

<figure>

![Campaña de Té Lipton](/files/images/blog/lipton.jpg)

</figure>

### Perrier

Por su parte, el agua mineral francesa Perrier elaboró 3 piezas en el 2009
inspiradas en la obra surrealista de Dalí. La campaña llevó por nombre “Melting
Campaing”, donde el agua embotellada es la solución definitiva para refrescarse
ante las oleadas de calor. Una excelente forma de adentrarse a la obra de
Salvador Dalí.

<figure>

![Campaña de agua mineral Perrier](/files/images/blog/perrier_02.jpg)

</figure>

## El logo de Chupa Chups.

Sin embargo la mayor incursión de Salvador Dalí fue el diseño del logotipo de
Chupa Chups. La compañía pasaba por un programa de expansión y en 1969 se acercó
al artista catalán para que realizará el logotipo de la empresa.

La romántica historia dice que el maestro catalán solo tardó una hora en hacer
el histórico diseño. A pesar del poco tiempo de aplicación los cambios
realizados por el artista plásticos fueron de lo más adecuados, incluso
superando el paso del tiempo. Dalí aplicó letras rojas sobre fondo amarillo
además de crear la “flor” que rodea el logotipo. Otra sugerencia que aún aplica
la marca el día de hoy fue la colocación del logotipo en la parte superior de la
envoltura.

Hasta el día de hoy, el trabajo de Dalí se considera atemporal, además de una
fuerte influencia en miles de artistas de la publicidad gráfica. Sencillamente
increíble.

<figure>

![Logo Chupa Chups](/files/images/blog/la-obra-maestra-de-salvador-dali-la-envoltura-de-chupa-chups1.jpg)

</figure>
