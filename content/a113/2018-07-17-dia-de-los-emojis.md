---
title: Una imagen con mil expresiones
subtitle: ¿Cuál es tu emoji favorito?
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/emojis/apple-1.jpg'
headline: Una imagen con mil expresiones
abstract:
  Los emojis llegaron para transformar nuestra manera de comunicarnos y han
  evolucionado hacia nuevos diseños con el pasar de los años y las plataformas
  donde se utilizan.
url: https://www.dinamo.mx/a113/2018/dia-de-los-emojis
description:
  Conoce hasta dónde han evolucionado los emojis y el uso que hacemos de ellos
  para facilitar y  enfatizar nuestra comunicación.
keywords:
  - 'Emojis'
  - 'evolución del emoji'
  - 'diseño de emojis'
  - 'día del emoji'
  - 'chats'
publishedAt: 2018-07-17 20:20:20
---

Desde la creación de los mensajes de texto en lo primeros dispositivos móviles,
cuando se hacía uso de signos de puntuación para formar expresiones como: dos
puntos y paréntesis para formar una expresión de felicidad o tristeza “:)” “:(“,
hasta hoy que parece se han vuelto parte indispensable de nuestra forma de
comunicarnos y sus diseños, sin ser complejos, son icónicos.
![Dos puntos y un paréntecis cerrado](/files/images/blog/emojis/carita-feliz.jpg)

Remontándonos a su origen, la palabra emoji viene del japonés y tiene que ver
con la unión de dos palabra “e” que significa imagen y moji que se puede
traducir como letra o carácter ¡ah que no se la sabían!. Estas imágenes
surgieron en la década de los 90’s para complementar el lenguaje japonés tan
difícil de usar de manera escrita en un teclado, así que no te sorprenda el
hecho de que muchos de ellos representan la cultura asiática a través de
imágenes de dragones, arquitectura y folclor del continente asiático.

<div class="flex">

![Emoji dragon](/files/images/blog/emojis/dragon.jpg)
![Emoji peces](/files/images/blog/emojis/peces.jpg)
![Emoji geisha](/files/images/blog/emojis/geisha.jpg)

</div>

Estos emojis, como la generación que creció se ha percatado, han evolucionado en
diseño y emociones representadas, cada vez hay más imágenes para representarlas,
además de que hay un sin fin de otros elementos como, casas , flores, árboles o
seres de otro mundo que se han apoderado de las redes sociales. Dicha evolución
ha sido en beneficio o detrimento del lenguaje escrito, sin duda es algo que se
puede debatir y que puede causar cierta polémica, ya que en algunas ocasiones
las palabras son reemplazadas por estas imágenes que se pueden prestar a un sin
fin de significados y se podría decir que corresponden a un nuevo lenguaje. Los
emojis se han apoderado del mundo y nosotros de ellos, los podemos ver presentes
en cualquier medio e incluso hasta tienen una película - que cabe mencionar fue
un fracaso en taquilla-, adicionalmente varias compañías como Apple han mostrado
innovaciones y expresiones que no encontramos en otros dispositivos, lo que se
ha transformado en una competencia por ver quién tiene más.

![Emojis de apple](/files/images/blog/emojis/apple-1.jpg)
![Emojis de apple](/files/images/blog/emojis/apple-2.jpg)

Cuéntanos ¿Cuál es tu emoji favorito?
