---
title: 7 años creando soluciones
subtitle:
  ¿Por qué dinamo? visita A113 para conocer un poco más de nuestra manera de
  trabajar. Formemos un equipo y logremos resultados.
formato: B
tag: Dinamo
autor: Tanya González
autor_detalle: 'tanya@wdinamo.com'
image_card: 'blog/somos.jpg'
headline: 7 años creando soluciones
abstract:
  ¿Por qué dinamo? visita A113 para conocer un poco más de nuestra manera de
  trabajar. Formemos un equipo y logremos resultados.
description: ''

url: https://www.dinamo.mx/a113/2017/creando-soluciones
keywords:
  - 'dinamo'
  - 'Diseño web'
  - 'Comunicación estratégica'
publishedAt: 2017-08-15 20:20:20
---

En la relación cliente-agencia es fundamental la colaboración, la disposición y
la buena comunicación entre ambas partes, para lograr óptimos resultados.

En este artículo te platicamos cómo conseguimos este equilibrio en **dínamo**.

**Para nosotros es muy importante trabajar como un solo equipo**. En **dínamo**
no nos visualizamos como un proveedor más, trabajamos como una extensión del
equipo de nuestros clientes.

No somos burocráticos e inaccesibles, por el contrario, entendemos que es
importante que estemos disponibles para nuestros clientes y nos gusta que acudan
a nosotros para ayudarles a encontrar soluciones.

**Somos confiables y transparentes**, en **dínamo** trabajamos para que haya
transparencia en la comunicación agencia-cliente, en las expectativas de los
proyectos y en los tiempos de entrega, teniendo como resultado, por un lado que
nuestros clientes confíen en nosotros y por otro que los miembros del equipo
seamos más eficientes. Preferimos fijarnos objetivos reales y no prometer
imposibles.

![hacemos](/files/images/blog/somos.jpg)

¡Nos metemos hasta en la cocina! **Nos gusta escuchar a nuestros clientes**,
conocer su historia, anécdotas, motivaciones, retos, buenos días y malos días…
nada sobra y todo aporta para construir un mejor camino juntos, proponer de
forma asertiva y tomar decisiones con mayor facilidad.

¡Papelito habla! Definimos, comunicamos y enviamos por escrito a nuestros
clientes acuerdos, fechas, alcances, presupuesto… y todo acuerdo generado, con
el objetivo de que sea fácil de tener siempre las **cosas claras y bien
comunicadas**, esto nos ha ayudado a evitar malos entendidos.

No vamos a prometerte que absolutamente todo saldrá bien, pero te aseguramos que
gracias a nuestra forma de trabajo y a cada uno de los integrantes de
**dínamo**, el trabajo juntos será provechoso, apasionante y generará buenos
resultados.
