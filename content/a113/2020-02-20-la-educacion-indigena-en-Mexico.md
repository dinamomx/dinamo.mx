---
title: La Educación Indígena en México
subtitle: Habu gä ´yo hu, ga ña hu mä hñäki.
autor: Daniela Quiñonez
formato: B
tag: Cultura y Arte
main: true
autor_detalle: 'daniela@wdinamo.com'
image_card: 'blog/educacion-card.jpg'
image_cover: 'blog/educacion-cover.jpg'
og_image:
  src: '/files/images/blog/educacion-opengraph.jpg'
  type: 'image/jpeg'
  height: 1200
  width: 630

headline: La Educación Indígena en México
abstract: Habu gä ´yo hu, ga ña hu mä hñäki.
description:
  'En México más de 7 millones 382 mil 785 personas hablan alguna lengua
  indígena, es decir, 1 de cada 10 mexicanos es indígena. La Secretaría de
  Cultura señala que existen 69 lenguas nacionales: 68 indígenas y el español,
  dicho de otra manera, somos de los 10 países con más diversidad en lenguas'
url: https://www.dinamo.mx/a113/2020/la-educacion-indigena-en-mexico
keywords:
  - 'Educacion indigena'
  - 'Secretaria de cultura'
  - 'Lenguas nacionales'
  - 'INEGI'
  - 'Sistema de Educacion Nacional'
  - 'SEN'
  - 'Inclusion de pueblos indigenas'
  - 'Instituto Nacional para la Evaluación de la Educación'
  - 'INEE'
  - 'Diversidad étnica'
  - 'UNICEF'
  - 'Juan Briseño Guerrero'
  - 'Movimiento Nacional por la Diversidad Cultural de México'

publishedAt: 2020-02-20 20:20:20
---

> “Donde vayamos, hablemos siempre nuestra lengua”.  
> **Lengua hñahñu**

**En México más de 7 millones 382 mil 785 personas hablan alguna lengua
indígena, es decir, 1 de cada 10 mexicanos es indígena. La Secretaría de Cultura
señala que existen 69 lenguas nacionales: 68 indígenas y el español, dicho de
otra manera, somos de los 10 países con más diversidad en lenguas.**

En la CDMX viven poco más de 700 mil indígenas, según el INEGI, más de 122 mil
personas de cinco años o más hablan alguna lengua originaria y la mayoría de
ellos se encuentran en los estados de Puebla, Hidalgo, Guerrero y Oaxaca. Por
otro lado alrededor de 4 millones de niños y adolescentes indígenas se
encuentran en edad escolar; si bien es cierto que las escuelas indígenas fueron
creadas para atender la gran diversidad étnica, lingüística y cultural del país,
también es una realidad la baja matrícula que existe a nivel básico.

Al inicio del ciclo escolar 2017-2018 el Sistema de Educación Nacional (SEN)
matriculó a 36.5 millones de niños y jóvenes en más de 255,000 escuelas o
planteles, convirtiéndose así en uno de los sistemas educativos más grandes del
mundo y el tercero más grande en América. Pero, **¿qué sucede con la inclusión
de los pueblos indígenas en la educación?**

![Educación Indígena en México](/files/images/blog/educacion-contenido.jpg)

> Grupo de artesanos Wäda  
> **Comunidad indígena otomí. Cardonal, Hgo.**

La población indígena tiene una tasa mayor de analfabetismo (19.2%) que el
promedio nacional (6.3%), destacando a su vez que el nivel de escolaridad
promedio es educación primaria. Por su parte, las evaluaciones realizadas por el
Instituto Nacional para la Evaluación de la Educación (INEE) arrojaron que el
aprendizaje de los niños que asisten a escuelas indígenas se encuentran por
debajo de los que asisten a otro tipo de escuela.

El servicio indígena para educación preescolar y primaria se localiza
principalmente en localidades rurales con poblaciones mayores a 250 habitantes y
con alta o muy alta marginación. Como mencionamos anteriormente, existe una baja
matrícula de alumnos en nivel preescolar, primaria y secundaria: 3.2%, 0.7% y
0.7% respectivamente.

Cabe señalar que las escuelas indígenas fueron creadas para atender la
diversidad étnica, lingüística y cultural del país, mismas que se pueden
localizar en comunidades monolingües o bilingües, cubriendo principalmente los
niveles básicos de educación. Este servicio funciona como seguidor y vigilante
de los temas que se imparten, desde que el contenido sea indicado y pertinente
en el ámbito cultural de sus comunidades hasta que los docentes hablen la misma
lengua. Sin embargo, **¿cuáles son algunos de los obstáculos a los que se
enfrentan los niños indígenas en materia educativa?**

Partiendo de la premisa anterior, el INEE y la UNICEF declaran que 1 de cada 10
escuelas no cuenta con docentes que hablen la lengua materna de la comunidad. De
igual manera, este tipo de servicio educacional se imparte solamente en
comunidades reconocidas como indígenas, dejando de lado la oportunidad de seguir
estudiando a todos aquellos niños migrantes que se han establecido en
localidades distintas a la de su origen.

Por su parte, el investigador Juan Briseño Guerrero, quien es asesor científico
del Movimiento Nacional por la Diversidad Cultural de México y que trabaja en
estudios indígenas desde hace 36 años, señala que el racismo empieza desde las
aulas, donde hablar una lengua indígena en México es considerado un atraso para
el desarrollo de la nación y señala, aún existen municipios que consideran se
debe dejar de hablar estas lenguas.

Sabemos que el trabajo del docente dentro de las aulas es un facilitador del
aprendizaje, pero a pesar de que existen libros de texto y materiales
didácticos, cuyo supuesto objetivo es favorecer la enseñanza de la lengua
originaria, el desinterés predomina. La deserción por parte de estudiantes
indígenas es más alta en Puebla, Chiapas y Yucatán donde las características y
política educativa se limita a los regímenes municipales.

Asimismo, dichas deserciones ocurren con mayor frecuencia durante segundo de
secundaria, ya sea por su condición económica o la de su comunidad, situaciones
de vulnerabilidad, carencias sociales o por ingresos.

**Y tú, ¿cuál crees que es el siguiente paso en educación para apoyar a los
pueblos indígenas?**

**En dínamo pensamos que, más que un tema de inclusión, la diversidad cultural y
el trabajo colectivo enriquecen, benefician y aportan a la comunicación y al
desarrollo del tejido social con ideas más amplias y complementarias que
transforman nuestro día a día.**

<!-- Div para no renderizar componente dentro de parrafo -->
<div>
<video-player src="https://player.vimeo.com/video/392853486" label="La Educación Indígena en México"></video-player>
</div>
