---
title: 'GPDR: La ley que cambia no sólo el internet, sino el mundo entero.'

formato: B
tag: Ciencia y Tecnología
autor: César Valadez
autor_detalle: 'cesar@wdinamo.com'
image_card: 'blog/gdpr.jpg'
headline: GPDR, La ley que cambia no sólo el internet, sino el mundo entero.
abstract:
  ¿Sabías que ahora hay una ley que obliga a las empresas a darte acceso a tus
  datos para borrarte de sus bases de datos?
description: ''

url: https://www.dinamo.mx/a113/2018/gdpr-la-ley-que-lo-cambio-todo
keywords:
  - 'GPDR'
  - 'Política de privacidad'
  - 'Datos personales'
  - 'Reglamento General de Protección de Datos de la Unión Europea'
publishedAt: 2018-06-01 20:20:20
---
## ¿Sabías que ahora hay una ley que obliga a las empresas a darte acceso a tus datos para borrarte de sus bases de datos?
No es extraño para nadie que en los últimos años la privacidad se haya
convertido en un tema de suma importancia y gran controversia; nosotros como una
empresa que desarrolla experiencias digitales y que constantemente estamos en
contacto o gestionamos información privada, nos tomamos muy en serio el manejo
de los datos de los usuarios, tanto nuestros como de nuestros clientes. Por ello
tomamos la iniciativa de informar sobre el Reglamento General de Protección de
Datos de la Unión Europea (GDPR) que entró en vigor el 25 de mayo de 2018.

> Nosotros no somos abogados, ni este es un artículo de consejo legal, nuestro
> único propósito es informar sobre esta nueva situación.

## Qué es la GDPR

Es una actualización de la Ley de Protección de Datos Personales de la Unión
Europea que protege la información pública y privada de sus ciudadanos,
empoderandose sobre el uso, guardado y distribución de sus datos.

Esta ley tiene muchas implicaciones legales y contempla situaciones muy
complejas, en este artículo les compartimos los aspectos más destacados de esta
actualización:

- **Mayor alcance territorial.** Esto significa que la legislación afecta no
  sólo a las empresas y organizaciones que operan en Europa, sino también a
  aquellas que “procesan los datos personales” de las personas que viven en la
  Unión Europea, que son la mayoría de los sitios web alrededor del mundo.
- **Consentimiento.** Todos los usuarios cuyos datos sean recopilados deben dar
  su consentimiento para realizar esta acción. Ésto no sólo aplica para los
  datos recopilados a través de formularios, también para los datos recogidos en
  segundo plano, como las direcciones IP, si se usa para identificar a un
  individuo.
- **Derecho de acceso.** Los usuarios tendrán derecho a acceder a sus datos y a
  la información sobre cómo se procesa y utiliza. Derecho a ser olvidado. Los
  usuarios tendrán derecho a que se borren sus datos y a no ser divulgados.
- **Privacidad por diseño.** Esto significa que, en lugar de acomodarse a la
  privacidad de los datos, deberá incorporarse al diseño de un sistema desde el
  principio.

## Cómo me puede afectar

Incluso una compañía mexicana sin trato comercial con Europa es afectada por
esta ley, ya que en caso de poseer en su base de datos o lista de correos,
información personal de un ciudadano europeo, al incumplir con esta ley puede
ser merecedora de una multa de hasta 4% de las ganancias anuales de la empresa o
veinte millones de euros (la cifra que sea mayor).

Y esto no se limita a la información recolectada o guardada de forma digital,
cualquier recopilación de datos como una encuesta que pueda identificar
personalmente a una persona (como el correo electrónico), tiene que ser tratado
con lo que dicta la GDPR.

## Así que por eso tengo la bandeja llena

Así es, muchas empresas en todo el mundo notaron que era más sencillo tratar a
todos sus clientes con la misma regla (la de la GDPR) que tener que tratar de
forma diferente a los usuarios de la Unión Europea y a los del resto del mundo.
Ya que no importa si te registraste hace 10 años cuando la empresa apenas pisaba
territorio en internet, ahora tienen que cumplir con esa ley.

Te acuerdas de ese sitio al que hace años te suscribiste pero ahora no para de
mandarte correos y por más que intentaste borrar tus datos no puedes, pues si
esa empresa también tomó la iniciativa de cumplir con la GDPR sin importar la
nacionalidad de sus usuarios ahora tienes una oportunidad de que no sólo te
dejen de enviar correos de promociones que no te interesan, si no que podrás
exigir que borren todos tus datos. Conclusión

Nosotros en **dínamo** consideramos esta ley como algo que no sólo necesitaba
Internet, si no que, en la era de la información es algo esencial. Sin ella
todas las empresas podrían lucrar con nuestra información sin siquiera tener una
oportunidad de saberlo.

Esta es una ley que aunque esté fuera de nuestro alcance de negocio, tenemos que
tener en cuenta y cumplir, ya que no sólo tiene unas multas impresionantes (20
millones de euros 😥) sino que el respeto de la información y la privacidad de
los usuarios de los sitios que desarrollamos es importante para nosotros.

Si necesitas saber más sobre el tema o requieres revisar si tu sitio y política
de privacidad cumplen con los requerimientos de esta nueva actualización,
[acércate con nosotros](/contacto) y con gusto te podemos asesorar.
