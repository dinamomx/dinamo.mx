---
title: Adiós paloma gris
subtitle: La palomita gris ya voló… ¿Me preocupo?
formato: B
tag: Redes Sociales
autor: Jovany Cruz, Emilia Silis, Diego Villamar e Isaac Lucio
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/paloma-tarjeta.jpg'
image_cover: 'blog/paloma-cover.jpg'
og_image:
  src: '/files/images/blog/paloma-cover.jpg'
  type: 'image/jpeg'
  height: 1890
  width: 1890
headline: Adiós paloma gris
abstract: La palomita gris ya voló… ¿Me preocupo?
description: ''

url: https://www.dinamo.mx/a113/2019/adios-a-la-paloma-gris
keywords:
  - 'Facebook'
  - 'Insignea gris'
  - 'PYMEs'
  - 'Negocio'
  - 'Emprendedores'
  - 'Auntenticidad'
  - 'Contenido de calidad'
  - 'Crecimiento'
  - 'Engagament'
  - 'Experiencia de usuario'
  - 'Servicio de calidad'
  - 'Fan page'
publishedAt: 2019-10-28 20:20:20
---

Con la decisión de Facebook de eliminar la insignia de verificación gris, ¿se
pone en riesgo la autenticidad de la cuenta de tu negocio? La respuesta, aunque
pareciera desconcertante, es no.

### Primero un poco de contexto...

La insignia gris nace en 2015 y surge con el objetivo de apoyar a las PYMEs y
emprendedores con cuentas comerciales en Facebook, para mostrar un indicador de
autenticidad. **A partir del próximo 30 de octubre de este año, las páginas con
esta característica experimentarán un cambio. Desaparece la insignia.**

En **dínamo** hemos recibido la notificación, y observamos cierta preocupación
en el medio por las implicaciones de la pérdida de este reconocimiento. Ante
toda esta información nos mantenemos escépticos.

Consideramos que lo realmente importante es tener una comunicación clara,
cumplir la promesa de cada negocio, y generar contenido de calidad, todo esto
implica muchas horas de trabajo y análisis.

No solo es cuestión de mantener la información actualizada en tu página de
Facebook, sino de generar contenido de calidad que tenga como resultado un
crecimiento en engagement con el usuario y tenga como objetivo final generar
buenas evaluaciones.

La remoción de la insignia gris no debe verse como una limitante, sino como una
motivación para que tu negocio cumpla con lo que el usuario está buscando. No
solo se trata de vender un producto como tal, sino de enfocarse a dar una
experiencia de usuario que lo motive a dejarte una recomendación en Facebook.

### Para lo cual necesitas un mix entre buena comunicación y un producto o servicio de calidad.

Seamos sinceros, como lo demostró el estudio realizado por Facebook para llegar
a esta decisión, a los usuarios les era irrelevante que las PyMes contarán con
esta insignia. Lo que afecta en realidad a tu Fan Page en Facebook, es no hacer
contenido de calidad, tener malas evaluaciones de usuarios y , en general, no
mantener cercanía con ellos.

**_Una buena evaluación y recomendación en tu Fan Page valen más que una
insignia gris._**

Fuentes:
https://marketingland.com/facebook-removing-gray-verified-badges-from-pages-later-this-month-269168
