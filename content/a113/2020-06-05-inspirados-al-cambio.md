---
title: "Inspirados\nF̶o̶r̶z̶a̶d̶o̶s̶ al cambio."
abstract:
  Con los cambios que a presentado esta pandemia, tenemos la certeza en seguir
  avanzando con optimismo al futuro.
description: Con el fin de la cuarentena regresamos a una nueva normalidad.
autor: Seth González.
tag: Noticias
main: false
header_color: 'alternate'
autor_detalle: 'seth@wdinamo.com'
image_card: 'blog/inspirados-al-cambio/card.jpg'
image_cover: 'blog/inspirados-al-cambio/transformacion%.jpg'
og_image:
  src: '/files/images/blog/inspirados-al-cambio/transformacion-facebook.jpg'
  type: 'image/jpeg'
  height: 1200
  width: 630
url: https://www.dinamo.mx/a113/2020/inspirados-al-cambio
keywords:
  - salarios
  - empresas
  - facebook
  - normalidad
  - estrategia
  - sitio web
  - analytics
  - ads
  - pandemia
  - economia
  - cambios
publishedAt: 2020-06-05 20:02:02.152
publisher: 'https://www.dinamo.mx/'
---

Son muchas las aristas de “La nueva normalidad”, por un lado mucha gente no ha
tenido oportunidad de estar en cuarentena, otras han elegido no hacerlo porque
no creen más en los gobiernos, muchos llevamos al menos un par de meses aislados
y en el mejor de los casos en un ambiente familiar saludable.

En lo que refiere a pequeñas y medianas empresas son las más desfavorecidas por
la pandemia. De las que han logrado aguantar al día de hoy el esfuerzo se
concentra en hacer más con menos y mantener los salarios, los despidos han sido
muchos, las empresas que ya no están...

Nos enfrentamos a una tríada que bien puede ser el guión de una película de
ciencia ficción, una pandemia global, una crisis económica mundial y otras
crisis emocionales individuales en masa.

¿Qué hacemos con esto?

**Echarnos para adelante con todos los cuidados, atención al detalle y buscar la
excelencia en lo que hacemos.**

Es clave pensar fuera de la caja, entiéndase que necesitamos buscar opciones que
estén más allá de nuestros límites visibles, no cabe más el “siempre se ha hecho
así”, la vida ya no es así.

<!-- prettier-ignore -->
![Dibujo de una caja conectada por puntos](/files/images/blog/inspirados-al-cambio/flecha.gif){widht="300" height="300" loading="lazy" .block .mx-auto .mb-4}

El planeta nos hace un llamado de atención y es trascendental atenderlo, debemos
cuidarlo, procurarlo, preservemos la tierra que preserva nuestras vidas.

Byung-Chul Han, filósofo coreano, en su último ensayo afirma que la conversión
de la producción y el rendimiento en valores absolutos está desvirtualizando
cada vez más a la sociedad, cree que la violencia que el ser humano ejerce
contra la naturaleza se está volviendo contra él con más fuerza, así mismo Han
considera que todo el mundo practica el culto, la adoración del yo y que la
sociedad de consumo se orienta a la satisfacción de deseos, así mismo él
sostiene que tenemos que inventar nuevas formas de acción y juego colectivo que
se realicen más allá del ego, el deseo y el consumo, y creen comunidad.

**Considero que hoy más que nunca las empresas tienen que ser colaborativas, el
trabajo en equipo debe ser colaborativo, en estos tiempos debemos confiar en la
gente, si gana uno ganamos todos, un reto grande es para los líderes, los que
están ahí por un rango jerárquico y los que se han ganado ese puesto, los
líderes hoy más que nunca deben incentivar y sacar lo mejor de cada integrante,
entiéndase que el trabajo colaborativo no es todos hacemos lo mismo, es todos
somos diferentes y desde nuestras diferencias y responsabilidades aportamos al
bien común cuidando nuestro espacio de trabajo que va desde nuestro escritorio
hasta el polo opuesto del planeta. Insisto, hay que trabajar para la
excelencia.**

Veamos que en esta pandemia nos hemos forzado a cambiar el paradigma de que la
gente no puede trabajar desde casa, muchos haciendo home office, se han
disciplinado, todo aquel que está en una zona de confort trabajando desde un
acto consciente valora el beneficio, es importante que los colaboradores se
sientan integrados, valorados, la gente no es reemplazable; la consecuencia es
una mayor productividad y un buen ambiente laboral.

<!-- prettier-ignore -->
![Cartel "Forzados al cambio" con la palabra Forzados reemplazada por Inspirados](/files/images/blog/inspirados-al-cambio/inspirados.jpg){width="800" height="450" loading="lazy" .block .mb-4 .mx-auto}

Si bien hablamos del trabajo en equipo y colaborativo, las empresas deben
interesarse y ocuparse genuinamente en ser parte de la solución y no en parte
del origen del problema, el tema económico y la crisis emocional son mundiales,
hay que invertir en el bienestar de nuestra gente, hoy más que nunca, y
trabajado en equipo, no podemos permitirnos que nuestros colaboradores estén
pasando por momentos difíciles solos, es clave acompañarnos como empresa, como
compañeros de equipo, esa es una gran diferencia.

La base de la transformación es la gente, es fundamental acercarnos a
especialistas que nos puedan ayudar a transitar la crisis, a mejorar nuestro
entorno laboral y a desarrollar a nuestros líderes, un líder no puede ser quien
da instrucciones únicamente. Se requiere de líderes que trabajen por el bien
común, que prediquen con el ejemplo, solidarios, preparados, empáticos y
orientados a la solución.

Pasando al terreno de lo digital, el reto es muy grande pues hay cosas que son
insustituibles y que seguro retomaremos una vez pase el pico de la pandemia,
socializar, abrazarnos, retomar y/o reinventar rituales sociales que nos dan
cordura.

Al pensar fuera de la caja, podremos ver que nuestros negocios se pueden
reinventar, es clave planear, tener una estrategia con objetivos reales y
medibles, trabajar en una cultura de excelencia, poner empeño, no desistir,
analizar la información, acercarse a expertos en distintas áreas, buscar
partners que nos puedan fortalecer, es trascendental ser impecables con nuestras
palabras.

El filósofo y lingüista Noam Chomsky se refiere al Coronavirus como "Otra falla
masiva y colosal de la versión neoliberal del capitalismo”.

Slavoj Zizek considera que "No habrá ningún regreso a la normalidad”, el
filósofo esloveno analiza la catástrofe mundial que desató el Coronavirus como
una oportunidad para instalar un nuevo sistema social “comunista” que reemplace
al “Nuevo Orden Mundial liberal-capitalista”.

**En conclusión considero que todas las empresas pequeñas y medianas tenemos el
reto de generar una comunidad al interior de nuestras empresas y para lograrlo
los modelos de empresas en términos de comunicación, logro de objetivos,
economía, calidad de vida deben ser en ambas direcciones colaboradores -
empresa, empresa - colaboradores, el trabajo colaborativo en busca de la
excelencia donde todo el ecosistema gane es nuestro nuevo reto.**

**Es momento de ver nuevas formas de economía, como la economía circular que
busca NO más producir, usar y tirar, donde el objetivo es reducir, reusar y
reciclar. O la economía Naranja; implica convertir el talento en dinero a través
de proyectos que conviertan las ideas en acciones productivas, fomentando la
creatividad, las habilidades y el ingenio de los emprendedores, etc.**

**Los jóvenes y las nuevas generaciones cargarán las consecuencias de
generaciones y generaciones, por ello es trascendental hacer un frente común
donde nos salgamos de la caja como sociedad y nos reinventemos en el proceso.
Sabemos que no podemos cambiar al mundo pero sí podemos empezar a hacerlo.**

Referencias:

- [_Noam Chomsky y el coronavirus_ **pagina12**](https://www.pagina12.com.ar/261649-noam-chomsky-y-el-coronavirus-otra-falla-masiva-y-colosal-de)
- [_“El dataísmo es una forma pornográfica de conocimiento que anula el pensamiento”_ **el pais**](https://elpais.com/cultura/2020/05/15/babelia/1589532672_574169.html)
- [_Pequeñas y medianas empresas, las más afectadas por la pandemia_ **jornada**](https://www.jornada.com.mx/ultimas/economia/2020/05/12/pequenas-y-medianas-empresas-las-mas-afectadas-por-la-pandemia-6730.html)
- [_Pensar fuera de la caja_ **wikipedia**](https://es.wikipedia.org/wiki/Pensar_fuera_de_la_caja)
- [_¿Qué hace que la vida sea vivible? ¿Qué constituye un mundo habitable?_ **Cultura.Unam/Facebook**](https://www.facebook.com/Cultura.UNAM.pagina/videos/255335428868133)
