---
title: '#Tipografia: La historia de la fuente Arial'
subtitle:
  'Arial es una de las tipografías más conocidas en la actualidad. A
  continuación conoce la breve historia de la familia tipográfica Arial..'
formato: B
tag: Ciencia y Tecnología
autor: Gustavo Galvan
autor_detalle: 'hola@wdinamo.com'
headline: Arial, la más famosa por excelencia
abstract:
  Arial es una de las tipografías más conocidas en la actualidad. Prácticamente
  cualquier trabajo escolar tiene se tiene como requerimiento ser  impreso en
  “letra Arial a 1.5 de espacio”, sin embargo no siempre fue así. Conoce aquí la
  breve historia de la familia tipográfica más famosa del mundo.
description:
  'Arial es una de las tipografías más conocidas en la actualidad. Conoce aquí
  la breve historia de la familia tipográfica más famosa del mundo.'

url: https://www.dinamo.mx/a113/2015/tipografia-la-historia-de-la-fuente-arial
keywords:
  - 'Arial'
  - 'tipografía'
  - 'letras'
  - 'historia'
publishedAt: 2015-04-24 20:20:20

image_card: 'blog/arial-vs.jpg'
---

**Arial** es una de las tipografías más conocidas en la actualidad.
Prácticamente cualquier trabajo escolar tiene que ser impreso en _“letra Arial a
1.5 de espacio”_, sin embargo no siempre fue así. A continuación conoce la breve
historia de la familia tipográfica Arial.

Corría el año de 1982 cuando Robin Nicholas y Patricia Saunders trabajaban para
Monotype Imaging, su labor primaria era buscar una fuente tipográfica que se
adaptara a la impresión láser de IBM. El resultado fue llamado originalmente
Sonoran San Serif, rindiendo cierto homenaje a las familias sans serif, pero en
1992 Microsoft la renombró con el nombre Arial y la incluyó en Windows 3.1 donde
obtuvo gran popularidad.

Desde entonces, la familia Arial logró volverse un estándar dentro de los
parámetros de impresión, todo gracias a la época de bonanza del sistema
operativo Windows. Dentro de los valores más importantes de la fuente están su
facilidad de lectura y adaptabilidad para impresos y pantallas. Hoy podemos ver
aplicada la tipografía en periódicos, revistas, anuncios y libros.

![Arial vs Helvética](/files/images/blog/arial-vs.jpg)

## Arial vs Helvética

Mucho se ha escrito sobre la paternidad de Arial. Algunos señalan a la fuente
Helvética como la inspiración para su nacimiento. La extrema similitud entre
ambas familias solo logra ser visible por los redondeados acabados, curvas
suaves y peso en los extremos de las letras “c”, “e”, “g”, “t” y “s”. Algunos
atribuyen el éxito de Arial gracias al impulso que le dio Microsoft y que la
llevo a la estandarización.

Hoy en día todavía se maneja la historia de que Microsoft, al no poder pagar la
licencia de Helvética, decidió apoyar la fuente Arial. Si es verdad o mentira
hoy la mayoría de nosotros seguimos apreciando la facilidad de uso de las letras
en la familia tipográfica Arial.

<figure>
<iframe width="560" height="315" src="https://www.youtube.com/embed/wkoX0pEwSCw" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
<figcaption><a href="https://www.youtube.com/watch?v=wkoX0pEwSCw">Mira el trailer de la película dedicada a la tipografía Helvética</a></figcaption>
</figure>
