---
title: '#Diseño: La evolución del escudo del Club América.'
subtitle:
  'En casi 100 años de historia el escudo del Club América paso por varias
  modificaciones en su diseño, conócelas a continuación.'
formato: B
tag: Cultura y Arte
autor: Gustavo Galvan
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/america.jpg'
headline: Club ámerica en el tirmpo
abstract:
  Con casi ya 100 años de historia, uno de los clubs deportivos más importantes,
  controversiales y reconocidos del futbol soccer ha tenido varias
  modificaciones en su diseño hasta llegar al que hoy en día conocemos,
  transformaciones no solo en el color, si no en el diseño entero. Aquí te
  mostramos que tanto ha cambiado desde sus inicios.
description:
  El Club América de México es uno de los clubes de fútbol más importantes del
  continente y con casi 100 años de historia, el diseño de este ha tenido varias
  modificaciones.

url: https://www.dinamo.mx/a113/2015/diseno-la-evolucion-del-escudo-del-club-america
keywords:
  - 'club america'
  - 'futbol, america'
  - 'futbol soccer'
publishedAt: 2015-05-26 20:20:20
---

El **Club América de México** es uno de los clubes de fútbol más importantes del
continente. En su palmares encontramos 12 campeonatos locales, 6 copas
nacionales, 6 copas de campeón de la Concacaf y 2 copas Interamericanas. Fundado
el 12 de octubre de 1916, el escudo del club paso por varios diseños en sus casi
100 años de historia.

El escudo del equipo de fútbol más popular de México mantiene 3 rasgos
característicos: las letras “C” y “A”, acrónimos de Club América; la imagen del
continente americano y el círculo que encierra los dos anteriores elementos.
Estos signos son parte de la identidad gráfica del equipo.

<figure>

![1er Escudo del América](/files/images/blog/america-1er-escudo.jpg)

<figcaption>1er Escudo del América</figcaption>
</figure>

En su origen, el América fue conformado por jóvenes estudiantes menores de 20
años. En su primera participación en un torneo de a Liga del Distrito Federal no
obtuvo el éxito deportivo deseado, dando paso a la integración de jugadores de
varios colegios y un cambio de nombre e identidad: Club Unión.

A nivel diseño el escudo sufre cambios considerables y va por el camino de la
sencillez. Una “C” y una “U” son el único referente del nombre. En una segunda
etapa intentan un escudo mucho más elaborado que raya en lo ilegible, una
tendencia demasiado exagerada para la época. Poco tiempo después regresarían al
escudo original.

<figure>

![Escudo del Centro Unión.](/files/images/blog/america-centro-u.jpg)

<figcaption>Escudo del Centro Unión.</figcaption>
</figure>
<figure>

![Escudo del Centro Unión.](/files/images/blog/america-CUnion.jpg)

<figcaption>Escudo del Centro Unión.</figcaption>
</figure>

Por un corto período, en el año de 1938 y por un corto período de tiempo, el
Club América cambiaría de escudo. Tal vez con aires premonitorios o
casualidades, pero en el escudo apareció un águila. En esta ocasión el trazado
original fue encerrado en un triángulo con un águila. El uso fue común en la
década de los 40’s, incluso ocupado en la elaboración de banderines para
intercambio entre rivales deportivos. Por cuestiones de imagen, este diseño
duraría una sola temporada para regresar al diseño base.

<figure>

![En 1938 presentan el primer escudo con un águila como elemento.](/files/images/blog/america-1938.jpg)

<figcaption>En 1938 presentan el primer escudo con un águila como elemento.</figcaption>
</figure>

La década de 1940 fue particularmente difícil para el equipo de fútbol. Pero
también significó su entrada al fútbol profesional. Con el paso de los años y
los buenos torneos el éxito profesional finalmente llegó. En 1959 el equipo fue
comprado por Emilio Azcárraga Milmo, después presidente de Televisa, quién le
inyectó capital al proyecto deportivo.

Sobre el escudo tenemos un regreso a los elementos básicos pero con un toque
mucho más estilizado. El círculo que rodeaba al continente americano paso a ser
un balón, lo cual los significaba como equipo de fútbol profesional. El trazado
es más delgado y el color amarillo se vuelve mucho más fuerte.

<figure>

![Escudo del America en los 70's](/files/images/blog/america-70s.jpg)

<figcaption>Escudo de los años 70’s que marcó la tendencia de diseño hasta la actualidad.</figcaption>
</figure>

Los siguientes diez años son considerados la “época dorada” del Club América. En
diez torneos cosecharon 5 campeonatos, además de tener sus primeros logros a
nivel internacional. Sobre el escudo vemos cambios en la posición del continente
americano, el cual pasa a una posición más al centro dejando libre la letra “A”.

<figure>

![Escudo del America en los 80's](/files/images/blog/america-80s.jpg)

<figcaption>Escudo de la “época dorada” del Club América.</figcaption>
</figure>

En los años 90’s el escudo recibió cambios en el tamaño de los elementos y la
posición del continente. Tenemos un resultado mucho más agradable visualmente,
incluso que permite ver con claridad cada elemento de la identidad gráfica y
respetando la tradición del equipo de fútbol.

<figure>

![Escudo del America en los 90's](/files/images/blog/america-90s.jpg)

<figcaption>Escudo de la década de 1990.</figcaption>
</figure>

El cambio más reciente se dio en el 2010. Podemos ver un trazado mucho más
grueso y un manejo mucho más sintetizado de los elementos gráficos. El aumento
del grosor de los elementos gráficos resalta el contraste generado por el color
amarillo, aumentando el peso en los elementos más representativos de la larga
historia del escudo: el continente americano, el círculo que encierra la
composición y las letras “C” y “A”.

<figure>

![Escudo del America en los 90's](/files/images/blog/america-2010ame.jpg)

<figcaption>Escudo de la década de 1990.</figcaption>
</figure>

Justo ahora puedes seguir disfrutando del llamado “juego del hombre”, claro
ahora conociendo un poco sobre los pequeños cambios que sufre un escudo a lo
largo del tiempo. Un tema aparte son los uniformes y como también corresponden a
tendencias del diseño de la época, algo que esperamos contar más adelante.
Mientras tanto #OdíameMás.
