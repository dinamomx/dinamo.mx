---
title: ¿Compras en físico? Una cosa del pasado…
subtitle: ¡Vámonos de shopping sin salir de casa!
autor: Isaac Lucio
formato: B
tag: Ciencia y Tecnología
headerIsDark: true
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/compra-en-linea.jpg'
image_cover: 'blog/compra-en-linea.jpg'
og_image:
  src: '/files/images/blog/compra-en-linea.jpg'
  type: 'image/jpeg'
  height: 1920
  width: 1080
headline: ¿Compras en físico? Una cosa del pasado…
description:
  El aumento en México de las compras en línea son una gran oportunidad para los
  negocios interesados en vender más y mejor.
abstract:
  Las compras en línea se han posicionado en los últimos años como una opción
  cómoda y segura para los mexicanos, principalmente a través del uso de
  diferentes plataformas de  e-commerce.
url: https://www.dinamo.mx/a113/2019/compras-en-fisico-una-cosa-del-pasado
keywords:
  - 'Cómo vender en línea'
  - 'Compras en línea en México'
  - 'Mercado Libre'
  - 'E-commerce'
  - 'ventas en línea'
  - 'Marketing para mi negocio'
  - 'marketing'
  - UX
publishedAt: 2019-11-19 20:20:20
---

Levantarse, tomar la billetera, guardar las tarjetas y emprender el viaje a la
tienda para hacer las compras, aunque no lo parezca, cada vez puede ser menos
común, por una simple razón: Las compras en línea.

Esta actividad cada vez alcanza un mayor sector entre el comportamiento de los
consumidores en México, por varias y, admitámoslo, muy comprensibles razones,
algunas de las más comunes son el ahorro en tiempo (53%), entrega a domicilio
(58%) y que en las tiendas online a veces sale más barato (48%).

Sumado a eso, el posicionamiento de algunas marcas de tiendas en línea por el
gran servicio y popularidad de la marca, generan mayor confianza al momento de
tomar la decisión de compra, desde el 2016 hasta el 2018 el crecimiento de
usuarios dentro de Amazon.com y Mercado Libre se ha visto elevado posicionándose
en los dos primeros lugares.

| **Año**            | **2016** | **2017** | **2018** |
| ------------------ | -------- | -------- | -------- |
| **Mercado Libre**  | 18,120   | 16,729   | 26,817   |
| **Amazon**         | 7,173    | 7,477    | 13,548   |
| **eBay**           | 5,516    | 6,687    | 8,583    |
| **Walmart México** | 6,028    | 7,004    | 7,981    |
| **Wish**           | 2,946    | 2,054    | 7,568    |

(2018, Forbes México)

Pero, ¿qué compran los usuarios?, ¿es seguro?, de acuerdo a la **Asociación
Mexicana de Venta Online (AMVO)**, la ropa ocupa el primer lugar (79%), seguido
por negocios de comida a domicilio (68%), tecnología (66%), productos para el
hogar (64 %) y juguetes (60%).

En nuestra experiencia, este comportamiento de los usuarios es en gran parte
detonado por el aumento en el uso de redes sociales, ya que cada vez hay más
negocios anunciados de esta forma, además de que el hecho es causa del aumento
anual en el gasto promedio de los usuarios de e-commerce.

Y tú, ¿sigues creyendo realmente que no vale la pena que tu negocio esté en
redes sociales?
