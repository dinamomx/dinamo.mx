---
title: Diseño por todos lados
subtitle:
  Se acercan eventos muy importantes de diseño. Te compartimos los más
  atractivos.
formato: B
tag: Cultura y Arte
autor: Diego Villamar
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/portadaDiseno.jpg'
headline: El diseño viene con todo
abstract:
  Se aproximan grandes y muy buenos eventos de diseño en la ciudad, Aquí te
  decimos donde.
description:
  Se acercan eventos muy importantes de diseño. Te compartimos los más
  atractivos.

url: https://www.dinamo.mx/a113/2017/eventos-diseno
keywords:
  - 'Diseño'
  - 'CDMX'
  - 'eventos'
publishedAt: 2017-10-04 20:20:20
---

En la CDMX tendremos el **DECIMOPRIMER CONGRESO NACIONAL DE TIPOGRAFÍA EN
MÉXICO**. En su onceava edición Tipografilia promete ser un espacio interactivo
para todos aquellos diseñadores con gusto por las letras y el diseño editorial.

Talleres nacionales e internacionales, presentaciones, exposiciones,
demostraciones, proyecciones y concursos son algunas de las actividades que en
las que podrás participar.

![Abierto del diseño](/files/images/blog/abiertoDiseno.jpg)

**Sede:** Centro Histórico de la CDMX  
**Fecha:** 18 a 22 de octubre
**Página:**[https://abiertodediseno.mx/](https://abiertodediseno.mx/)

Cambiando de aires, en Jalisco estará presentándose el **Design Fest
Guadalajara**. En su onceava edición este festival se consagra como uno de los
más grandes en el país y latinoamérica. Las temáticas son diversas y abarcan
desde el Diseño de Mobiliario, Automotor, Ilustración, Branding, Animación,
Videojuegos, hasta el Diseño Culinario.

![Design Fest](/files/images/blog/designFest.jpg)

**Sede:** Teatro Diana, Guadalajara, Jalisco  
**Fecha:** 19 a 21 de octubre.  
**Página:** [https://design-fest.com/](https://design-fest.com/)

Por último y regresando a la CDMX, a finales de octubre podrás asistir al
**INFOVIS**, es el Primer Congreso Internacional de Comunicación Visual dedicado
a la Infografía y a la Visualización. En este congreso destaca la participación
de Fernando Baptista y Alberto Lucas, ambos editores gráficos de la revista
National Geographic. Sin duda una oportunidad muy importante de aprender y
aplicar nuevos conceptos a tu trabajo.

![Infovis](/files/images/blog/infovis.jpg)

**Sede** Centro Cultural Universitario Tlatelolco, CDMX **Fecha** 30 y 31 de
octubre  
**Página** [http://infovis.com.mx/](http://infovis.com.mx/)

Ya están enterados, si tienen oportunidad asistan a cualquiera de estos eventos
y cuéntenos qué tal estuvo y qué cosas nuevas aprendieron. Nos leemos en nuestra
próxima entrada.
