---
title: Mujeres y decisiones que cambian la historia.
subtitle: Un gran paso en la historia de las mujeres
formato: B
tag: Cultura y Arte
autor: Isaac Lucio
autor_detalle: 'isaac@wdinamo.com'
image_card: 'blog/voto-mujer.jpg'
headline: Mujeres y decisiones que cambian la historia.
abstract:
  Conmemoramos un nuevo aniversario en la proclamación del voto para las
  mujeres, destacando su importancia en la sociedad.
url: https://www.dinamo.mx/a113/2018/voto-mujer
description:
  Porque son parte fundamental en la toma de decisiones de nuestra sociedad,
  recordamos la primera vez que la mujer votó libremente en México.
keywords:
  - 'Voto de la mujer'
  - 'Voto de mujer'
  - 'Día de la mujer'
  - 'votaciones'
  - 'sistema electoral'
publishedAt: 2018-06-18 20:20:20
---

La búsqueda de un cambio en favor de la igualdad entre mujeres y hombres es un
tema que aún involucra polémica por los pocos cambios que se han hecho en
materia de oportunidades laborales, seguridad y educación, lucha que no se ha
detenido y se puede remontar a una de las más destacadas en la historia del
país, la del voto popular.

El proceso comenzó a gestarse el 13 de enero de 1916 durante el primer congreso
feminista en México, en el que se acordó exigir el voto ciudadano de las mujeres
a través de una propuesta enviada por Hermila Galindo, secretaria particular del
entonces presidente Venustiano Carranza, a pesar de que dicha iniciativa fue
declinada por una amplia mayoría sentó un precedente para su lucha.

Misma que continúo tras varios años de constante debate, hasta que en 1923 el
estado de Yucatán hizo historia al darle a Elvia Carrillo Puerto el cargo de
diputada por el V distrito, hecho que representó un importante avance, no
obstante, después de dos años en el cargo se vió obligada a renunciar por el
clima hostil al que se vio enfrentada durante su mandato.

Décadas después y todavía sin haber conseguido el anhelado objetivo, el 6 de
abril de 1952 el Parque 18 de marzo ubicado en la Ciudad de México, fue testigo
de **más de 20 mil mujeres reunidas** con la exigencia hacia el candidato
presidencial Adolfo Ruiz Cortinez, de tener participación en la toma de
decisiones, a lo que éste respondió ofreciéndoles una ciudadanía libre de
restricciones.

En el transcurso de poco más de un año, en la fecha histórica de 3 de julio de
1953, el presidente electo Adolfo Ruiz Cortinez, lanzó un decreto constitucional
en el artículo 34, para que la mujer pudiera ejercer su derecho al voto, mismo
que establece que los varones y las mujeres de la República son ciudadanos con
derecho al voto, bajo los requisitos de mayoría de edad -18 años - y modo
honesto de vida.

Fue en las elecciones, a cargo de diputados federales para la XLIII Legislatura,
celebradas el 3 de julio de 1955, cuando por primera vez el voto femenino se vio
presente en la urnas y hasta la fecha continúa siendo un importante participante
en la toma de decisiones y postulación para cargos importantes en México.

![Fotografía de las primeras elecciones en Estados Unidos en las que participaron las mujeres](/files/images/blog/voto-mujer.jpg)
