---
title: 'Los comerciales de Gandhi que participarán en Cannes.'
subtitle:
  'Las librerías Gandhi nuevamente participarán en los premios Cannes Lions, con
  un par de comerciales sorprendentes.'
formato: B
tag: Cultura y Arte
autor: Gustavo Galvan
autor_detalle: 'hola@wdinamo.com'
image_card: 'blog/gandhi.jpg'
headline: 'Ghandi en Cannes'
abstract:
  'La publicidad de las librerías Ghandi, sin duda además de ser bastante buena,
  siempre da de que hablar y este año no es la excepción, pues como en años
  anteriores tendrá participación en el festival Cannes uno de los más
  importantes del mundo cinematográfico.'
description:
  'Las librerías Gandhi nuevamente participarán en los premios Cannes Lions, con
  un par de comerciales sorprendentes que fomentan a la lectura.'

url: https://www.dinamo.mx/a113/2015/los-comerciales-de-gandhi-que-participaran-en-cannes
keywords:
  - 'Ghandi'
  - 'canes'
  - 'lectura'
  - 'libros'
  - 'comerciales'
  - 'campañas'
publishedAt: 2015-06-15 20:20:20

caption: movie
videoQuality: 720p
duration: '1:30'
---

Las [librerías Gandhi](http://www.gandhi.com.mx/) nuevamente participarán en los
premios Cannes Lions, con un par de comerciales sorprendentes. Con los temas
_“Un libro te lleva a otro”_ y _“Enfrentamiento”,_ la tienda de libros acaparará
las miradas en la justa francesa de creatividad publicitaria.

En años anteriores, otros comerciales de Gandhi fueron premiados en los Cannes
Lions. Un justo premio por generar una de las campañas más interesantes de
fomento a la lectura. Para la premiación del 2015, los comerciales seleccionados
estuvieron bajo la dirección de Tino Vélez, director creativo que tiene bajo su
cargo la cuenta de la librería desde hace ocho años.

En el 2012, los comerciales para web de las librerías Gandhi ya fueron
galardonados en los Cannes Lions. Así que la oportunidad aparece de nuevo en el
horizonte para regresar con el premio. A continuación te compartimos los dos
comerciales que estarán en competencia.

<figure>
<iframe width="788" height="470" src="https://www.youtube.com/embed/ijbTVAv_GRA" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</figure>
<figure>
<iframe width="788" height="470" src="https://www.youtube.com/embed/3Ufq2y29wN8" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</figure>
