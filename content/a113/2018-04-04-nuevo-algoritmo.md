---
title: Nuevo algoritmo, nueva estrategia
formato: B
tag: Ciencia y Tecnología
autor: Diego Villamar
autor_detalle: 'diego@wdinamo.com'
image_card: 'blog/antiguo-facebook.jpg'
headline: El nuevo algoritmo de facebook
abstract:
  El nuevo algoritmo de Facebook y sus implicaciones” y algo que comenzó como un
  rumor, se convirtió en realidad.
description:
  El nuevo algoritmo de Facebook ¿qué beneficios traería para los usuarios este
  cambio? Debemos darles una razón a nuestros usuarios para mantenerse leales a
  nuestra comunidad.

url: https://www.dinamo.mx/a113/2018/nuevo-algoritmo
keywords:
  - 'Nuevo algoritmo'
  - 'Facebook'
publishedAt: 2018-04-04 20:20:20
---

Es sábado a las 8 de la mañana y como tengo la mala costumbre de dejar la
computadora prendida, después de medio despertar checo que hay de nuevo en
Facebook. Generalmente a estas benditas horas del fin de semana encontraba la
agenda del día de las principales ligas de futbol del mundo y por supuesto las
noticias previas a los partidos de la Juventus.

![Fotografía de un partido de futbol soccer](/files/images/blog/juventus.jpg)

Pero ese sábado fue algo diferente, en mi _timeline_ aparecían demasiadas
publicaciones de amigos y grupos a los que ya no recordaba que pertenecía. Seguí
dando _scroll_ y nada, más y más publicaciones de amigos y grupos ¿Y la
Juventus? ¿Récord? ¿As? ¿Qué sucedió con ellos? ¿Acaso hubo una especie de
boicot del cual no me enteré y ahora los medios a los que seguía habían cerrado
sus _fan page_? No le di importancia y continué con mis actividades.\*

Twitter tenía la respuesta, un _tweet_ con un enlace titulado “El nuevo
algoritmo de Facebook y sus implicaciones” me hizo recordar algo que empezó como
un rumor, se transformó en una prueba y ese día ya era una realidad.

![Fotografía de la antigua interfaz de Facebook](/files/images/blog/antiguo-facebook.jpg)

En resumen a partir de ese momento el usuario tendría que entrar a la fan page
de algún medio, empresa o figura pública y activar el botón _Ver las
notificaciones primero_, pero este movimiento también implicaba que para que el
contenido de una página fuera visible sin la necesidad de que el usuario
realizar una acción extra, las páginas de medios, negocios, empresas, etc.
estaban casi obligadas a pagar publicidad.

De esta manera le decíamos adiós al alcance orgánico, ese alcance que dependía
del tipo de contenido y horario en que se publicaría un _post_. Este cambio en
el algoritmo de Facebook se realizó con la intención de que el usuario tuviera
una mejor experiencia, no olvidemos que al final del día Facebook es una red
social y lo que precisamente quiere el usuario es interactuar con otros
usuarios. Por lo tanto Facebook determinó que era necesario privilegiar el
contenido de usuario sobre el de marca.

Pero no todo es negativo ¿qué beneficios traería para los usuarios este cambio?
Que los responsables de generar contenido en Facebook ahora deberíamos
esforzarnos más por crear contenido atractivo para el usuario. Si bien es cierto
que ahora tendríamos que pautar la mayoría de nuestros _posts_, también es una
realidad que debemos darle una razón a nuestros usuarios para mantenerse leales
a nuestra comunidad.

En el punto estrictamente de _alcance o a cuántos usuarios queremos llegar_, sí
fue un cambio agresivo y que impactó de manera importante a cualquiera que
tuviera una _fan page_. Pagar por llegar a nuestros o nuevos usuarios no es algo
que todos tuvieran contemplado.

Este cambio tan radical en Facebook nos hace replantear estrategias, diseñar
contenidos pensados en este nuevo algoritmo y por supuesto pensar en nuevas
formas de acercarnos y mantener a nuestros usuarios, teniendo presente que
Facebook es y siempre será una red social.

\*Por cierto horas después encontré la programación del día y las noticias
previas al partido de la Juventus.

¿Quieres profundizar sobre el tema? Checa este
[artículo de gizmodo](https://es.gizmodo.com/el-nuevo-algoritmo-de-facebook-para-dar-prioridad-al-co-1822113943)
