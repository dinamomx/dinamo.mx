---
title: Y tú ¿ya lo leíste?
subtitle: El experimento, Sebastian Fitzek.
autor: Bernyy Sandoval
formato: B
tag: Cultura y Arte
autor_detalle: 'bernardo@wdinamo.com'
image_card: 'blog/el-experimento.jpg'
og_image:
  src: '/files/images/blog/el-experimento.jpg'
  type: 'image/jpeg'
  height: 1920
  width: 1080
headline: Y tú ¿ya lo leíste?
abstract: El experimento, Sebastian Fitzek.
description: ''

url: https://www.dinamo.mx/a113/2019/el-experimento
keywords:
  - 'Sebastian Fitzek'
  - 'El experimento'
  - 'Psicología'
  - 'Terror'
  - 'Horor'
  - 'Libro'
publishedAt: 2019-12-02 20:20:20
---

El experimento es un libro de suspenso y horror, donde el autor te introducirá
cada vez más a este mundo y, si lo lees detenidamente, te pondrá los pelos de
punta.

A continuación te dejamos la reseña de este terrorífico libro y así te animes a
leerlo.

**Reseña:**

De la mano de su profesor, dos estudiantes de psicología participan en un
experimento que consiste en analizar el expediente médico de un paciente de una
clínica psiquiátrica privada en Berlín, donde hace años tuvieron lugar escenas
de horror y que, actualmente, se mantiene cerrada al público. En medio de los
acontecimientos se hallan el joven Caspar, un paciente que sufre amnesia e
incapaz de recordar quién es, así como el asesino conocido como el "Destructor
de Almas". Tras el ataque a tres mujeres, el Destructor de Almas centrará ahora
su objetivo en el centro psiquiátrico. Cualquiera puede ser la próxima víctima…

**Nuestro fragmento favorito de este libro:**

"…entonces, cuando ya creía percibir el olor a carne quemada, lo vio todo con
claridad: el sótano húmedo y frío al que la habían traído, la lámpara halógena
que oscilaba sobre su cabeza, la silla de tortura y la mesa metálica se
desvanecieron dejando un gran vacío tras de sí. «Gracias a Dios - pensó - sólo
es un sueño». Abrió los ojos pero no logró comprender lo que ocurría. Seguía
atrapada en la misma pesadilla, nada había cambiado".

**¿Te atreves?** Aquí te dejamos un link donde podrás adquirirlo.
https://www.gandhi.com.mx/el-experimento-3
