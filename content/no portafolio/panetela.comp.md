---
title: Panetela
cover: portafolio/single/panetela/panetela.jpg
description: Gestionamos redes sociales, sesiones fotográficas y realizamos coberturas de eventos especiales. Generamos el contenido mensual para Facebook y recorridos virutales 360º en Google Maps.
abstract: Trabajamos en rediseñar, construir y actualizar su sitio web. En redes sociales realizamos campañas de generación de leads, generación de contenido mensual y diseño gráfico.
tags:
  - Fotografía
  - Diseño Gráfico
  - Redes Sociales

headerIsDark: alternate
heroTextColor: 'text-white'
---

::: section text

:::

::: section gallery

## Fotografía

<Gallery :images="[{src:'portafolio/single/panetela/pa6.jpg', alt:'Panetela'},{src:'portafolio/single/panetela/pa5.jpg', alt:'Panetela'},{src:'portafolio/single/panetela/pa4.jpg', alt:'Panetela'},{src:'portafolio/single/panetela/pa3.jpg', alt:'Panetela'},{src:'portafolio/single/panetela/pa2.jpg', alt:'Panetela'},{src:'portafolio/single/panetela/pa1.jpg', alt:'Panetela'},]" />
:::

::: section image

## Redes sociales

<ImageResponsive src="portafolio/single/panetela/PanetelaRedes.jpg" alt="Panetela" sizes="100vw" />
:::
