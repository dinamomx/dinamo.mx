---
title: Invec
cover: portafolio/single/invec/HeroInvecImpresos.jpg
description: Trabajamos imagen corporativa, diseño de logotipo y papelería. Además diseñamos y construimos su sitio web acorde a los estándares de Google.
abstract: INVEC tiene 19 años de experiencia en cálculo, diseño y construcción de obras de ingeniería civil. Para INVEC, **dínamo** ha trabajado en distintas áreas como; diseño de logotipo, imagen corporativa, impresión de tarjetas de presentación, diseño y desarrollo de página web.
tags:
  - Imagen Corporativa
  - Diseño Web

headerIsDark: true
proyectLink: http://invec.mx/
---

::: section text

:::

::: section gallery

## Logotipo

<Gallery :images="[{src:'portafolio/single/invec/inveclogo1.jpg', alt:'Invec'},{src:'portafolio/single/invec/inveclogo2.jpg', alt:'Invec'},{src:'portafolio/single/invec/inveclogo.jpg', alt:'Invec'}]" />
:::

::: section image

## Impresos

<ImageResponsive src="portafolio/single/invec/invecimpresos.jpg" alt="Invec" sizes="100vw" />
:::
