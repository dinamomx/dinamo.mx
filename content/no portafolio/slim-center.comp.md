---
title: Slim Center
cover: portafolio/single/slim/slim.jpg
description: Trabajamos en rediseñar, construir y actualizar su sitio web. En redes sociales realizamos campañas de generación de leads, generación de contenido mensual y diseño gráfico.
abstract: Trabajamos con una de las marcas de perfeccionamiento corporal más importantes del país. Hicimos el rediseño, construcción y actualización constante de la página web y gestionamos redes sociales.
tags:
  - Redes Sociales
  - Diseño Web
headerIsDark: true
proyectLink: http://slimcenter.com.mx/
---

::: section text

:::

::: section image

## Redes Sociales

<ImageResponsive src="portafolio/single/slim/SlimRedes.jpg" alt="Slim Center" sizes="100vw" />
:::

::: section gallery

## Web

<Gallery :images="[{src:'portafolio/single/slim/slim.jpg', alt:'Slim Center'},{src:'portafolio/single/slim/SlimWeb.jpg', alt:'Slim Center'},]" />
:::
