---
title: Crea
cover: portafolio/single/crea/creaLogoWeb.jpg
description: Diseñamos su logotipo y nos encargamos de la imagen corporativa. Una nueva empresa se merece empezar de la mejor manera.
abstract: CREA es una empresa nueva en el campo del financiamiento y crédito personal. Era importante diseñar un logotipo para posicionar y hacer memorable la marca.
tags:
  - Imagen Corporativa
  - Diseño Gráfico

headerIsDark: true
---

::: section text

:::

::: section image
<ImageResponsive src="portafolio/single/crea/Crealogo1.jpg" alt="Crea Logo" sizes="100vw" />
:::

::: section image
<ImageResponsive src="portafolio/single/crea/Crealogo.jpg" alt="Crea Logo" sizes="100vw" />
:::

::: section image
<ImageResponsive src="portafolio/single/crea/Crealogo2.jpg" alt="Crea Logo" sizes="100vw" />
:::
