---
title: Zensky Cine
cover: portafolio/single/zensky/zenskycine.jpg
description: Plasmamos la magia del cine en un sitio web.
tags:
  - Diseño Web
headerIsDark: true
proyectLink: http://zenskycine.tv/
---

::: section text
Zensky Cine es una productora de cine independiente. Trabajamos en el diseño, construcción y mantenimiento de su página web.
:::

::: section image
<ImageResponsive src="portafolio/single/zensky/ZenskyWeb1.jpg" alt="Zensky" sizes="100vw" />
:::

::: section image
<ImageResponsive src="portafolio/single/zensky/ZenskyWeb2.jpg" alt="Zensky" sizes="100vw" />
:::
