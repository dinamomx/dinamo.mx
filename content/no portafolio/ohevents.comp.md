---
title: Oh Events!
cover: portafolio/single/ohevents/heroweb.jpg
description: Desde la CDMX trabajamos en el diseño y construcción del sitio de Oh Events! (Huatulco)
abstract: Oh Events! se encuentra en Huatulco, pero la distancia no fue ningún impedimento para trabajar en el diseño y construcción de su página web.
tags:
  - Diseño Web

headerIsDark: alternate
heroTextColor: 'text-white'
proyectLink: http://oheventshuatulco.com/
---

::: section text

:::

::: section image
<Gallery :images="[{src:'portafolio/single/ohevents/oheventsWeb2.jpg', alt:'oh! Events'},{src:'portafolio/single/ohevents/oheventsWeb1.jpg', alt:'oh! Events'},{src:'portafolio/single/ohevents/oheventsWeb.jpg', alt:'oh! Events'},]" />
:::
