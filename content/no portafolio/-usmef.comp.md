---
title: USMEF
cover:
description: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
tags:
  - Diseño Web

headerIsDark: true
heroTextColor: 'text-white'
proyectLink: http://www.usmef.org.mx/
---

::: section text
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
:::

::: section image
<Gallery :images="[{src:'portafolio/single/ohevents/oheventsWeb2.jpg', alt:'oh! Events'},{src:'portafolio/single/ohevents/oheventsWeb1.jpg', alt:'oh! Events'},{src:'portafolio/single/ohevents/oheventsWeb.jpg', alt:'oh! Events'},]" />
:::
