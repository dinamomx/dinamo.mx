---
title: Happy Frits
cover: portafolio/single/happyfrits/happy.jpg
description: Con esta sesión fotográfica se nos hizo agua la boca, logramos transmitir  la frescura, lo crujiente y el sazón de su producto.  La imagen corporativa reforzó la idea de transmitir el giro de la empresa. 
tags:
  - Fotografía
  - Impresión

headerIsDark: alternate
heroTextColor: 'text-white'
---

::: section gallery
## Fotografía

<Gallery :images="[{src:'portafolio/single/happyfrits/happ1.jpg', alt:'Happy Fritz'},{src:'portafolio/single/happyfrits/happ2.jpg', alt:'Happy Fritz'},{src:'portafolio/single/happyfrits/happ4.jpg', alt:'Happy Fritz'},{src:'portafolio/single/happyfrits/happ5.jpg', alt:'Happy Fritz'},{src:'portafolio/single/happyfrits/happ6.jpg', alt:'Happy Fritz'},]" />
:::

::: section gallery

## Branding
<Gallery :images="[{src:'portafolio/single/happyfrits/HappyFritzlogo.jpg',alt:'Logo de Happy Fritz'},{src:'portafolio/single/happyfrits/HappyFritzlogo1.jpg',alt:'Logo de Happy Fritz'},{src:'portafolio/single/happyfrits/HappyFritzlogo2.jpg',alt:'Logo de Happy Fritz'},{src:'portafolio/single/happyfrits/HappyFritzlogo3.jpg',alt:'Logo de Happy Fritz'},]" />
:::
