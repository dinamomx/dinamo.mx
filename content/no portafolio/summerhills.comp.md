---
title: Summer Hills
cover: portafolio/layout_b/summer/SH_01.jpg
description: Generamos contenido diario para su público objetivo con base en la investigación de los temas de interés para sus seguidores y dándole el tono adecuado. Realizamos campañas en Google y Facebook Ads, cuyo objetivo principal es conseguir nuevos prospectos interesados en la escuela.
tags:
  - Redes Sociales
  - Cobertura de Eventos
  - Diseño Web
headerIsDark: true
textClasses: 'text-white'
template: 'Behance'
showDescription: true
maxImg: true
order: 2
---

<picture>
  <source srcset="/files/images/portafolio/layout_b/summer/SH_01_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_01_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_01_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_01_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_01_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_01_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_01_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_01_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/summer/SH_01_xl.jpg" alt="Summer Hills redes" type="image/jpeg" />
</picture>
<picture>
  <source srcset="/files/images/portafolio/layout_b/summer/SH_02_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_02_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_02_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_02_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_02_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_02_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_02_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_02_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/summer/SH_02_xl.jpg" alt="Summer Hills redes" type="image/jpeg" />
</picture>
<picture>
  <source srcset="/files/images/portafolio/layout_b/summer/SH_03_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_03_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_03_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_03_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_03_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_03_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_03_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_03_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/summer/SH_03_xl.jpg" alt="Summer Hills redes" type="image/jpeg" />
</picture>
<picture>
  <source srcset="/files/images/portafolio/layout_b/summer/SH_04_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_04_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_04_xs.jpg 1x, /files/images/portafolio/layout_b/summer/SH_04_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_04_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_04_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_04_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/summer/SH_04_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/summer/SH_04_xl.jpg" alt="Summer Hills redes" type="image/jpeg" />
</picture>
