---
title: Avc Travel
cover: portafolio/single/AvcTravel/AvcWeb2.jpg
description: 'Una de nuestras pasiones es viajar… Así que trabajar con una agencia de viajes como AVC Travel, ha sido un reto muy placentero.'
tags:
  - Diseño Web
  - Redes Sociales

proyectLink: http://avctravel.com/
---

::: section text
Cuando los conocimos nos identificamos con su filosofía: servicio personalizado rápido y eficaz, por lo que rápidamente hubo química (entre nosotros) con este proyecto.

Después de un **exhaustivo análisis** definimos, junto con AVC Travel, que era necesaria una **reestructuración completa**, que abarcaría desde el tono de comunicación, **rediseño** de su **página web** y renovación de su **logotipo**.
:::

::: section image
<ImageResponsive src="portafolio/single/AvcTravel/AvcLogo.jpg" alt="Logo AVC" sizes="100vw" />
:::

::: section text
En AVC Travel tener una página web que los representara y llegar a más viajeros era el reto. El rediseño ayuda a que el usuario inicie su proceso de reservación de manera fácil y sencilla.
:::

::: section image
<ImageResponsive src="portafolio/single/AvcTravel/AvcWeb.jpg" alt="Web AVC" sizes="100vw" />
:::

::: section text
<p><strong>Renovar la comunicación</strong> en redes sociales era muy importante para mejorar el alcance y obtener mayores clientes potenciales, como resultado <strong> logramos un 300% </strong> más de <strong>interacciones</strong> con intención de compra en un mes.</p>
:::

::: section image
<ImageResponsive src="portafolio/single/AvcTravel/AvcRedes.jpg" alt="Redes AVC" sizes="100vw" />
:::
