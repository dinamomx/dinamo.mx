---
title: Book
cover: portafolio/single/book/herofoto.jpg
description: Sin importar si es fotografía de producto o un book profesional, cuidamos hasta el mínimo detalle en cada sesión.
tags:
  - Fotografía
headerIsDark: alternate
heroTextColor: 'text-white'
---

::: section gallery
<Gallery :images="[{src:'portafolio/single/book/me1.jpg',alt:'Marcelo Ebrard',},{src:'portafolio/single/book/me2.jpg',alt:'Marcelo Ebrard',},{src:'portafolio/single/book/me3.jpg',alt:'Marcelo Ebrard',},{src:'portafolio/single/book/mo1.jpg',alt:'Foto Sesión',},{src:'portafolio/single/book/mo2.jpg',alt:'Foto Sesión',},{src:'portafolio/single/book/mango.jpg',alt:'Mango',},]" />
:::
