---
title: Retro Classic Dinner
cover: portafolio/single/retro/r7.jpg
description: El estilo vintage llegó hasta la página de Retro.
tags:
  - Fotografía
  - Impresión
  - Redes Sociales
  - Diseño Gráfico

---

::: section text
Retro es un concepto que revive el estilo de los 50’s. Para ellos trabajamos en el diseño y construcción de página web, rediseño de logotipo, gestión de redes sociales, shooting de producto y diseño e impresión de menús.
:::

::: section gallery
## Fotografía
<Gallery :images="[{src:'portafolio/single/retro/r1.jpg', alt:'Retro Classic Diner'},{src:'portafolio/single/retro/r2.jpg', alt:'Retro Classic Diner'},{src:'portafolio/single/retro/r3.jpg', alt:'Retro Classic Diner'},{src:'portafolio/single/retro/r4.jpg', alt:'Retro Classic Diner'},]" />
:::


::: section gallery
## Redes Sociales y Web
<Gallery :images="[{src:'portafolio/single/retro/RetroPagina.jpg', alt:'Retro Classic Diner'},{src:'portafolio/single/retro/RetroRedes.jpg', alt:'Retro Classic Diner'},]" />
:::

::: section image
## Impresos
<ImageResponsive src="portafolio/single/retro/retroimpresos.jpg" alt="Retro Classic Diner" sizes="100vw" />
:::
