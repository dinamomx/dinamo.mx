---
title: Cupcake Love
cover: portafolio/single/cupcake/cupcake.jpg
description: En este proyecto realizamos una deliciosa sesión de fotografía de producto, además trabajamos diseño gráfico, generación de contenido y gestión para redes sociales.
abstract: Cupcake Love se especializa en cupcakes artesanales y personalizados. En este proyecto realizamos una deliciosa sesión de fotografía de producto.
tags:
  - Fotografía
  - Diseño Gráfico

headerIsDark: true
---

::: section text

:::

::: section gallery

<Gallery :images="[{src:'portafolio/single/cupcake/cp1.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp2.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp3.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp4.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp5.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp6.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp7.jpg', alt:'Cupcake love'},{src:'portafolio/single/cupcake/cp8.jpg', alt:'Cupcake love'}]" />

:::
