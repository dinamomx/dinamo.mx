---
title: Haz Pan
cover: portafolio/single/hazpan/hazpan.jpg
description: Realizamos sesiones fotográficas con el objetivo de captar la deliciosa escencia del pan.
abstract: Haz Pan es líder en el sector del pan precocido y congelado y forma parte de Grupo Bimbo. Desde 2013 hemos trabajado en conjunto realizando tomas fotográficas de más de 400 productos.
tags:
  - Fotografía

headerIsDark: alternate
heroTextColor: 'text-white'
---

::: section text

:::

::: section gallery
<Gallery :images="[{src:'portafolio/single/hazpan/hp1.jpg', alt:'Haz Pan'},{src:'portafolio/single/hazpan/hp2.jpg', alt:'Haz Pan'},{src:'portafolio/single/hazpan/hp3.jpg', alt:'Haz Pan'},{src:'portafolio/single/hazpan/hp4.jpg', alt:'Haz Pan'},]" />
:::
