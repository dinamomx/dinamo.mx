---
title: 180º
cover: portafolio/single/180/180web.jpg
description: 'Ciento ochenta necesitaba un rediseño en su página web y nosotros los ayudamos.'
tags:
  - Diseño Web
  - Redes Sociales
proyectLink: https://cientoochentagrados.mx/
---

::: section text
El proyecto de 180º consistia en el **rediseño y desarrollo del sitio web**, además de crear una **estrategia** en redes sociales que sirviera no sólo para **fomentar** la **interacción** con los usuarios, sino para hacer un registro de los mismos y formar una **base de datos** de posibles clientes potenciales.
:::

::: section image
<ImageResponsive src="portafolio/single/180/180Web2.jpg" alt="Web 180" sizes="100vw" />
:::

::: section text
El sitio web **no contaba con un diseño responsivo** adaptado a dispositivos móviles, en **consecuencia** generaba **pocas** o **nulas visitas.** En dinamo nos dimos a la tarea de mejorar el sitio así que **trabajamos cuidadosamente su versión móvil**, **cada imagen está optimizada** para diversos dispositivos y el contenido fue cuidado para su fácil lectura. Además, con la incorporación del formulario **logramos registrar 258 usuarios en 20 días.**
:::

::: section image
<ImageResponsive src="portafolio/single/180/180Web1.jpg" alt="Web 180" sizes="100vw" />
:::

::: section text
Nos tomamos muy en serio el seguimiento de nuestros proyectos, así que **mes con mes nos encargamos de ver qué podemos mejorar y actualizamos el sitio.**
:::

::: section image
<ImageResponsive src="portafolio/single/180/180Redes.jpg" alt="Web 180" sizes="100vw" />
:::

::: section text
Con unos ajustes estratégicos en redes sociales, se obtuvo un **incremento** en **citas en un 30%** vía Facebook, (en total se lograron 121 registros y se cerraron 24 citas) en un lapso de **30 días**. Este alcance se logró con una estrategia de comunicación que se adaptaba a los requerimientos de Facebook y que además incluía contenido dirigido al usuario final.
:::
