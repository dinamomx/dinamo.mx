---
title: Jaguar / Land Rover
cover: portafolio/layout_b/jaguar/jaguar1.jpg
description: Para este proyecto gestionamos redes sociales, realizamos diseño gráfico y cubrimos eventos de experiencia, que van desde pruebas en pista hasta eventos de golf. En Facebook construimos una comunidad sólida de seguidores alrededor de ambas marcas.

tags:
  - Redes Sociales
  - Cobertura de Eventos
headerIsDark: true
heroTextColor: 'text-black'
template: 'Behance'
showDescription: false
order: 4
---

<ImageResponsive src="portafolio/layout_b/jaguar/jaguar1.jpg" alt="Redes sociales de jaguarhill" sizes="100vw" webp height="768px" width="1400px" />
<ImageResponsive src="portafolio/layout_b/jaguar/jaguar2.jpg" alt="Redes sociales de jaguar del valle" sizes="100vw" webp height="768px" width="1400px" />
<ImageResponsive src="portafolio/layout_b/jaguar/jaguar3.jpg" alt="Redes sociales de jaguarhill" sizes="100vw" webp height="768px" width="1400px" />
<ImageResponsive src="portafolio/layout_b/jaguar/jaguar4.jpg" alt="Redes sociales de jaguarhill" sizes="100vw" webp height="768px" width="1400px" />
