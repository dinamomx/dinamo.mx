---
title: BY Everyday
cover: portafolio/single/By/ByWeb.jpg
description: 'Become Yourself llegó a México con un proyecto innovador; en dinamo aceptamos el reto para diseñar y desarrollar su página web.'
tags:
  - Diseño Web
  - Redes Sociales
proyectLink: https://byeveryday.mx/
---

::: section text
**Become Yourself** se destaca por ser una empresa a la vanguardia en tratamientos de perfeccionamiento corporal. Por ello, su **página** debía responder a las necesidades de sus clientes.

El objetivo era **diseñar y desarrollar un sitio** en el que el usuario pudiera conocer de manera fácil los diferentes aspectos que conforman a Become Yourself; tratamientos, ubicaciones, formulario de contacto y promociones, fueron algunos de los aspectos fundamentales que había que incluir en este sitio.
:::

::: section image
<ImageResponsive src="portafolio/single/By/ByMovil.jpg" alt="Mobil Byeveryday" sizes="100vw" />
:::

::: section text
Además, gracias a un **análisis estadístico** pudimos constatar que más del **80%** de los clientes de BY navegan en **dispositivos móviles**, por lo que este sitio no sólo requería ser **responsivo**, sino también estar optimizado para su versión móvil y lograr la **carga** en un tiempo máximo de **3.5 segundos.**

En dinamo, la base es crear **páginas** que estén **optimizadas** para versiones móviles y este proyecto no sería una excepción.
:::

::: section image
<ImageResponsive class="is-16by9" src="portafolio/single/By/ByWeb.jpg" alt="By Everyday" sizes="100vw" />
:::
