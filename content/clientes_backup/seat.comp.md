---
title: Seat Acapulco
cover: portafolio/layout_b/seat/01-SeatA.jpg
description: Para SEAT Furia Motors realizamos generación de contenido mensual, propuestas de diseños para oferta comercial y campañas publicitarias para Facebook  y Google Ads, cuyo objetivo se basa en la generación de clientes potenciales.
tags:
  - Redes Sociales
  - Cobertura de Eventos
headerIsDark: true
heroTextColor: 'text-black'
template: 'Behance'
showDescription: true
order: 4
---

<picture>
  <source srcset="/files/images/portafolio/layout_b/seat/01-SeatA_xs.jpg 1x, /files/images/portafolio/layout_b/seat/01-SeatA_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/01-SeatA_xs.jpg 1x, /files/images/portafolio/layout_b/seat/01-SeatA_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/seat/01-SeatA_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/01-SeatA_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/seat/01-SeatA_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/01-SeatA_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/seat/01-SeatA_xl.jpg" alt="Seat Acapulco redes" type="image/jpeg" />
</picture>
<picture>
  <source srcset="/files/images/portafolio/layout_b/seat/02-SeatA_xs.jpg 1x, /files/images/portafolio/layout_b/seat/02-SeatA_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/02-SeatA_xs.jpg 1x, /files/images/portafolio/layout_b/seat/02-SeatA_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/seat/02-SeatA_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/02-SeatA_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/seat/02-SeatA_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/02-SeatA_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/seat/02-SeatA_xl.jpg" alt="Seat Acapulco fotografía" type="image/jpeg" />
</picture>
<picture>
  <source srcset="/files/images/portafolio/layout_b/seat/03-SeatA_xs.jpg 1x, /files/images/portafolio/layout_b/seat/03-SeatA_xs.jpg 2x" media="(max-width: 640px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/03-SeatA_xs.jpg 1x, /files/images/portafolio/layout_b/seat/03-SeatA_xs.jpg 2x" media="(max-width: 640px)" type="image/jpeg"  />
  <source srcset="/files/images/portafolio/layout_b/seat/03-SeatA_md.jpg" media="(max-width: 1399px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/03-SeatA_md.jpg" media="(max-width: 1399px)" type="image/jpeg" />
  <source srcset="/files/images/portafolio/layout_b/seat/03-SeatA_xl.jpg" media="(min-width: 1400px)" type="image/webp" />
  <source srcset="/files/images/portafolio/layout_b/seat/03-SeatA_xl.jpg" media="(min-width: 1400px)" type="image/jpeg" />
  <img src="/files/images/portafolio/layout_b/seat/03-SeatA_xl.jpg" alt="Seat Acapulco galería" type="image/jpeg" />
</picture>
