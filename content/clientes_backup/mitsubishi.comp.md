---
title: Mitsubishi
cover: portafolio/layout_b/mitsu/mitsuNew1.jpg
description: En Mitsubishi Universidad hacemos generación de contenido y gestión de redes sociales, además de activación de campañas de Facebook Ads para la generación de clientes potenciales, asimismo producimos, editamos videos y realizamos coberturas de eventos.
tags:
  - Redes Sociales
  - Cobertura de Eventos
order: 2
headerIsDark: true
showDescription: true
heroTextColor: 'text-black'
template: 'Behance'
---

<ImageResponsive src="portafolio/layout_b/mitsu/mitsuNew1.jpg" alt="Redes sociales de mitsuhill" webp sizes="100vw" webp height="768px" width="1400px" />
<ImageResponsive src="portafolio/layout_b/mitsu/mitsuNew2.jpg" alt="Redes sociales de mitsu del valle" webp sizes="100vw" webp height="768px" width="1400px" />
<ImageResponsive src="portafolio/layout_b/mitsu/mitsuNew3.jpg" alt="Redes sociales de mitsuhill" webp sizes="100vw" webp height="768px" width="1400px" />
<ImageResponsive src="portafolio/layout_b/mitsu/mitsuNew4.jpg" alt="Redes sociales de mitsuhill" webp sizes="100vw" webp height="768px" width="1400px" />
