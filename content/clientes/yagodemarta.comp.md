---
title: Yago de Marta
cover: /portafolio/six/yago.jpg
description:
  Reconstruimos su sitio web, implementando animaciones sutiles, pero que
  implican un grado alto de complejidad. Además logramos implementar esto desde
  el servidor y no del navegador, lo que trae como resultado una navegación
  fluida y con excelente tiempo de carga.
tags:
  - Diseño Web
headerIsDark: true
heroTextColor: 'text-white'
template: 'Behance'
showDescription: true
textClasses: 'text-white'
order: 4
accessMode: 'visiual'
author: 'https://www.dinamo.mx/'
keywords:
  - 'Yago de Marta'
  - 'Diseǹo web'
  - 'Sitio web'
  - 'Animaciones'
  - 'Desempeño'
  - 'Navegación'
  - 'Servidor'
  - 'Tiempo de carga'

alternateName:
  - 'Diseño web'
url: 'https://www.dinamo.mx/portafolio/yagodemarta'
---

<ImageResponsive
  src="/files/images/portafolio/layout_b/yago/yago_01.jpg"
  alt="Yago de Marta redes sociales"
  :lazy="false"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/yago/yago_02.jpg"
  alt="Yago de Marta fotografía"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/yago/yago_03.jpg"
  alt="Yago de Marta web"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" />
