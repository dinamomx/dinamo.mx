---
title: Darila
cover: /portafolio/six/darila.jpg
description:
  Para Darila México, además de generación de contenido y gestión de redes
  sociales, se han realizado campañas en Facebook Ads y desarrollo de landing
  pages, cuyo objetivo se ha enfocado en la generación de clientes potenciales
  pertenecientes al sector corporativo.
tags:
  - Redes Sociales
  - Cobertura de Eventos
headerIsDark: true
textClasses: 'text-white'
showDescription: true
template: 'Behance'
order: 4
author: 'https://www.dinamo.mx/'
keywords:
  - 'Darila México'
  - 'Redes sociales'
  - 'Facebook Ads'
  - 'landing pages'
  - 'Generación de clientes'
  - 'Sector corporativo'
  - 'Redes Sociales'
  - 'Cobertura de eventos'

alternateName:
  - 'Redes Sociales'
  - 'Cobertura de Eventos'
url: 'https://www.dinamo.mx/portafolio/darila'
---

<ImageResponsive
  src="/files/images/portafolio/layout_b/darila/01-darila.jpg"
  alt="Darila redes sociales"
  :lazy="false"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/darila/02-darila.jpg"
  alt="Darila fotografía"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/darila/03-darila.jpg"
  alt="Darila galería"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" />
