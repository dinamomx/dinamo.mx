---
title: City Express
cover: /portafolio/six/cityexpress.jpg
description:
  En este proyecto trabajamos diseño y adaptación de materiales gráficos. Desde
  flyers hasta espectaculares, hemos hecho el esfuerzo de transmitir la esencia
  de esta reconocida cadena hotelera a nivel nacional, respetando su identidad,
  pero poniendo en práctica nuestra creatividad.
tags:
  - Redes Sociales
  - Cobertura de Eventos
headerIsDark: true
heroTextColor: 'text-black'
template: 'Behance'
showDescription: false
order: 3
accessMode: 'visiual'
author: 'https://www.dinamo.mx/'
keywords:
  - 'Diseǹo web'
  - 'Adaptación gráfica'
  - 'Flayers'
  - 'Espectaculares'
  - 'Creatividad'
  - 'Redes Sociales'
  - 'Cobertura de eventos'

alternateName:
  - 'Redes Sociales'
  - 'Cobertura de Eventos'
url: 'https://www.dinamo.mx/portafolio/cityexpress'
---

<ImageResponsive
  src="/files/images/portafolio/layout_b/cityExpress/city_01.jpg"
  alt="City Express redes sociales"
  :lazy="false"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/cityExpress/city_02.jpg"
  alt="City Express fotografía"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/cityExpress/city_03.jpg"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" />
