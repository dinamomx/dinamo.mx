---
title: Volkswagen Cresta del Valle
cover: /portafolio/six/cresta.jpg
description:
  'Generación de contenido, diseño gráfico, gestión de campañas en Facebook y
  Google Ads. Los objetivos de este proyecto son claros: mantener el
  posicionamiento de la agencia, generación de clientes potenciales y reducir el
  costo por lead.'
tags:
  - Redes Sociales
template: Case
headerIsDark: alternate

excerpt:
  'Comenzamos a trabajar con Cresta Del Valle en el 2018. Con un servicio que
  incluye: Gestión de redes sociales, campañas en Facebook Ads y Google Ads,
  fotografía de producto y producción de video.'
order: 1
author: 'https://www.dinamo.mx/'
keywords:
  - 'Volkswagen Cresta del Valle'
  - 'Generación de contenido'
  - 'Diseño gráfico'
  - 'Gestión de redes sociales'
  - 'Redes Sociales'
  - 'Facebook Ads'
  - 'Fotografica'
  - 'Producción de video'

alternateName:
  - 'Redes Sociales'
url: 'https://www.dinamo.mx/portafolio/volkswagen-cresta-del-valle'
---

<portafolio-case-hero logo="/files/images/portafolio/layout_caso/logo_cresta.png">
  <template v-slot:title>
    <span>Volkswagen</span><br />
    <span>Cresta del Valle</span>
  </template>
  <p>Comenzamos a trabajar con Cresta Del Valle en el 2018.</p>
  <p>
    Con un servicio que incluye: Gestión de redes sociales, campañas en
    Facebook Ads y Google Ads, fotografía de producto y producción de
    video.
  </p>
</portafolio-case-hero>
<portafolio-case-section
  h1="Objetivos"
  h2="Los objetivos de nuestro cliente para este proyecto son:"
  img-background="/files/images/portafolio/layout_caso/VW_camioneta1">
  <portafolio-case-slider
    :slides="3"
    :per-page-custom="[[0, 1], [768, 2], [1024, 3]]"
    center-mode>
    <template v-slot:1>
      <figure class="w-48 mx-auto">
        <img src="~assets/icono_posicionamiento.svg" alt="Posicionamiento" class="block mx-auto mb-6 w-16 h-16 " />
        <figcaption class="block text-center">
          <p>
            Mantener el
            <br />
            <span class="uppercase text-brand-naranja font-bold">
              posicionamiento
            </span>
            <br />
            en redes sociales.
          </p>
        </figcaption>
      </figure>
    </template>
    <template v-slot:2>
      <figure class="w-48 mx-auto">
        <img src="~assets/icono_comunicacion.svg" alt="Comunicación" class="block mx-auto mb-6 w-16 h-16" />
        <figcaption class="block text-center">
        Mejorar la 
          <br />
          <span class="uppercase text-brand-naranja font-bold">
            comunicación
          </span>
          <br />
          con el usuario.
        </figcaption>
      </figure>
    </template>
    <template v-slot:3>
      <figure class="w-48 mx-auto">
        <img src="~assets/icono_leads.svg" alt="Leads" class="block mx-auto mb-6 w-16 h-16" />
        <figcaption class="block text-center">
          Generar mayor volumen de
          <span class="uppercase text-brand-naranja font-bold">
            leads calificados.
          </span>
        </figcaption>
      </figure>
    </template>
  </portafolio-case-slider>
</portafolio-case-section>
<portafolio-case-section
  h2="Las acciones que hemos realizado han sido"
  h1="Implementación">
  <portafolio-case-accordion :items="[
        {
          image: '/files/images/portafolio/layout_caso/vw/1_contenido.png',
          caption: 'Generación de contenido.',
        },
        {
          image: '/files/images/portafolio/layout_caso/vw/2_GestionRS.png',
          caption: 'Gestión de redes sociales.',
        },
        {
          image: '/files/images/portafolio/layout_caso/vw/3_campanas.png',
          caption: 'Campañas de generación de leads en Facebook.',
        },
        {
          image: '/files/images/portafolio/layout_caso/vw/4_video.png',
          caption: 'Producción de video.',
        },
        {
          image: '/files/images/portafolio/layout_caso/vw/5_fotos.jpg',
          caption: 'Toma de fotografía de producto.',
        },
        {
          image: '/files/images/portafolio/layout_caso/vw/7_resultados.jpg',
          caption: 'Reporte de resultados.',
        },
      ]"></portafolio-case-accordion>
</portafolio-case-section>
<portafolio-case-section
  h2="Los resultados que hemos obtenido han sido los siguientes"
  h1="Resultados"
  img-background="/files/images/portafolio/layout_caso/VW_camioneta2">
  <portafolio-case-slider
    :slides="4"
    :per-page="4"
    :per-page-custom="[[0, 1], [768, 2], [1024, 4]]"
     center-mode>
    <template v-slot:1>
      <portafolio-number-counter :number="4" title="campañas">
        implementadas en diciembre
      </portafolio-number-counter>
    </template>
    <template v-slot:2>
      <portafolio-number-counter :number="24471" title="usuarios">
        alcanzados <br />
        (en promedio)
      </portafolio-number-counter>
    </template>
    <template v-slot:3>
      <portafolio-number-counter :number="478" title="leads">
        generados
      </portafolio-number-counter>
    </template>
    <template v-slot:4>
      <portafolio-number-counter :number="11.0" title="costo">
        <template v-slot:number="{ number }">
          ${{ number }}
        </template>
        promedio por lead
      </portafolio-number-counter>
    </template>
  </portafolio-case-slider>
</portafolio-case-section>
