---
title: Clínica Colorrectal
cover: /portafolio/six/clinica.jpg
description:
  Generación de contenido y diseño gráfico para redes sociales y campañas de
  Google Ads, logrando una notable interacción con la marca y generando
  confianza del servicio de la Clínica Colorrectal. En consecuencia, el número
  de citas agendadas aumentó un 30 %.
tags:
  - Redes Sociales
  - Cobertura de Eventos
headerIsDark: true
heroTextColor: 'text-black'
showDescription: true
template: 'Behance'
order: 4
author: 'https://www.dinamo.mx/'
keywords:
  - 'Diseǹo gráfico'
  - 'Redes sociales'
  - 'Campañas'
  - 'Google Ads'
  - 'Clinica Colorrectal'
  - 'Redes Sociales'
  - 'Cobertura de eventos'

alternateName:
  - 'Redes Sociales'
  - 'Cobertura de Eventos'
url: 'https://www.dinamo.mx/portafolio/clinica-colorrectal'
---

<ImageResponsive
  src="/files/images/portafolio/layout_b/draFlor/Clinica_01.jpg"
  alt="Clínica Colorrectal redes sociales"
  :lazy="false"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/draFlor/Clinica_02.jpg"
  alt="Clínica Colorrectal fotografía"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" /> <ImageResponsive
  src="/files/images/portafolio/layout_b/draFlor/Clinica_03.jpg"
  sizes="100vw"
  webp
  height="768px"
  width="1400px"
  :image-sizes="[ {separator: '_', word: 'xs', size: '900w'},
    {separator: '_', word: 'md', size: '1400w'},
    {separator: '_', word: 'xl', size: '1980w'}]" />
