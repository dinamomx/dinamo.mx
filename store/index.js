import {
  META_CHANGED,
  SET_BROWSER_DATA,
  CHANGE_HEADER_COLOR,
  SET_BLOG_BACKGROUND,
  FOOTER_ON_VIEWPORT,
} from '~/utils/mutations'

/** @typedef {import('types').VuexState} State */

/** @type {State} */
const rootState = {
  route: null,
  current: {},
  matched: [],
  browserData: null,
  currentBlogHero: null,
  nearlyOnFooter: null,
}
export const state = () => rootState

/**
 * @type {import("vuex").GetterTree<State, State>}
 */
export const getters = {}

/**
 * @type {import("vuex").MutationTree<State>}
 */
export const mutations = {
  [META_CHANGED](currentState, { route, current, matched }) {
    currentState.route = route
    currentState.current = current
    currentState.matched = matched
  },
  [SET_BROWSER_DATA](currentState, parsedUA) {
    currentState.browserData = parsedUA
  },
  [SET_BLOG_BACKGROUND](currentState, background) {
    currentState.currentBlogHero = background
  },
  [CHANGE_HEADER_COLOR](currentState, color) {
    currentState.current.headerIsDark = color
  },
  [FOOTER_ON_VIEWPORT](currentState, visibility) {
    currentState.nearlyOnFooter = visibility
  },
}

/**
 * @type {import("vuex").ActionTree<State, State>}
 */
export const actions = {
  metaChanged({ commit }, meta) {
    commit(META_CHANGED, meta)
  },
}
