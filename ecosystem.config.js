const PORT = 3006

module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   * Importante: Cambiar el puerto de acuerdo a la configuración de nginx
   */
  apps: [
    // First application
    {
      name: 'dinamo.mx',
      append_env_to_name: true,
      script: 'server/start-nuxt.sh',
      interpreter: '/bin/bash',
      env: {
        HOST: 'localhost',
        NODE_ENV: 'production',
      },
      env_staging: {
        PORT: '3007',
        DEBUG: null,
      },
      env_production: {
        PORT,
        PRODUCTION: true,
        DEBUG: null,
      },
    },
  ],
  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: 'webdev',
      host: 'droplet.dinamo.mx',
      ref: 'origin/master',
      repo: 'git@gitlab.com:dinamomx/dinamo.mx.git',
      path: '/var/www/dinamo.mx',
      'pre-deploy-local':
        'PORT=3006 HOST=localhost NODE_ENV=production npm run build && sh uploadbuild.sh',
      'pre-deploy': 'git pull; pnpm install --production --frozen-shrinkwrap',
      'post-deploy': 'pm2 startOrReload ecosystem.config.js --env production',
    },
    staging: {
      user: 'webdev',
      host: 'droplet.dinamo.mx',
      ref: 'origin/dev',
      repo: 'git@gitlab.com:dinamomx/dinamo.mx.git',
      path: '/var/www/beta.dinamo.mx',
      'pre-deploy-local':
        'PORT=3007 HOST=localhost NODE_ENV=production npm run build && sh uploadbuild.sh',
      'pre-deploy': 'git pull; pnpm install --production --frozen-shrinkwrap',
      'post-deploy': 'pm2 startOrReload ecosystem.config.js --env staging',
    },
  },
}
