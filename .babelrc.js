/**
 * @type {import('@babel/core').ConfigFunction}
 */
const babelConfig = () => ({
  presets: [
    [
      require.resolve('@nuxt/babel-preset-app'),
      {
        corejs: { version: 3 },
        buildTarget: process.server ? 'server' : client,
        // Incluir polyfills globales es mejor que no hacerlo
        useBuiltIns: 'entry',
        // Un poco menos de código a cambio de posibles errores
        loose: true,
        // Nuxt quiere usar ie 9, yo no.
        targets: process.server
          ? {
              node: 10,
            }
          : {},
      },
    ],
  ],
  plugins: ['@babel/plugin-proposal-optional-chaining', 'lodash'],
})

module.exports = babelConfig
