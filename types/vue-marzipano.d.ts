import './Marzipano'
declare namespace VueMarzipano {
  interface Hotspot {
    yaw: number
    pitch: number
  }

  export interface HotspotLink extends Hotspot {
    rotation: number
    target: string
  }
  export interface HotspotInfo extends Hotspot {
    title: string
    text: string
    image: string
  }

  export interface ViewParameters extends Hotspot {
    fov: number
  }

  export interface SceneLevel {
    titleSize: number
    size: number
    fallbackOnly: boolean
  }

  export interface SceneData {
    id: string
    name: string
    levels: SceneLevel[]
    faceSize: number
    initialViewParameters: ViewParameters
    linkHotspots: HotspotLink
    infoHotspots: HotspotInfo
  }

  export interface Scene {
    data: SceneData
    scene: Marzipano.Scene
    view: Marzipano.RectilinearView | Marzipano.FlatView
  }

}
