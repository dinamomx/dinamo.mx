type ShareData = {
  title?: string
  text?: string
  url?: string
}

interface Navigator {
  share?: (data?: ShareData) => Promise<void>
}

declare module '*.png' {
  const fileName: string
  export default fileName
}
declare module '*.webp' {
  const fileName: string
  export default fileName
}
declare module '*.jpg' {
  const fileName: string
  export default fileName
}
}
declare module '*.scss' {
  const fileName: string
  export default fileName
}
