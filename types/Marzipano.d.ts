class Controls {
  constructor(t: any)
  addEventListener(t: any, e: any): void
  addMethodGroup(t: any, e: any): void
  attach(t: any): void
  attached(): any
  destroy(): void
  detach(): void
  disable(): void
  disableMethod(t: any): void
  disableMethodGroup(t: any): void
  emit(t: any, e: any, args: any[]): void
  enable(): void
  enableMethod(t: any): void
  enableMethodGroup(t: any): void
  enabled(): any
  method(t: any): any
  methodGroups(): any
  methods(): any
  registerMethod(t: any, e: any, r: any): void
  removeEventListener(t: any, e: any): void
  removeMethodGroup(t: any): void
  unregisterMethod(t: any): void
}
class CssCubeRenderer {
  constructor(t: any, e: any)
  calculateTransform(t: any, e: any, r: any): any
}
class CssFlatRenderer {
  constructor(t: any, e: any)
  calculateTransform(t: any, e: any, r: any): any
}
class CssStage {
  constructor(t: any)
  TextureClass(t: any, e: any, r: any): void
  createRenderer(t: any): any
  destroy(): void
  destroyRenderer(t: any): void
  domElement(): any
  endFrame(): void
  loadImage(t: any, e: any, r: any): any
  setSizeForType(): void
  startFrame(): void
  takeSnapshot(): void
  validateLayer(t: any): void
}
export class CubeGeometry {
  constructor(levelPropertiesList: { size: number; tileSize: number }[])
  Tile(t: any, e: any, r: any, i: any, n: any): void
  levelTiles(t: any, e: any): any
  maxTileSize(): any
  visibleTiles(t: any, e: any, r: any): any
}
class DragControlMethod {
  constructor(t: any, e: any, r: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
}
class DynamicAsset {
  constructor(t: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  isDynamic(): any
  markDirty(): void
  removeEventListener(t: any, e: any): void
  timestamp(): any
}
class Dynamics {
  constructor()
  equals(t: any): any
  nullVelocityTime(): any
  offsetFromVelocity(t: any): any
  reset(): void
  update(t: any, e: any): void
  velocityAfter(t: any): any
}
class ElementPressControlMethod {
  constructor(t: any, e: any, r: any, i: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
}
class EquirectGeometry {
  constructor(t: any)
  Tile(t: any, e: any): void
  levelTiles(t: any, e: any): any
  maxTileSize(): any
  visibleTiles(t: any, e: any, r: any): void
}
class FlashCubeRenderer {
  constructor(t: any, e: any, r: any)
}
class FlashFlatRenderer {
  constructor(t: any, e: any, r: any)
}
class FlashStage {
  constructor(t: any)
  TextureClass(t: any, e: any, r: any): void
  addFlashCallbackListener(t: any, e: any): void
  createRenderer(t: any): any
  destroy(): void
  destroyRenderer(t: any): void
  domElement(): any
  endFrame(): void
  flashElement(): any
  loadImage(t: any, e: any, r: any): any
  removeFlashCallbackListener(t: any, e: any): void
  setSizeForType(): void
  startFrame(): void
  takeSnapshot(t: any): any
  validateLayer(t: any): void
}
class FlatGeometry {
  constructor(t: any)
  Tile(t: any, e: any, r: any, i: any): void
  levelTiles(t: any, e: any): any
  maxTileSize(): any
  visibleTiles(t: any, e: any, r: any): any
}
class FlatView {
  constructor(t: any, e: any)
  addEventListener(t: any, e: any): void
  coordinatesToScreen(t: any, e: any): any
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  height(): any
  intersects(t: any): any
  inverseProjection(): any
  limiter(): any
  mediaAspectRatio(): any
  offsetX(t: any): void
  offsetY(t: any): void
  offsetZoom(t: any): void
  parameters(t: any): any
  projection(): any
  removeEventListener(t: any, e: any): void
  screenToCoordinates(t: any, e: any): any
  selectLevel(t: any): any
  setLimiter(t: any): void
  setMediaAspectRatio(t: any): void
  setParameters(t: any): void
  setSize(t: any): void
  setX(t: any): void
  setY(t: any): void
  setZoom(t: any): void
  size(t: any): any
  updateWithControlParameters(t: any): void
  width(): any
  x(): any
  y(): any
  zoom(): any
}
class Hotspot {
  constructor(t: any, e: any, r: any, i: any, n: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  domElement(): any
  emit(t: any, e: any, args: any[]): void
  hide(): void
  perspective(): any
  position(): any
  removeEventListener(t: any, e: any): void
  setPerspective(t: any): void
  setPosition(t: any): void
  show(): void
}
class HotspotContainer {
  constructor(t: any, e: any, r: any, i: any, n: any)
  addEventListener(t: any, e: any): void
  createHotspot(t: any, e: any, r: any): any
  destroy(): void
  destroyHotspot(t: any): void
  domElement(): any
  emit(t: any, e: any, args: any[]): void
  hasHotspot(t: any): any
  hide(): void
  listHotspots(): any
  rect(): any
  removeEventListener(t: any, e: any): void
  setRect(t: any): void
  show(): void
}
class ImageUrlSource {
  constructor(t: any, e: any)
  addEventListener(t: any, e: any): void
  emit(t: any, e: any, args: any[]): void
  loadAsset(t: any, e: any, r: any): any
  removeEventListener(t: any, e: any): void
}
class KeyControlMethod {
  constructor(t: any, e: any, r: any, i: any, n: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
}
class Layer {
  constructor(t: any, e: any, r: any, i: any, n: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  effects(): any
  emit(t: any, e: any, args: any[]): void
  fixedLevel(): any
  geometry(): any
  mergeEffects(t: any): void
  pinFirstLevel(): any
  pinLevel(t: any): void
  removeEventListener(t: any, e: any): void
  setEffects(t: any): void
  setFixedLevel(t: any): void
  source(): any
  textureStore(): any
  unpinFirstLevel(): any
  unpinLevel(t: any): void
  view(): any
  visibleTiles(t: any): any
}
class PinchZoomControlMethod {
  constructor(t: any, e: any, r: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
}
class QtvrControlMethod {
  constructor(t: any, e: any, r: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
}
class RectilinearView {
  constructor(t: any, e: any)
  addEventListener(t: any, e: any): void
  coordinatesToPerspectiveTransform(t: any, e: any, r: any): any
  coordinatesToScreen(t: any, e: any): any
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  fov(): any
  height(): any
  intersects(t: any): any
  inverseProjection(): any
  limiter(): any
  normalizeToClosest(t: any, e: any): any
  offsetFov(t: any): void
  offsetPitch(t: any): void
  offsetRoll(t: any): void
  offsetYaw(t: any): void
  parameters(t: any): any
  pitch(): any
  projection(): any
  projectionCenterX(): any
  projectionCenterY(): any
  removeEventListener(t: any, e: any): void
  roll(): any
  screenToCoordinates(t: any, e: any): any
  selectLevel(t: any): any
  setFov(t: any): void
  setLimiter(t: any): void
  setParameters(t: any): void
  setPitch(t: any): void
  setProjectionCenterX(t: any): void
  setProjectionCenterY(t: any): void
  setRoll(t: any): void
  setSize(t: any): void
  setYaw(t: any): void
  size(t: any): any
  updateWithControlParameters(t: any): void
  width(): any
  yaw(): any
}
class RenderLoop {
  constructor(t: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
  renderOnNextFrame(): void
  stage(): any
  start(): void
  stop(): void
}
class Scene {
  constructor(t: any, e: any)
  addEventListener(t: any, e: any): void
  createLayer(t: any): any
  destroy(): void
  destroyAllLayers(): void
  destroyLayer(t: any): void
  emit(t: any, e: any, args: any[]): void
  hotspotContainer(): any
  layer(): any
  listLayers(): any
  lookTo(t: any, e: any, r: any): any
  movement(): any
  removeEventListener(t: any, e: any): void
  startMovement(fn: Function, done?: Function): void
  stopMovement(): void
  switchTo(opts?: switchSceneOptions, done?: Function): any
  view(): any
  viewer(): Viewer
  visible(): any
}
type switchSceneOptions = {
  transitionDuration?: number
  transitionUpdate?: number
}

class ScrollZoomControlMethod {
  constructor(t: any, e: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
  withSmoothing(t: any): void
  withoutSmoothing(t: any): void
}
class SingleAssetSource {
  constructor(t: any)
  asset(): any
  loadAsset(t: any, e: any, r: any, args: any[]): any
}
class StaticAsset {
  constructor(t: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  element(): any
  emit(t: any, e: any, args: any[]): void
  height(): any
  isDynamic(): any
  removeEventListener(t: any, e: any): void
  timestamp(): any
  width(): any
}
class TextureStore {
  constructor(t: any, e: any, r: any)
  addEventListener(t: any, e: any): void
  asset(t: any): any
  clear(): void
  clearNotPinned(): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  endFrame(): void
  markTile(t: any): void
  pin(t: any): any
  query(t: any): any
  removeEventListener(t: any, e: any): void
  source(): any
  stage(): any
  startFrame(): void
  texture(t: any): any
  unpin(t: any): any
}
class VelocityControlMethod {
  constructor(t: any)
  addEventListener(t: any, e: any): void
  destroy(): void
  emit(t: any, e: any, args: any[]): void
  removeEventListener(t: any, e: any): void
  setFriction(t: any): void
  setVelocity(t: any): void
}
class Viewer {
  constructor(t: any, e: any)
  addEventListener(t: any, e: any): void
  breakIdleMovement(): void
  controls(): any
  createEmptyScene(t: any): any
  createScene(t: any): any
  destroy(): void
  destroyAllScenes(): void
  destroyScene(t: any): void
  domElement(): any
  emit(t: any, e: any, args: any[]): void
  hasScene(t: any): any
  listScenes(): any
  lookTo(t: any, e: any, r: any): void
  movement(): any
  removeEventListener(t: any, e: any): void
  renderLoop(): any
  scene(): any
  setIdleMovement(t: number, e?: Function | null): void
  stage(): any
  startMovement(fn: Function, done?: Function): void
  stopMovement(): void
  switchScene(t: any, e: any, r: any): any
  updateSize(): void
  view(): any
}
class WebGlEquirectRenderer {
  constructor(t: any)
  destroy(): void
  endLayer(t: any, e: any): void
  renderTile(t: any, e: any, r: any, i: any): void
  startLayer(t: any, e: any): void
}
class WebGlStage {
  constructor(t: any)
  TextureClass(t: any, e: any, r: any): void
  createRenderer(t: any): any
  destroy(): void
  destroyRenderer(t: any): void
  domElement(): any
  endFrame(): void
  loadImage(t: any, e: any, r: any): any
  maxTextureSize(): any
  setSizeForType(): void
  startFrame(): void
  takeSnapshot(t: any): any
  validateLayer(t: any): void
  webGlContext(): any
}
namespace CssStage {
  class TextureClass {
    constructor(t: any, e: any, r: any)
    destroy(): void
    refresh(t: any, e: any): void
  }
  function supported(): any
  const type: string
}
class CubeTile {
  constructor(t: any, e: any, r: any, i: any, n: any)
  atBottomEdge(): any
  atBottomLevel(): any
  atLeftEdge(): any
  atRightEdge(): any
  atTopEdge(): any
  atTopLevel(): any
  centerX(): any
  centerY(): any
  children(t: any): any
  cmp(t: any): any
  equals(t: any): any
  hash(): any
  height(): any
  levelHeight(): any
  levelWidth(): any
  neighbors(): any
  padBottom(): any
  padLeft(): any
  padRight(): any
  padTop(): any
  parent(): any
  rotX(): any
  rotY(): any
  scaleX(): any
  scaleY(): any
  str(): any
  vertices(t: any): any
  width(): any
}
namespace Dynamics {
  function equals(t: any, e: any): any
}
namespace EquirectGeometry {
  class Tile {
    constructor(t: any, e: any)
    atBottomEdge(): any
    atBottomLevel(): any
    atLeftEdge(): any
    atRightEdge(): any
    atTopEdge(): any
    atTopLevel(): any
    centerX(): any
    centerY(): any
    children(t: any): any
    cmp(t: any): any
    equals(t: any): any
    hash(): any
    height(): any
    levelHeight(): any
    levelWidth(): any
    neighbors(): any
    padBottom(): any
    padLeft(): any
    padRight(): any
    padTop(): any
    parent(): any
    rotX(): any
    rotY(): any
    scaleX(): any
    scaleY(): any
    str(): any
    width(): any
  }
  namespace Tile {
    const type: string
  }
  const type: string
}
namespace FlashStage {
  class TextureClass {
    constructor(t: any, e: any, r: any)
    destroy(): void
    refresh(t: any, e: any): void
  }
  function supported(): any
  const type: string
}
namespace FlatGeometry {
  class Tile {
    constructor(t: any, e: any, r: any, i: any)
    atBottomEdge(): any
    atBottomLevel(): any
    atLeftEdge(): any
    atRightEdge(): any
    atTopEdge(): any
    atTopLevel(): any
    centerX(): any
    centerY(): any
    children(t: any): any
    cmp(t: any): any
    equals(t: any): any
    hash(): any
    height(): any
    levelHeight(): any
    levelWidth(): any
    neighbors(): any
    padBottom(): any
    padLeft(): any
    padRight(): any
    padTop(): any
    parent(): any
    rotX(): any
    rotY(): any
    scaleX(): any
    scaleY(): any
    str(): any
    vertices(t: any): any
    width(): any
  }
  namespace Tile {
    const type: string
  }
  const type: string
}
namespace FlatView {
  namespace limit {
    function letterbox(): any
    function resolution(t: any): any
    function visibleX(t: any, e: any): any
    function visibleY(t: any, e: any): any
    function x(t: any, e: any): any
    function y(t: any, e: any): any
    function zoom(t: any, e: any): any
  }
  const type: string
}
namespace ImageUrlSource {
  function fromString(t: any, e: any): any
}
namespace RectilinearView {
  namespace limit {
    function hfov(t: any, e: any): any
    function pitch(t: any, e: any): any
    function resolution(t: any): any
    function roll(t: any, e: any): any
    function traditional(t: any, e: any, r: any): any
    function vfov(t: any, e: any): any
    function yaw(t: any, e: any): any
  }
  const type: string
}
namespace WebGlStage {
  class TextureClass {
    constructor(t: any, e: any, r: any)
    destroy(): void
    refresh(t: any, e: any): void
  }
  function supported(): any
  const type: string
}
export interface colorEffects {
  applyToImageData(t: any, e: any): void
  applyToPixel(t: any, e: any, r: any): void
  identity(t: any): any
}
export interface util {
  async(t: any): any
  cancelize(t: any, args: any[]): any
  chain(args: any[]): any
  clamp(t: any, e: any, r: any): any
  clearOwnProperties(t: any): void
  cmp(t: any, e: any): any
  compose(args: any[]): any
  decimal(t: any): any
  defaults(t: any, e: any): any
  defer(t: any, e: any): void
  degToRad(t: any): any
  delay(t: any, e: any, args: any[]): any
  extend(t: any, e: any): any
  hash(args: any[]): any
  inherits(t: any, e: any): void
  mod(t: any, e: any): any
  noop(): void
  now(): any
  once(t: any, args: any[]): any
  pixelRatio(): any
  radToDeg(t: any): any
  real(t: any): any
  retry(t: any, args: any[]): any
  tween(t: any, e: any, r: any, args: any[]): any
  type(t: any): any
  convertFov: {
    convert(t: any, e: any, r: any): any
    dtoh(t: any, e: any, r: any): any
    dtov(t: any, e: any, r: any): any
    htod(t: any, e: any, r: any): any
    htov(t: any, e: any, r: any): any
    vtod(t: any, e: any, r: any): any
    vtoh(t: any, e: any, r: any): any
  }
  dom: {
    getWithVendorPrefix(t: any): any
    prefixProperty(t: any): any
    setAbsolute(t: any): void
    setBlocking(t: any): void
    setFullSize(t: any): void
    setNoPointerEvents(t: any): void
    setNullSize(t: any): void
    setNullTransform(t: any): void
    setNullTransformOrigin(t: any): void
    setOverflowHidden(t: any): void
    setOverflowVisible(t: any): void
    setPixelPosition(t: any, e: any, r: any): void
    setPixelSize(t: any, e: any, r: any): void
    setTransform(t: any, r: any): any
    setTransformOrigin(t: any, r: any): any
    setWithVendorPrefix(t: any): any
  }
}
export interface Marzipano {
  WebGlStage: Marzipano.WebGlStage
  CssStage: Marzipano.CssStage
  FlashStage: Marzipano.FlashStage
  WebGlCubeRenderer(args: any[]): void
  WebGlFlatRenderer(args: any[]): void
  WebGlEquirectRenderer: Marzipano.WebGlEquirectRenderer
  CssCubeRenderer: Marzipano.CssCubeRenderer
  CssFlatRenderer: Marzipano.CssFlatRenderer
  FlashCubeRenderer: Marzipano.FlashCubeRenderer
  FlashFlatRenderer: Marzipano.FlashFlatRenderer
  registerDefaultRenderers(t: any): void
  WebGlStage: Marzipano.WebGlStage
  CssStage: Marzipano.CssStage
  FlashStage: Marzipano.FlashStage

  CubeGeometry: CubeGeometry
  FlatGeometry: Marzipano.FlatGeometry
  EquirectGeometry: Marzipano.EquirectGeometry
  RectilinearView: Marzipano.RectilinearView
  FlatView: Marzipano.FlatView
  ImageUrlSource: Marzipano.ImageUrlSource
  SingleAssetSource: Marzipano.SingleAssetSource
  StaticAsset: Marzipano.StaticAsset
  DynamicAsset: Marzipano.DynamicAsset
  TextureStore: Marzipano.TextureStore
  Layer: Marzipano.Layer
  RenderLoop: Marzipano.RenderLoop
  KeyControlMethod: Marzipano.KeyControlMethod
  DragControlMethod: Marzipano.DragControlMethod
  QtvrControlMethod: Marzipano.QtvrControlMethod
  ScrollZoomControlMethod: Marzipano.ScrollZoomControlMethod
  PinchZoomControlMethod: Marzipano.PinchZoomControlMethod
  VelocityControlMethod: Marzipano.VelocityControlMethod
  ElementPressControlMethod: Marzipano.ElementPressControlMethod
  Controls: Marzipano.Controls
  Dynamics: Marzipano.Dynamics
  Viewer: Marzipano.Viewer
  Scene: Marzipano.Scene
  Hotspot: Marzipano.Hotspot
  HotspotContainer: Marzipano.HotspotContainer
  colorEffects: Marzipano.colorEffects
  registerDefaultControls(t: any, e: any, r: any): any
  autorotate(t: any): any
  util: Marzipano.util
}
declare module 'marzipano' {
  export default Marzipano
}
