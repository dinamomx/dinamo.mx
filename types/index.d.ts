import './vue-meta'
import './vue'
import './globals'
import './vue-marzipano'
//import '@nuxt/content'

export interface ImageObject {
  route: string
  name: string
  format: string
}
export type ogImage =
  | string
  | {
      src: string
      height: string
      width: string
      type?: 'image/png' | 'image/jpeg'
      alt: string
    }
export interface SeoObject {
  title: string | undefined
  domain?: string
  og_image?: ogImage
  description: string
  url?: string
}

export interface PortafolioItems {
  name: string
  costumers: string
  coverImg: string
  altImg: string
  link: string
}

export type TeamCardInfoImageWorks = {
  image: string
  altImage: string
}
export type TeamCardInfoImageBackground = {
  desktop: string
  mobile: string
}
export interface TeamCardInfo {
  name: string
  position: string
  skills: string
  descripcion?: string[]
  bgPosition: string
  theme: string
  imageWorks: TeamCardInfoImageWorks
  imageBackground: TeamCardInfoImageBackground
  altImage: string
  imagen?: string
  image?: string
  imageCenter?: string
  imageBack?: string
  imageBackCenter?: string
}

export type DataImages = {
  url: string
  alt: string
  extension: string
  poster?: string
}

export type CategoryWork = {
  position?: 'center' | 'right'
  imgVertical: boolean
  name: string
  project: string[] | string
  images: DataImages[]
}

export interface Category extends SeoObject {
  binder: string
  intro: string[] | string
  urlPrev: String
  urlNext: String
  titlePrev: String
  titleNext: String
  subtitle?: string
  images_content?: string[]
  works: CategoryWork[]
}

export type HoverDisortArguments = {
  parent: HTMLCanvasElement
  displacementImage: string
  image1: string
  image2: string
  imagesRatio?: number
  intensity?: number
  intensity1?: number
  intensity2?: number
  commonAngle?: number
  angle1?: number
  angle2?: number
  speedIn?: number
  speedOut?: number
  userHover?: boolean
  easing?: gsap.EaseFunction
  speed?: number
  video?: boolean
}

export type NuxtentBody =
  | string
  | {
      relative_url: string
    }

export type NuxtentMeta = {
  dirname: string
  filename: string
  index: number
  section: string
}

export interface NuxtContentItem {
  createdAt: Date
  updatedAt: Date
  slug: string
  dir: string
  path: string
  extension: '.md' | '.yaml' | '.csv'
}

export interface NuxtentBlogPost extends SeoObject, NuxtContentItem {
  title: string
  subtitle: string
  /** Reflejo del titulo para schema.org */
  headline?: string
  /** El resumen de la entrada con un promedio de 200 a 260 caracteres */
  abstract: string
  autor: string
  image_cover?: string
  image_card?: string
  autor_detalle: string
  body: any
  main: Boolean
  subtitle?: string
  tag: string
  title: string
  headerIsDark: boolean
  keywords: string[]
  publishedAt: Date
  videos?: VideoSeo[]
  mostReaded?: number
  url: string
  header_color: boolean | 'alternate'
}

export type AcordionItem = {
  number: string
  image: string
  imageName: string
  imageText: string
  imageText2: string
  imageTextName: string
  title: string
  sillabe: number
  text: string[]
}

export type HacemosWordItem = {
  interval?: number
  timeoutA?: number
  timeoutB?: number
  isActive: boolean
  left: string
  right: string
}

export type VideoSeo = {
  actor?: string
  director?: string
  caption?: string
  duration?: string
  videoQuality?: string
}

export type CreativeWorkPortafolio = {
  author: string
  keywords: string
  publisher: string
  alternateName: string
  url: string
}
/**
 * Base para todo el contenido generado con markdown
 */
export interface MarkdownContent {
  /** El contenido en html */
  $_content: string
  /** Un texto sin html haciendo un resumen del contenido */
  $_excerpt?: string
}

export interface Checklist extends MarkdownContent {
  title: string
}

export type RouteMeta = {
  headerIsDark: boolean | 'alternate'
  // Aquí van las opciones meta que quieras meter como meta para el componente
  [x: string]: boolean | string
}

export interface VuexModuleMeta {
  route: string | null
  current: RouteMeta
  matched: RouteMeta[]
}

export interface BlogPost extends MarkdownContent {
  slug?: string
  title: string
  image: string
}
export interface VuexState extends VuexModuleMeta {
  browserData: IUAParser.IResult | null
  currentBlogHero: null | string
  nearlyOnFooter: boolean | null
}
