import { MetaDataProperty } from 'vue-meta/types/vue-meta'

declare module 'vue-meta/types/vue-meta' {
  export interface MetaDataProperty {
    hid?: string
  }
}
