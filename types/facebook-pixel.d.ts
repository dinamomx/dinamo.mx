export interface FacebookPixel {
  event: (name: string, data?: any) => void
  query: (...args: any[]) => void
  enable: () => void
  disable: () => void
  init: (id: string, data?: any) => void
}
