import Vue, { ComponentOptions } from 'vue'
import { TrackingPlugin } from './vuetracking'
import { FacebookPixel } from '@dinamomx/nuxt-fbpixel-module'

export declare module 'vue/types/vue' {
  interface Vue {
    $fbq: FacebookPixel
    $tracking: TrackingPlugin
  }
}
