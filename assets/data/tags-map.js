/** @type {[string,string][]} */
export const tagsArrayMap = [
  ['Dinamo', 'dinamo'],
  ['Cultura y Arte', 'cultura-y-arte'],
  ['Ciencia y Tecnología', 'ciencia-y-tecnologia'],
  ['Marketing', 'marketing'],
  ['Todas las categorías', 'todas-las-categorias'],
  ['Noticias', 'noticias'],
  ['Redes Sociales', 'redes-sociales'],
]

/**
 * @type {Map<string, string>}
 */
export const tagsMap = new Map(tagsArrayMap)

/**
 * Busca una categoria por su url
 *
 * @param {string} searchTerm Parámetro a buscar
 * @returns {[string, string] | undefined}
 */
export const findTagBySlug = (searchTerm) =>
  tagsArrayMap.find(([, slug]) => slug === searchTerm)
