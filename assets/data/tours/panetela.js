export default {
  tilePath: '/tours/panetela',
  scenes: [
    {
      id: '0-fachada',
      name: 'Fachada',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 0.042961950818325434,
        pitch: -0.4254650411505754,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: -0.015542814478379796,
          pitch: -0.1237025623739143,
          rotation: 0,
          target: '1-entrada',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '1-entrada',
      name: 'Entrada',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [
        {
          yaw: -2.954491292597673,
          pitch: 0.21285283980183323,
          rotation: 0,
          target: '0-fachada',
        },
        {
          yaw: 1.6905768409359982,
          pitch: 0.07744998276871407,
          rotation: 0,
          target: '2-comedor-1',
        },
        {
          yaw: 0.2406492801250586,
          pitch: 0.07103978508199837,
          rotation: 0,
          target: '5-comedor-2',
        },
        {
          yaw: -0.26109976455300554,
          pitch: 0.09013285071107546,
          rotation: 0,
          target: '4-rea-de-ensaladas',
        },
      ],
      infoHotspots: [
        {
          yaw: -1.6024348556857984,
          pitch: 0.07197291196719746,
          title: 'Área de panadería<br>',
          text: 'Text',
        },
      ],
    },
    {
      id: '2-comedor-1',
      name: 'Comedor 1',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [
        {
          yaw: -2.9536888170763724,
          pitch: 0.05714716044689894,
          rotation: 0,
          target: '1-entrada',
        },
        {
          yaw: 0.231346549419154,
          pitch: 0.05713574301220703,
          rotation: 0,
          target: '3-rea-de-bar',
        },
        {
          yaw: -2.3539197305911657,
          pitch: 0.05638899678395681,
          rotation: 0,
          target: '4-rea-de-ensaladas',
        },
        {
          yaw: -1.7295205648835594,
          pitch: 0.05562666878585176,
          rotation: 0,
          target: '5-comedor-2',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '3-rea-de-bar',
      name: 'Área de Bar',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [
        {
          yaw: -3.0907943046810367,
          pitch: 0.09628851964767904,
          rotation: 0,
          target: '2-comedor-1',
        },
      ],
      infoHotspots: [
        {
          yaw: 1.1712132088007952,
          pitch: 0.07042589436493785,
          title: 'Salida de emergencia<br>',
          text: 'Text',
        },
      ],
    },
    {
      id: '4-rea-de-ensaladas',
      name: 'Área de ensaladas',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: -2.0355831943325313,
        pitch: 0.1670986939741006,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 0.0007863072405420723,
          pitch: 0.16861745598598077,
          rotation: 0,
          target: '1-entrada',
        },
        {
          yaw: -0.7366022178788363,
          pitch: 0.13389376562970057,
          rotation: 0,
          target: '2-comedor-1',
        },
        {
          yaw: -1.1819994967637442,
          pitch: 0.08871488168094288,
          rotation: 0,
          target: '3-rea-de-bar',
        },
        {
          yaw: -2.152733613080512,
          pitch: 0.02048842635545256,
          rotation: 0,
          target: '6-baos-y-escalera',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '5-comedor-2',
      name: 'Comedor 2',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [
        {
          yaw: 0.23116975004758444,
          pitch: 0.1064435367845622,
          rotation: 0,
          target: '4-rea-de-ensaladas',
        },
        {
          yaw: -0.2857090553573265,
          pitch: 0.12475216226329522,
          rotation: 0,
          target: '1-entrada',
        },
        {
          yaw: -0.6827062596206837,
          pitch: 0.11964883496785639,
          rotation: 0,
          target: '2-comedor-1',
        },
        {
          yaw: -2.1823233641456383,
          pitch: 0.03773420514884229,
          rotation: 0,
          target: '6-baos-y-escalera',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '6-baos-y-escalera',
      name: 'Baños y escalera',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: -3.046878019945437,
        pitch: 0.15058069811121655,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 2.630523029068195,
          pitch: -0.041934921851261464,
          rotation: 0,
          target: '5-comedor-2',
        },
        {
          yaw: -2.2986647921058143,
          pitch: -0.16396310845923168,
          rotation: 0,
          target: '7-escalera',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '7-escalera',
      name: 'Escalera',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: -2.553327624187233,
        pitch: -0.1611291188614441,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 2.0872884805353227,
          pitch: 0.6623167846388149,
          rotation: 0,
          target: '6-baos-y-escalera',
        },
        {
          yaw: -2.7534478772620066,
          pitch: -0.2259547004482556,
          rotation: 0,
          target: '8-entrada-a-la-terraza',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '8-entrada-a-la-terraza',
      name: 'Entrada a la Terraza',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [
        {
          yaw: 1.4992974565027044,
          pitch: 0.3034281999879269,
          rotation: 0,
          target: '7-escalera',
        },
        {
          yaw: -0.20440683110781777,
          pitch: 0.030227208066756717,
          rotation: 0,
          target: '9-terraza',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '9-terraza',
      name: 'Terraza',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        pitch: 0,
        yaw: 0,
        fov: 1.5707963267948966,
      },
      linkHotspots: [
        {
          yaw: -2.6246713543622473,
          pitch: 0.114043545807343,
          rotation: 0,
          target: '8-entrada-a-la-terraza',
        },
        {
          yaw: 0.1455222355695387,
          pitch: 0.017575044221489478,
          rotation: 0,
          target: '10-terraza-2',
        },
        {
          yaw: -0.5283457339848727,
          pitch: 0.0005096461773046457,
          rotation: 0,
          target: '11-terraza-bar',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '10-terraza-2',
      name: 'Terraza 2',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: -0.10836585095299256,
        pitch: 0.01768546395305748,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: -3.009149747751989,
          pitch: 0.09633170160879878,
          rotation: 0,
          target: '9-terraza',
        },
        {
          yaw: -1.0634223089841548,
          pitch: 0.04425431562871296,
          rotation: 0,
          target: '11-terraza-bar',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '11-terraza-bar',
      name: 'Terraza bar',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 3.015162764811393,
        pitch: 0.10056572077721526,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: -2.6929878177121953,
          pitch: 0.05837693260074417,
          rotation: 0,
          target: '9-terraza',
        },
        {
          yaw: 2.622106401070079,
          pitch: 0.07341861602534294,
          rotation: 0,
          target: '10-terraza-2',
        },
      ],
      infoHotspots: [],
    },
  ],
  name: 'Panetela',
  settings: {
    mouseViewMode: 'drag',
    autorotateEnabled: false,
    fullscreenButton: true,
    viewControlButtons: true,
  },
}
