export default {
  tilePath: '/tours/vw-insurgentes',
  scenes: [
    {
      id: '0-fachada',
      name: 'Fachada',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 3.1053596831179817,
        pitch: -0.38765748022463953,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: -3.0046123031074927,
          pitch: -0.06120984608944546,
          rotation: 0,
          target: '1-entrada-principal',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '1-entrada-principal',
      name: 'Entrada principal',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 3.046283067439636,
        pitch: 0.2167647614796735,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 3.085780118365995,
          pitch: 0.056016793662017506,
          rotation: 0,
          target: '2-recepcin',
        },
        {
          yaw: -1.9697517287174886,
          pitch: 0.09418635866905944,
          rotation: 0,
          target: '3-recepcin-derecha',
        },
        {
          yaw: -0.15473165023646196,
          pitch: -0.023579858485721417,
          rotation: 0,
          target: '0-fachada',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '2-recepcin',
      name: 'Recepción',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: -2.975465130521444,
        pitch: 0.14718653431118334,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 3.1181664428177136,
          pitch: 0.10315286615691477,
          rotation: 0,
          target: '6-interior',
        },
        {
          yaw: 0.02961959821684168,
          pitch: 0.09037492550257653,
          rotation: 0,
          target: '1-entrada-principal',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '3-recepcin-derecha',
      name: 'Recepción (derecha)',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 2.9448960503776025,
        pitch: 0.23126736544932314,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 3.126597462784243,
          pitch: 0.00009281579514741622,
          rotation: 0,
          target: '4-ala-derecha',
        },
        {
          yaw: 0.03205327473773245,
          pitch: 0.03831586849853075,
          rotation: 0,
          target: '1-entrada-principal',
        },
        {
          yaw: 0.9685891874647776,
          pitch: -0.06370688006377989,
          rotation: 0,
          target: '2-recepcin',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '4-ala-derecha',
      name: 'Ala derecha',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 2.938797099658508,
        pitch: 0.21993942992326687,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 3.010546988891468,
          pitch: -0.015357066258971486,
          rotation: 0,
          target: '5-entrada-derecha',
        },
        {
          yaw: -0.024534753779100527,
          pitch: -0.018362931442567287,
          rotation: 0,
          target: '3-recepcin-derecha',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '5-entrada-derecha',
      name: 'Entrada derecha',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 2.9835206932419442,
        pitch: 0.06742209176126934,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 0,
          pitch: 0,
          rotation: 0,
          target: '4-ala-derecha',
        },
      ],
      infoHotspots: [],
    },
    {
      id: '6-interior',
      name: 'Interior',
      levels: [
        {
          tileSize: 256,
          size: 256,
          fallbackOnly: true,
        },
        {
          tileSize: 512,
          size: 512,
        },
        {
          tileSize: 512,
          size: 1024,
        },
        {
          tileSize: 512,
          size: 2048,
        },
      ],
      faceSize: 1344,
      initialViewParameters: {
        yaw: 0,
        pitch: 0,
        fov: 1.362234561578609,
      },
      linkHotspots: [
        {
          yaw: 3.1262804959376354,
          pitch: -0.02351039710677938,
          rotation: 0,
          target: '2-recepcin',
        },
      ],
      infoHotspots: [],
    },
  ],
  name: 'Volkswagen Insurgentes',
  settings: {
    mouseViewMode: 'drag',
    autorotateEnabled: false,
    fullscreenButton: true,
    viewControlButtons: true,
  },
}
