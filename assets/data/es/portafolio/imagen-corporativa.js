/** @type {import('types').Category} */
const imagenCorporativa = {
  title: 'Imagen Corporativa',
  description: '',
  og_image: {
    src: '/files/images/category/imagen-corporativa/crea-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Imagen Corporativa',
  },
  binder: '/category/imagen-corporativa/',
  urlPrev: 'diseno-grafico',
  urlNext: 'fotografia',
  titlePrev: 'Diseño Gráfico',
  url: '/portafolio/category/imagen-corporativa',
  titleNext: 'Fotografía',
  intro:
    'Consideramos la funcionalidad de un logotipo en los contextos que el cliente necesita, separándose de la construcción de la marca para cumplir ambos objetivos.',
  works: [
    {
      position: 'center',
      imgVertical: false,
      name: 'Milkiin',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Diseñamos la identidad gráfica para una clínica ginecológica adaptada para su uso en diferentes aplicaciones.

      Analizamos las necesidades del cliente para realizar un diseño orientado a sus servicios.
      </p>
      `,
      images: [
        {
          url: 'milkin/milkin-1',
          alt: 'Rediseño de la imagen corporativa',
          extension: '.webp',
        },
        {
          url: 'milkin/milkin-2',
          alt: 'Diseño de hojas de pesentacion',
          extension: '.webp',
        },
        {
          url: 'milkin/milkin-3',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.webp',
        },
        {
          url: 'milkin/milkin-4',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.webp',
        },
        {
          url: 'milkin/milkin-5',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.webp',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'CREA',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>La propuesta gráfica se acerca a un estilo sintético/minimalista, el cual mantiene cierta referencia al abecedario tipográfico de Bruno Munari. Trabajamos nuestra propuesta con una proporción aurea.</p>
      `,
      images: [
        {
          url: 'crea/crea',
          alt: 'Rediseño de la imagen corporativa',
          extension: '.jpg',
        },
        {
          url: 'crea/crea-2',
          alt: 'Diseño de hojas de pesentacion',
          extension: '.jpg',
        },
        {
          url: 'crea/crea-3',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Happy Frits',
      name2: 'HappyFrits',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>En el 2014 los emojis tomaron un papel más relevante en nuestra comunicación cotidiana. Bajo este contexto el logo de Happy Fritz incluye una referencia clara a un emoji y está orientado a un público joven. Además, tomamos como base los rasgos del producto para estilizar la propuesta tipográfica.</p>
      `,
      images: [
        {
          url: 'happy/happy',
          alt: 'Rediseño de la imagen corporativa',
          extension: '.jpg',
        },
        {
          url: 'happy/happy-2',
          alt: 'Diseño en papel sobre la marca',
          extension: '.jpg',
        },
        {
          url: 'happy/happy-3',
          alt: 'Diseño para los envases de las papas',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'DIES',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Logotipo creado en 2018, reticulado áureo, es un logotipo de trazo limpio y estilizado, se buscó tener un logotipo de fácil recordación y fácil aplicación. Dentro de sus aplicaciones se consideró un buen funcionamiento del logo en positivo y negativo.</p>
      `,
      images: [
        {
          url: 'dies/logo-web',
          alt: 'Diseño del logo DIES',
          extension: '.jpg',
        },
        {
          url: 'dies/pasto',
          alt: 'Diseño sobre un pasto para espacios deportivos',
          extension: '.jpg',
        },
        {
          url: 'dies/tarjetas',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Emily Kings',
      name2: 'EmilyKings',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Diseño de logo desde cero con reticulación y manual de uso, donde se unifican diferentes servicios manteniendo la identidad de la marca.</p>
      `,
      images: [
        {
          url: 'emily/emily1',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
        {
          url: 'emily/emily2',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
        {
          url: 'emily/emily3',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
        {
          url: 'emily/emily4',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default imagenCorporativa
