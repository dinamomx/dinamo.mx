/** @type {import('types').Category} */
const disenoWeb = {
  title: 'Diseño Web',
  description:
    'Nos gusta resolver con HTML5, CSS, Javascript, Typescript, Node.js, Git. Sin embargo, siempre estamos buscando nuevas herramientas para optimizar resultados.',
  og_image: {
    src: '/files/images/category/web-design/infinity-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Diseño Web',
  },
  binder: '/category/web-design/',
  urlPrev: 'estrategia-integral',
  urlNext: 'social-media',
  titlePrev: 'Estrategia Integral',
  titleNext: 'Social Media',
  url: '/portafolio/category/diseno-web',
  intro:
    'Mezclamos diferentes disciplinas, experiencia y mucha pasión teniendo como base: UX, UI, usabilidad, navegabilidad, diseño gráfico, diseño editorial, composición y reticulación',
  subtitle:
    'Analizamos cada proyecto para proponer la tecnología correcta, siempre tomando en cuenta estándares de calidad, entre nuestras herramientas favoritas se encuentran vue.js y directus cms.',
  works: [
    {
      position: 'center',
      imgVertical: false,
      name: 'Gestopago',
      project: /* html */ `
        <h5>Fecha: 2020</h5> 
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Creamos una nueva propuesta de diseño aplicable para versiones móviles y navegación en equipos desktop.
              </span>
            </div>
          </li>
        
        </ul>
      `,
      images: [
        {
          url: 'gestopago/gesto1',
          alt: 'Gestopago 1',
          extension: '.mp4',
       
        },
        {
          url: 'gestopago/gesto2',
          alt: 'Gesto pago 2',
          extension: '.mp4',
    
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Yago de Marta',
      name2: 'YagodeMarta',
      project: /* html */ `
        <h5>Fecha: 2018</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Renderizado del lado del servidor.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Animaciones optimizadas con tweenMax.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'yago/web',
          alt: 'Cuidamos el renderizado del lado del servidor',
          extension: '.mp4',
       
        },
        {
          url: 'yago/yago-tablet',
          alt: 'Presentando sitio web en tablet',
          extension: '.mp4',
      
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'USMEF',
      project: /* html */ `
        <h5>Fecha: 2018-2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Re diseño de 3 sitios web, orientados a distintos usuarios.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Incremento notable en velocidad de carga.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'usmef/cs1',
          alt: 'Imagen del sitio web de USPORK en la seccion de recetas',
          extension: '.jpg',
        },
        {
          url: 'usmef/cs2',
          alt:
            'Imagen del sitio web de USPORK en la seccion de cortes de carne',
          extension: '.jpg',
        },
        {
          url: 'usmef/um1',
          alt: 'Imagen del sitio web de USMEF',
          extension: '.jpg',
        },
        {
          url: 'usmef/um2',
          alt: 'Imagen del sitio web de USMEF seccion del consumidor',
          extension: '.jpg',
        },
        {
          url: 'usmef/um3',
          alt: 'Imagen del sitio web de USMEF seccion de industria',
          extension: '.jpg',
        },
        {
          url: 'usmef/um4',
          alt: 'Imagen del sitio web de USMEF seccion de nosotros',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Toyota Querétaro',
      name2: 'ToyotaQueretaro',
      project: /* html */ `
        <h5>Fecha: 2019</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Actualización de contenido mediante CMS -Conexión con API de maxipublica.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'toyota/1',
          alt: 'Imagen dsesentacion del sitio web para toyota',
          extension: '.jpg',
        },
        {
          url: 'toyota/2',
          alt: 'Imagen de catalogo de autos',
          extension: '.jpg',
        },
        {
          url: 'toyota/3',
          alt: 'Imagen de las caracteristicas de un auto',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Infinity',
      project: /* html */ `
        <h5>Fecha: 2019</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Animaciones web avanzadas.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Optimización para obtención de leads.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Vista 360° de interior de auto.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Organización de grandes cantidades de información.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'infinity/infinity-dinamo',
          poster: 'infinity/infinity-dinamo.jpg',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.mp4',
        },
        {
          url: 'infinity/inf',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
        {
          url: 'infinity/inf2',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
        {
          url: 'infinity/infinity-360-ae',
          poster: 'infinity/infinity-play.jpg',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.mp4',
        },
        {
          url: 'infinity/inf3',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
        {
          url: 'infinity/inf4',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Andrés Siegel',
      name2: 'AndresSiegel',
      project: /* html */ `
        <h5>Fecha: 2019</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Sitio optimizado para e-commerce.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Implementación en el sitio de recorrido virtual 360.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'andres/recorrido',
          alt: 'Video de recorrido 360 para galeria de arte',
          extension: '.mp4',
          poster: 'andres/recorrido.jpg',
        },
        {
          url: 'andres/ipad',
          alt: 'Presentacion de los servicios del sitio web de Andrés',
          extension: '.jpg',
        },
        {
          url: 'andres/web',
          alt: 'Imagen de las categorias del sitio web de Andrés',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Korefusion',
      project: /* html */ `
        <h5>Fecha: 2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Sitio administrable con Wordpress cumpliendo los estándares de Google para su óptimo funcionamiento.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Korefusion pertenece a la industria fintech (tecnología financiera para empresas).</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'kore/korefusion1',
          alt: 'Imagen de muestra del trabajo en KoreFusion',
          extension: '.jpg',
        },
        {
          url: 'kore/korefusion2',
          alt: 'Imagen de muestra del trabajo en KoreFusion',
          extension: '.jpg',
        },
        {
          url: 'kore/korefusion3',
          alt: 'Imagen de muestra del trabajo en KoreFusion',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'CADEFI',
      project: /* html */ `
        <h5>Fecha: 2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Actualización del sistema web que permite una mejor interacción del usuario con el sitio para la capacitación de contadores.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Manejo de gran información para la digitalización de todas las Leyes Fiscales del país, donde el contenido se puede referenciar.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'cadefi/cadefi',
          alt: 'Video de presentación CADEFI',
          extension: '.mp4',
          poster: 'cadefi/Cadef3',
        },
        {
          url: 'cadefi/cadefi1',
          alt: 'Imagen de muestra del trabajo en CADEFI',
          extension: '.jpg',
        },
        {
          url: 'cadefi/cadefi2',
          alt: 'Imagen de muestra del trabajo en CADEFI',
          extension: '.jpg',
        },
        {
          url: 'cadefi/cadefi3',
          alt: 'Imagen de muestra del trabajo en CADEFI',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default disenoWeb
