/** @type {import('types').Category} */
const disenoGrafico = {
  title: 'Diseño Gráfico',
  description: '',
  og_image: {
    src: '/files/images/category/category/graphic-design/city-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Diseño Gráfico',
  },
  binder: '/category/graphic-design/',
  urlPrev: 'social-media',
  urlNext: 'imagen-corporativa',
  titlePrev: 'Social Media',
  titleNext: 'Imagen Corporativa',
  url: '/portafolio/category/diseno-grafico',
  intro:
    'Tomamos como base todos estos aspectos para nuestro diseño gráfico: contexto, composición, reticulación, estética, tamaños de letra, ritmo de lectura, color, fotografía, entre otros.',
  works: [
    {
      position: 'right',
      imgVertical: false,
      name: 'Fundación Japón',
      name2: 'FundacionJapon',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Desarrollamos diseño de personajes, trabajo de guión y animación de video para video tutoriales de la Fundación Japón en México.</p>
      `,
      images: [
        {
          url: 'fundajapon/FJ',
          alt: 'Fundación Japón',
          extension: '.mp4',
        },
        {
          url: 'fundajapon/hana',
          alt: 'Hana',
          extension: '.mp4',
        },
        {
          url: 'fundajapon/nano',
          alt: 'Nano',
          extension: '.mp4',
        },
        {
          url: 'fundajapon/tomy',
          alt: 'Tomy',
          extension: '.mp4',
        },
      
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Royal Skin',
      name2: 'RoyalSkin',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Renovamos el diseño de la página de Facebook a partir de la creación de un nuevo template aplicable dentro de formatos digitales.</p>
      `,
      images: [
        {
          url: 'royal/royal_1',
          alt: 'Imagen de anuncio Royal Skin',
          extension: '.jpg',
        },
        {
          url: 'royal/royal-2',
          alt: 'Imagen de redes sociales royal skin',
          extension: '.jpg',
        },
        {
          url: 'royal/royal-3',
          alt: 'Imagen sobre royal skin en tablet',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'City Express',
      name2: 'CityExpress',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Hay proyectos que exigen tiempos de entrega cortos. La solución: procesos definidos, trabajo en equipo y creatividad.</p>
      `,
      images: [
        {
          url: 'city/city-express-plus',
          alt: 'Imagen de anuncio sobre viajes',
          extension: '.jpg',
        },
        {
          url: 'city/city-2',
          alt: 'Imagen de carteles publicitarios en pared',
          extension: '.jpg',
        },
        {
          url: 'city/city-3',
          alt: 'Imagen sobre suites en San Luis Potosi',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Clínica Colorrectal',
      name2: 'ClínicaColorrectal',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>El objetivo es realizar propuestras gráficas utilizando la ilustración para obtener un resultado menos explícito, pero que transmita la información necesaria y precisa.</p>
      `,
      images: [
        {
          url: 'dra-flor/cupon',
          alt: 'Imagen del diseño de cupones',
          extension: '.jpg',
        },
        {
          url: 'dra-flor/dra-flor',
          alt: 'Imagen de diseños para post en redes sociales',
          extension: '.jpg',
        },
        {
          url: 'dra-flor/dra-flow',
          alt: 'Imagen del diseño web en una tablet',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'USMEF',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>A través de una estrategia 360º combinamos experiencia y trabajo en equipo para generar resultados en diferentes áreas como diseño gráfico, social media, desarrollo web y animación 2D.
      </p>
      `,
      images: [
        {
          url: 'usmef/USMEF',
          alt: 'Imagen del proceso de diseño de una cartel',
          extension: '.mp4',
        },
        {
          url: 'usmef/usmef-2',
          alt: 'Imagen del proceso de diseño de una cartel',
          extension: '.jpg',
        },
        {
          url: 'usmef/usmef',
          alt: 'Imegen del trabajo de diseño para una receta',
          extension: '.jpg',
        },
        {
          url: 'usmef/usmef-tablet',
          alt: 'Diseño sobre las partes de la carne de res',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Inmujeres',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>¿Cómo hacer más visual y cercano un tema complejo como la igualdad de género? Por las características del público al cuál iba dirigido, el diseño de los materiales debía ser amigable, claro y funcional. El logotipo y las ilustraciones aportan información al usuario.</p>
      `,
      images: [
        {
          url: 'inmujeres/INMUJERES',
          poster: 'inmujeres/inmujeres-telefono.jpg',
          alt: 'Video del diseño web para movil',
          extension: '.mp4',
        },
        {
          url: 'inmujeres/inmujeres-telefono',
          poster: 'inmujeres/inmujeres-telefono.jpg',
          alt: 'Video del diseño web para movil',
          extension: '.mp4',
        },
        {
          url: 'inmujeres/inmujeres-personajes',
          alt: 'Diseño de los personajes que se usaron en Inmujeres',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'KIA Del Valle',
      name2: 'KIADelValle',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Diseños funcionales que reflejan la esencia y estilo de la marca.</p>
      `,
      images: [
        {
          url: 'kia/kia-01',
          alt:
            'Imagen de una tablet mostrando el trabajo de Diseño en facebook',
          extension: '.jpg',
        },
        {
          url: 'kia/kia-02',
          alt: 'Celular mostrando diseño promocional de un auto',
          extension: '.jpg',
        },
        {
          url: 'kia/kia-03',
          alt: 'Tablet mostrando diseño promocional del sedona',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'SEAT Furia Motors',
      name2: 'SEATFuriaMotors',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Diseño de materiales óptimos para redes sociales y campañas en Facebook y Google Ads. Manteniendo la identidad de la marca y respetando la escencia de cada modelo SEAT.</p>
      `,
      images: [
        {
          url: 'seat/seat-01',
          alt: 'Macbook Pro presentando el diseño que habla del Seat Leon ST',
          extension: '.jpg',
        },
        {
          url: 'seat/seat-02',
          alt: 'Celular mostrando post del Seat en instagram',
          extension: '.jpg',
        },
        {
          url: 'seat/seat-03',
          alt: 'Tablet mostrando diseño promocional de camioneta Seat',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default disenoGrafico
