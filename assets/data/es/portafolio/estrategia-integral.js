/** @type {import('types').Category} */
const estrategiaIntegral = {
  title: 'Estrategia Integral',
  description: '',
  og_image: {
    src: '/files/images/category/estrategia-integral/instagram-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Estrategia Integral',
  },
  binder: '/category/estrategia-integral/',
  urlPrev: 'fotografia',
  urlNext: 'diseno-web',
  titlePrev: 'Fotografía',
  titleNext: 'Diseño Web',
  url: '/portafolio/category/estrategia-integral',
  intro:
    'Partimos del  trabajo en equipo y la suma de esfuerzos. A través de la conjugación de diferentes áreas especializadas de acuerdo al objetivo de cada cliente, con la finalidad de obtener mejores resultados.',
  subtitle:
    'El tener distintas áreas involucradas en el proyecto nos permite disminuir costos de proveedor, mejorar tiempos de entrega y mantener una comunicación más precisa y directa.',
  works: [
    {
      imgVertical: false,
      name: 'Summer Hills',
      name2: 'SummerHills',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Desde el 2017 la estrategia del Kindergarten y Nursery House “Summer Hills”, tiene como objetivo el aumento de su matrícula mediante diferentes acciones en conjunto, como campañas publicitarias en redes sociales y Google Ads, combinadas con una generación de contenido especializado y el desarrollo de sitio bajo la misma línea de diseño.</p>`,
      images: [
        {
          url: 'summer/facebook',
          alt: 'Imagen de tablet mostrando redes sociales (facebook)',
          extension: '.jpg',
     
        },
        {
          url: 'summer/impreso',
          alt: 'Imagen de cartel impreso sobre el ajedez',
          extension: '.jpg',
        },
        {
          url: 'summer/instagram',
          alt: 'Imagen de celular mostrando post de instagram',
          extension: '.jpg',
        },
        {
          url: 'summer/web',
          alt: 'Imagen del trabajo en pagina web',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'USMEF',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Trabajamos en este proyecto desde hace dos años, basados en una estrategia integral orientada a generar una comunidad activa de usuarios en redes sociales, a través de la generación de contenido como dinámicas, materiales informativos e inversión de publicidad cuya conjugación se ha reflejado en un notable crecimiento de sus redes sociales.</p>
      <p>Además a partir del desarrollo de tres sitios web, esta organización ha logrado ofrecer información valiosa y relevante a los usuarios, dando como resultado un incremento de visitas y sesiones en los sitios.</p>`,
      images: [
        {
          url: 'usmef/USMEF',
          alt: 'Imagen del proceso de diseño de una cartel',
          extension: '.mp4',
        },
        {
          url: 'usmef/facebook',
          poster: 'usmef/facebook.jpg',
          alt: 'Video de celular mostrando post de facebook',
          extension: '.mp4',
        },
        {
          url: 'usmef/instagram',
          alt: 'Imagen de celular mostrando post de instagram',
          extension: '.jpg',
        },
        {
          url: 'usmef/linkedin',
          alt: 'Imagen de laptop mostrando nuestro trabajo en linkedin',
          extension: '.jpg',
        },
        {
          url: 'usmef/web',
          alt: 'Imagen del trabajo en pagina web',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default estrategiaIntegral
