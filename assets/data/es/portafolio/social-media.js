/** @type {import('types').Category} */
const socialMedia = {
  title: 'Social Media',
  description: '',
  og_image: {
    src: '/files/images/category/category/social-media/facebook-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Social Media',
  },
  url: '/portafolio/category/social-media',
  binder: '/category/social-media/',
  urlPrev: 'diseno-web',
  urlNext: 'diseno-grafico',
  titlePrev: 'Diseño Web',
  titleNext: 'Diseño Gráfico',
  intro:
    'Generamos una comunicación clara y precisa en redes sociales, pensando en el usuario final y en los objetivos de la marca para potenciar su posicionamiento, ventas digitales, comunidad y asistencia a eventos.',
  works: [
    {
      imgVertical: false,
      name: 'Clínica Colorrectal',
      name2: 'ClinicaColorrectal',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
        <p>
          Un gran reto, cómo hablar de forma asertiva de un tema tabú socialmente.
          Más de un año realizando contenido para su comunicación en redes sociales. Los números avalan la calidad de nuestro trabajo.
        </p>
        <h5>
          Crecimiento:
        </h5>
        <p>
          Alcance en Facebook de 1k a 30 k usuarios en promedio (por publicación).<br />
          Incremento de citas agendadas en un 30%.
        </p>
        `,
      images: [
        {
          url: 'dra-flor/clinica-colorrectal',
          poster: 'dra-flor/clinica-colorrectal.jpg',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.mp4',
        },
        {
          url: 'dra-flor/facebook',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
        {
          url: 'dra-flor/instagram',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Mitsubishi',
      project: /* html */ `
        <p><b>Desarrollo del proyecto:</b></p>
        <p>Crecimiento de la página y posicionamiento de la marca, mediante generación de contenido y campañas publicitarias con buen rendimiento, además de una notable mejora en ventas digitales.</p>
        <h5>Campaña Promedio:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Leads generados:</span> <strong>100</strong>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Costo por lead promedio:</span> <strong>$20.00</strong>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Ventas promedio mensuales:</span> <strong>2-4</strong>
            </div>
          </li>
        </ul>
        `,
      images: [
        {
          url: 'mitsubishi/mitsu',
          poster: 'mitsubishi/mitsu.jpg',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.mp4',
        },
        {
          url: 'mitsubishi/facebook',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
        {
          url: 'mitsubishi/instagram',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'FEGER',
      project: /* html */ `
      <p><b>Desarrollo del proyecto:</b></p>
      <p>Diseño de catálogo de presentación corporativa para clientes, campañas en Google y Facebook, con generación de clientes potenciales y ventas cerradas.</p>
      `,
      images: [
        {
          url: 'feger/feger-facebook1',
          alt: 'Diseño del trabajo hecho para FEGER',
          extension: '.jpg',
        },
        {
          url: 'feger/feger-facebook2',
          alt: 'Diseño del trabajo hecho para FEGER',
          extension: '.jpg',
        },
        {
          url: 'feger/feger-facebook3',
          alt: 'Diseño del trabajo hecho para FEGER ',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default socialMedia
