/** @type {import('types').Category} */
const fotografia = {
  title: 'Fotografía',
  description:
    'Entra a nuestra galería y conoce más sobre nuestro trabajo de fotografía de producto y book. Participamos en diferentes tipos de proyectos. Cuidamos todos los detalles en cada sesión.',
  og_image: {
    src: '/files/images/category/fotografia/fotografia-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Fotografia',
  },
  binder: '/category/fotografia/',
  urlPrev: 'imagen-corporativa',
  urlNext: 'estrategia-integral',
  titlePrev: 'Corporate Branding',
  titleNext: 'Comprehensive Strategy',
  url: '/portafolio/category/fotografia',
  intro:
    'Over 10 years of experience in product photography. Check out our work.',
  works: [
    {
      imgVertical: false,
      name: 'Barra Norte',
      name2: 'BarraNorte',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>The most important thing is to highlight the difference of each dish in a simple and appetizing way. Dig in!</p>
      `,
      images: [
        {
          url: 'barra/tirado-negro',
          alt: 'Foto emplatado tirado negro',
          extension: '.jpg',
        },
        {
          url: 'barra/mole-negro',
          alt: 'Foto emplatado mole negro',
          extension: '.jpg',
        },
        {
          url: 'barra/caldo',
          alt: 'Foto emplatado de verduras bañadas en caldo',
          extension: '.jpg',
        },
        {
          url: 'barra/robin-hood',
          alt: 'Foto bebida Rogin Hood',
          extension: '.jpg',
        },
        {
          url: 'barra/tulum',
          alt: 'Foto bebida Tulum',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Cupcake Love',
      name2: 'CupcakeLove',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>All you need is Cupcake Love. Their specialty is cupcakes, ours is photography!</p>
      `,
      images: [
        {
          url: 'cupcake/cup-1',
          alt: 'Caja con 6 cupcakes',
          extension: '.jpg',
        },
        {
          url: 'cupcake/cup-2',
          alt: 'Foto de cupcake con cubierta de chocolate',
          extension: '.jpg',
        },
        {
          url: 'cupcake/cup-3',
          alt: 'Foto de cupcake con cubierta de chocolate',
          extension: '.jpg',
        },
        {
          url: 'cupcake/cup-4',
          alt: 'Foto emplatado de sandwitch y bebida',
          extension: '.jpg',
        },
        {
          url: 'cupcake/cup-3',
          alt: 'Foto de cupcake con adorno',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: true,
      name: 'Book Fotográfico',
      name2: 'BookFotografico',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>All you need is Cupcake Love. Their specialty is cupcakes, ours is photography!</p>
      `,
      images: [
        {
          url: 'book/modelo',
          alt: 'Foto rostro de perfil de modelo',
          extension: '.jpg',
        },
        {
          url: 'book/modelo-2',
          alt: 'Foto de cuerpo completo de modelo',
          extension: '.jpg',
        },
        {
          url: 'book/modelo-3',
          alt: 'Foto de medio cuerpo de modelo',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Darila',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Artwork 100% made by Mexicans.  Photographs 100% taken by Mexicans.</p>
      `,
      images: [
        {
          url: 'darila/jarra-metal',
          alt: 'Foto de jarra de metal',
          extension: '.jpg',
        },
        {
          url: 'darila/charola-metal',
          alt: 'Foto de charola de metal',
          extension: '.jpg',
        },
        {
          url: 'darila/plato-decorativo',
          alt: 'Foto de plato decorativo',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Grupo Bimbo / Haz Pan',
      name2: 'GrupoBimboHazPan',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>The flavor, variety and tradition of Mexico’s iconic bakery are accentuated by this delectable shoot.</p>
      `,
      images: [
        {
          url: 'bimbo/pan-chocolate',
          alt: 'Foto de pan de chocolate',
          extension: '.jpg',
        },
        {
          url: 'bimbo/galleta',
          alt: 'Foto de una galleta cubierta de chocolate',
          extension: '.jpg',
        },
        {
          url: 'bimbo/base-danes',
          alt: 'Foto de base danes, pan dulce',
          extension: '.jpg',
        },
        {
          url: 'bimbo/pastel',
          alt: 'Foto de una rebanada de pastel de chocolate',
          extension: '.jpg',
        },
        {
          url: 'bimbo/rebanada',
          alt: 'Foto de varias rebanadas de pan con cascaras de naranja',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Happy Frits',
      name2: 'HappyFrits',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>French fries have never looked so exquisite! With a splash of fun and composition, we bring a smile to the face of those who cozy up to this delicious client.</p>
      `,
      images: [
        {
          url: 'happy/happy-frits',
          alt: 'Foto de caja de papas gajo derramadas',
          extension: '.jpg',
        },
        {
          url: 'happy/happy-frits-2',
          alt: 'Foto de tres tipos de papas',
          extension: '.jpg',
        },
        {
          url: 'happy/happy-frits-3',
          alt: 'Foto de papas gajo',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'La Balance',
      name2: 'LaBalance',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Smell, flavor, and color all in one photographic session. We’re out to convey the aesthetic goodness of your products. Have we made your mouth water yet?</p>
      `,
      images: [
        {
          url: 'balance/la-balance',
          alt: 'Foto de 4 postres',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default fotografia
