/** @type {import('types').Category} */
const imagenCorporativa = {
  title: 'Imagen Corporativa',
  description: '',
  og_image: {
    src: '/files/images/category/imagen-corporativa/crea-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Imagen Corporativa',
  },
  binder: '/category/imagen-corporativa/',
  urlPrev: 'diseno-grafico',
  urlNext: 'fotografia',
  titlePrev: 'Graphic Design',
  url: '/portafolio/category/imagen-corporativa',
  titleNext: 'Photography',
  intro:
    'We take a logo’s performance into account as it pertains to the client’s needs, breaking away from brand building in order to fulfill both objectives.',
  works: [
    {
      position: 'center',
      imgVertical: false,
      name: 'Milkiin',
      project: /* html */ `
      <p><b>Project Development:</b></p>
      <p>We designed the brand identity  that can be used for different applications in digital and printed media. 

      We determined what it was that accurately represented our client’s needs, which was key in creating a functional visual identity for the clinic.
      
      </p>
      `,
      images: [
        {
          url: 'milkin/milkin-1',
          alt: 'Rediseño de la imagen corporativa',
          extension: '.jpg',
        },
        {
          url: 'milkin/milkin-2',
          alt: 'Diseño de hojas de pesentacion',
          extension: '.jpg',
        },
        {
          url: 'milkin/milkin-3',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
        {
          url: 'milkin/milkin-4',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
        {
          url: 'milkin/milkin-5',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'CREA',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Our graphic proposal on this project comes close to a minimalist aesthetic, a referential nod to the typographic alphabet of Bruno Munari. The golden ratio is our guiding light.</p>
      `,
      images: [
        {
          url: 'crea/crea',
          alt: 'Rediseño de la imagen corporativa',
          extension: '.jpg',
        },
        {
          url: 'crea/crea-2',
          alt: 'Diseño de hojas de pesentacion',
          extension: '.jpg',
        },
        {
          url: 'crea/crea-3',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Happy Frits',
      name2: 'HappyFrits',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Emojis became more relevant than ever for visual communication in 2014. Within this context Happy Fritz’s logo is a salute to emoji culture and geared toward a young audience. Furthermore, we included the characteristics of the product as basis for a stylized, typographic proposal.</p>
      `,
      images: [
        {
          url: 'happy/happy',
          alt: 'Rediseño de la imagen corporativa',
          extension: '.jpg',
        },
        {
          url: 'happy/happy-2',
          alt: 'Diseño en papel sobre la marca',
          extension: '.jpg',
        },
        {
          url: 'happy/happy-3',
          alt: 'Diseño para los envases de las papas',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'DIES',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Created stylishly from clean strokes, the aerial lattice logo was created in 2018. Our client was in search of an easy-to-recognize and easily applicable logo. Within their applications the logo performs well in both positive and negative.</p>
      `,
      images: [
        {
          url: 'dies/logo-web',
          alt: 'Diseño del logo DIES',
          extension: '.jpg',
        },
        {
          url: 'dies/pasto',
          alt: 'Diseño sobre un pasto para espacios deportivos',
          extension: '.jpg',
        },
        {
          url: 'dies/tarjetas',
          alt: 'Diseño de tarjetas de pesentacion',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Emily Kings',
      name2: 'EmilyKings',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Logo design from scratch with reticulation and user manual, where different services come together while maintaining brand recognition.</p>
      `,
      images: [
        {
          url: 'emily/emily1',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
        {
          url: 'emily/emily2',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
        {
          url: 'emily/emily3',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
        {
          url: 'emily/emily4',
          alt: 'Diseño del trabajo hecho para Emily Kings',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default imagenCorporativa
