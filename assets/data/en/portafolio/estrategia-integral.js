/** @type {import('types').Category} */
const estrategiaIntegral = {
  title: 'Estrategia Integral',
  description: '',
  og_image: {
    src: '/files/images/category/estrategia-integral/instagram-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Estrategia Integral',
  },
  binder: '/category/estrategia-integral/',
  urlPrev: 'fotografia',
  urlNext: 'diseno-web',
  titlePrev: 'Photography',
  titleNext: 'Web Design',
  url: '/portafolio/category/estrategia-integral',
  intro:
    'Our jumping-off point is teamwork and the sum of our efforts. Unified by the client’s vision, different areas of artistic expertise come together with the end goal of creating the best possible results.',
  subtitle:
    'Having distinct areas of our team involved in the project allows us to lower costs, improve turn-around times, and maintain a more precise and straightforward communication style.',
  works: [
    {
      imgVertical: false,
      name: 'Summer Hills',
      name2: 'SummerHills',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Summer Hills Kindergarten And Nursery House has been focused on increased student enrollment increases since 2017. Using a variety of tactics, including publicity campaigns on social media and Google Ads, Summer Hills has used specialized and stylistically consistent design content to get their message out.</p>`,
      images: [
        {
          url: 'summer/facebook',
          alt: 'Imagen de tablet mostrando redes sociales (facebook)',
          extension: '.jpg',
        },
        {
          url: 'summer/impreso',
          alt: 'Imagen de cartel impreso sobre el ajedez',
          extension: '.jpg',
        },
        {
          url: 'summer/instagram',
          alt: 'Imagen de celular mostrando post de instagram',
          extension: '.jpg',
        },
        {
          url: 'summer/web',
          alt: 'Imagen del trabajo en pagina web',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'USMEF',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>We’ve been working on this project for the past two years. Its based on a comprehensive strategy of creating an active community of social media patrons through content creation like dynamics, informative subject matter, and investment in advertising. The result has been stellar growth in their social networks.</p>
      <p>Besides the creation of three websites, USMEF has been able to offer valuable and relevant information to its users, seen in both increased hits and website sessions.</p>`,
      images: [
        {
          url: 'usmef/facebook',
          poster: 'usmef/facebook.jpg',
          alt: 'Video de celular mostrando post de facebook',
          extension: '.mp4',
        },
        {
          url: 'usmef/instagram',
          alt: 'Imagen de celular mostrando post de instagram',
          extension: '.jpg',
        },
        {
          url: 'usmef/linkedin',
          alt: 'Imagen de laptop mostrando nuestro trabajo en linkedin',
          extension: '.jpg',
        },
        {
          url: 'usmef/web',
          alt: 'Imagen del trabajo en pagina web',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default estrategiaIntegral
