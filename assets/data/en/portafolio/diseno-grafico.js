/** @type {import('types').Category} */
const disenoGrafico = {
  title: 'EN Diseño Gráfico',
  description: '',
  og_image: {
    src: '/files/images/category/category/graphic-design/city-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Diseño Gráfico',
  },
  binder: '/category/graphic-design/',
  urlPrev: 'social-media',
  urlNext: 'imagen-corporativa',
  titlePrev: 'Social Media',
  titleNext: 'Corporate Branding',
  url: '/portafolio/category/diseno-grafico',
  intro:
    'All aspects of design knowledge come into play with the work we do: context, composition, cross-linking, aesthetic, letter size, reading flow, color, photography, the list goes on.',
  works: [
    {
      position: 'right',
      imgVertical: false,
      name: 'Fundación Japón',
      name2: 'FundacionJapon',
      project: /* html */ `
      <p><b>Project Development:</b></p>
      <p>We did  character design, script writing and 2D animation for video tutorials for the Japan Foundation in Mexico. </p>
      `,
      images: [
        {
          url: 'fundajapon/FJ',
          alt: 'Fundación Japón',
          extension: '.mp4',
        },
        {
          url: 'fundajapon/hana',
          alt: 'Hana',
          extension: '.mp4',
        },
        {
          url: 'fundajapon/nano',
          alt: 'Nano',
          extension: '.mp4',
        },
        {
          url: 'fundajapon/tomy',
          alt: 'Tomy',
          extension: '.mp4',
        },
      
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Royal Skin',
      name2: 'RoyalSkin',
      project: /* html */ `
      <p><b>Project Development:</b></p>
      <p>We renewed the graphic design of the fan page, by creating a totally new template that is functional for different applications. 

      In addition, we created and developed Facebook Ads which led to a surge in the sales of the project.
      </p>
      `,
      images: [
        {
          url: 'royal/royal_1',
          alt: 'Imagen de anuncio Royal Skin',
          extension: '.jpg',
        },
        {
          url: 'royal/royal-2',
          alt: 'Imagen de redes sociales royal skin',
          extension: '.jpg',
        },
        {
          url: 'royal/royal-3',
          alt: 'Imagen sobre royal skin en tablet',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'City Express',
      name2: 'CityExpress',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>There are projects that require short turn-around. The solution: well-defined processes, teamwork and creativity.</p>
      `,
      images: [
        {
          url: 'city/city-express-plus',
          alt: 'Imagen de anuncio sobre viajes',
          extension: '.jpg',
        },
        {
          url: 'city/city-2',
          alt: 'Imagen de carteles publicitarios en pared',
          extension: '.jpg',
        },
        {
          url: 'city/city-3',
          alt: 'Imagen sobre suites en San Luis Potosi',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Clínica Colorrectal',
      name2: 'ClinicaColorrectal',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>The goal is to achieve design proposals through the use of illustration with subtle results, while also conveying the necessary information in a sharp manner.</p>
      `,
      images: [
        {
          url: 'dra-flor/cupon',
          alt: 'Imagen del diseño de cupones',
          extension: '.jpg',
        },
        {
          url: 'dra-flor/dra-flor',
          alt: 'Imagen de diseños para post en redes sociales',
          extension: '.jpg',
        },
        {
          url: 'dra-flor/dra-flow',
          alt: 'Imagen del diseño web en una tablet',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'USMEF',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Through a 360º estrategy we focus on communicating a consistent message through all the marketing mix elements each project requires. 

      Currently  we do graphic design, social media management, web development and motion graphics.
      </p>
      `,
      images: [
        {
          url: 'usmef/USMEF',
          alt: 'Imagen del proceso de diseño de una cartel',
          extension: '.mp4',
        },
        {
          url: 'usmef/usmef-2',
          alt: 'Imagen del proceso de diseño de una cartel',
          extension: '.jpg',
        },
        {
          url: 'usmef/usmef',
          alt: 'Imegen del trabajo de diseño para una receta',
          extension: '.jpg',
        },
        {
          url: 'usmef/usmef-tablet',
          alt: 'Diseño sobre las partes de la carne de res',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Inmujeres',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>How can complex subject matter like gender equality be made more relatable and visually amplified? By giving consideration to the public that its directed toward, it’s important that the design be friendly, clear and functional. The logo and illustration must convey the information to the viewer.</p>
      `,
      images: [
        {
          url: 'inmujeres/inmujeres-telefono',
          poster: 'inmujeres/inmujeres-telefono.jpg',
          alt: 'Video del diseño web para movil',
          extension: '.mp4',
        },
        {
          url: 'inmujeres/inmujeres-personajes',
          alt: 'Diseño de los personajes que se usaron en Inmujeres',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'KIA Del Valle',
      name2: 'KIADelValle',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Functional design that reflects the brand’s essence and style.</p>
      `,
      images: [
        {
          url: 'kia/kia-01',
          alt:
            'Imagen de una tablet mostrando el trabajo de Diseño en facebook',
          extension: '.jpg',
        },
        {
          url: 'kia/kia-02',
          alt: 'Celular mostrando diseño promocional de un auto',
          extension: '.jpg',
        },
        {
          url: 'kia/kia-03',
          alt: 'Tablet mostrando diseño promocional del sedona',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'SEAT Furia Motors',
      name2: 'SEATFuriaMotors',
      project: /* html */ `
      <p><b>Project development:</b></p>
      <p>Design of material optimal for social media, as well as Facebook and Google Ads campaigns. Reinforcing brand identity and respect for the essence of each SEAT model is the result we were after.</p>
      `,
      images: [
        {
          url: 'seat/seat-01',
          alt: 'Macbook Pro presentando el diseño que habla del Seat Leon ST',
          extension: '.jpg',
        },
        {
          url: 'seat/seat-02',
          alt: 'Celular mostrando post del Seat en instagram',
          extension: '.jpg',
        },
        {
          url: 'seat/seat-03',
          alt: 'Tablet mostrando diseño promocional de camioneta Seat',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default disenoGrafico
