/** @type {import('types').Category} */
const socialMedia = {
  title: 'Social Media',
  description: '',
  og_image: {
    src: '/files/images/category/category/social-media/facebook-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Social Media',
  },
  url: '/portafolio/category/social-media',
  binder: '/category/social-media/',
  urlPrev: 'diseno-web',
  urlNext: 'diseno-grafico',
  titlePrev: 'Web Design',
  titleNext: 'Graphic Design',
  intro:
    'We design clear and precise communication for social media where the end product’s ability to maximize brand visibility takes precedence, as well as to also maximize online sales, community and event participation.',
  works: [
    {
      imgVertical: false,
      name: 'Clínica Colorrectal',
      name2: 'ClínicaColorrectal',
      project: /* html */ `
      <p><b>Project development::</b></p>
        <p>
        It’s challenging to talk about social taboos,
        so to see that the numbers back up the work that we've done for over a year in creating their social media content let’s us know that we hit the mark with this project.
        </p>
        <h5>
          Growth:
        </h5>
        <p>
        Reaching an average of 1k - 30k users on Facebook. (per publication).<br />
        30% increase in amount of appointments set up.
        </p>
        `,
      images: [
        {
          url: 'dra-flor/clinica-colorrectal',
          poster: 'dra-flor/clinica-colorrectal.jpg',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.mp4',
        },
        {
          url: 'dra-flor/facebook',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
        {
          url: 'dra-flor/instagram',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Mitsubishi',
      project: /* html */ `
        <p><b>Project development::</b></p>
        <p>Webpage growth and brand positioning through content creation and publicity campaigns with stellar results, as well as noticeable increase in online sales.</p>
        <h5>Campaing averages:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Leads generated</span> <strong>100</strong>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Average cost per lead:</span> <strong>$20.00</strong>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Average monthly sales:</span> <strong>2-4</strong>
            </div>
          </li>
        </ul>
        `,
      images: [
        {
          url: 'mitsubishi/mitsu',
          poster: 'mitsubishi/mitsu.jpg',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.mp4',
        },
        {
          url: 'mitsubishi/facebook',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
        {
          url: 'mitsubishi/instagram',
          alt: 'Estrategia de presentar soluciones en redes sociales',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'FEGER',
      project: /* html */ `
      <p><b>Project development::</b></p>
      <p>Design of corporate presentation catalogue for clients, campaigns for Google and Facebook, outreach to numerous potential clients and deals closed.</p>
      `,
      images: [
        {
          url: 'feger/feger-facebook1',
          alt: 'Diseño del trabajo hecho para FEGER',
          extension: '.jpg',
        },
        {
          url: 'feger/feger-facebook2',
          alt: 'Diseño del trabajo hecho para FEGER',
          extension: '.jpg',
        },
        {
          url: 'feger/feger-facebook3',
          alt: 'Diseño del trabajo hecho para FEGER ',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default socialMedia
