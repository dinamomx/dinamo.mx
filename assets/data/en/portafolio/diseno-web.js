/** @type {import('types').Category} */
const disenoWeb = {
  title: 'EN Diseño Web',
  description:
    'Nos gusta resolver con HTML5, CSS, Javascript, Typescript, Node.js, Git. Sin embargo, siempre estamos buscando nuevas herramientas para optimizar resultados.',
  og_image: {
    src: '/files/images/category/web-design/infinity-og.jpg',
    width: '1200',
    height: '630',
    alt: 'Imagen del trabajo que hacemos de Diseño Web',
  },
  binder: '/category/web-design/',
  urlPrev: 'estrategia-integral',
  urlNext: 'social-media',
  titlePrev: 'Comprehensive Strategy',
  titleNext: 'Social Media',
  url: '/portafolio/category/diseno-web',
  intro:
    'We blend different disciplines, experience and passion and have the following tools at our disposal: UX, UI, usability, navigability, graphic design, editorial design, composition and cross-linking.',
  subtitle:
    'We analyze each project on an individual basis in order to propose the best possible technology with quality control standards at the forefront of the work we do. Some of our favorite tools include vue.js and directus cms.',
  works: [
    {
      position: 'center',
      imgVertical: false,
      name: 'Gestopago',
      project: /* html */ `
        <h5>Fecha: 2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">We created a new proposal for the web page through a new design that applies for mobile and desktop navigation.

              </span>
            </div>
          </li>
        
        </ul>
      `,
      images: [
        {
          url: 'gestopago/gesto1',
          alt: 'Cuidamos el renderizado del lado del servidor',
          extension: '.mp4',
          poster: 'yago/yago-001.jpg',
        },
        {
          url: 'gestopago/gesto2',
          alt: 'Presentando sitio web en tablet',
          extension: '.mp4',
          poster: 'yago/yago-002.jpg',
        },
      ],
    },
    {
      
      imgVertical: false,
      name: 'Yago de Marta',
      name2: 'YagodeMarta',
      project: /* html */ `
        <h5>Date: 2018</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Server-side rendering.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Animations optimized with tweenMax.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'yago/web',
          alt: 'Cuidamos el renderizado del lado del servidor',
          extension: '.mp4',
          poster: 'yago/yago-001.jpg',
        },
        {
          url: 'yago/yago-tablet',
          alt: 'Presentando sitio web en tablet',
          extension: '.mp4',
          poster: 'yago/yago-002.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'USMEF',
      project: /* html */ `
        <h5>Date: 2018-2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Overhaul of three websites with distinct users in mind.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
            <span class="text-brand-naranja font-semibold">Notable increase in upload speed.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'usmef/cs1',
          alt: 'Imagen del sitio web de USPORK en la seccion de recetas',
          extension: '.jpg',
        },
        {
          url: 'usmef/cs2',
          alt:
            'Imagen del sitio web de USPORK en la seccion de cortes de carne',
          extension: '.jpg',
        },
        {
          url: 'usmef/um1',
          alt: 'Imagen del sitio web de USMEF',
          extension: '.jpg',
        },
        {
          url: 'usmef/um2',
          alt: 'Imagen del sitio web de USMEF seccion del consumidor',
          extension: '.jpg',
        },
        {
          url: 'usmef/um3',
          alt: 'Imagen del sitio web de USMEF seccion de industria',
          extension: '.jpg',
        },
        {
          url: 'usmef/um4',
          alt: 'Imagen del sitio web de USMEF seccion de nosotros',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Toyota Querétaro',
      name2: 'ToyotaQueretaro',
      project: /* html */ `
        <h5>Date: 2019</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Updated content through a CMS connection with API from Maxipublica.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'toyota/1',
          alt: 'Imagen dsesentacion del sitio web para toyota',
          extension: '.jpg',
        },
        {
          url: 'toyota/2',
          alt: 'Imagen de catalogo de autos',
          extension: '.jpg',
        },
        {
          url: 'toyota/3',
          alt: 'Imagen de las caracteristicas de un auto',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'Infinity',
      project: /* html */ `
        <h5>Date: 2019</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Advanced web animations</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Optimization for obtaining leads</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">360º virtual viewing of auto interior.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Large-scale organization of information</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'infinity/infinity-dinamo',
          poster: 'infinity/infinity-dinamo.jpg',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.mp4',
        },
        {
          url: 'infinity/inf',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
        {
          url: 'infinity/inf2',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
        {
          url: 'infinity/infinity-360-ae',
          poster: 'infinity/infinity-play.jpg',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.mp4',
        },
        {
          url: 'infinity/inf3',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
        {
          url: 'infinity/inf4',
          alt: 'Presentanddo las animaciones que se trabajaron en el sitio web',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'right',
      imgVertical: false,
      name: 'Andrés Siegel',
      name2: 'AndresSiegel',
      project: /* html */ `
        <h5>Date: 2019</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Website optimized for e-commerce</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Virtual 360º website viewing implementation</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'andres/recorrido',
          alt: 'Video de recorrido 360 para galeria de arte',
          extension: '.mp4',
          poster: 'andres/recorrido.jpg',
        },
        {
          url: 'andres/ipad',
          alt: 'Presentacion de los servicios del sitio web de Andrés',
          extension: '.jpg',
        },
        {
          url: 'andres/web',
          alt: 'Imagen de las categorias del sitio web de Andrés',
          extension: '.jpg',
        },
      ],
    },
    {
      position: 'center',
      imgVertical: false,
      name: 'Korefusion',
      project: /* html */ `
        <h5>Date: 2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Wordpress-manageable website that conforms to Google’s optimal functioning standards.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Korefusion is part of the fintech industry (business-oriented financial technology).</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'kore/korefusion1',
          alt: 'Imagen de muestra del trabajo en KoreFusion',
          extension: '.jpg',
        },
        {
          url: 'kore/korefusion2',
          alt: 'Imagen de muestra del trabajo en KoreFusion',
          extension: '.jpg',
        },
        {
          url: 'kore/korefusion3',
          alt: 'Imagen de muestra del trabajo en KoreFusion',
          extension: '.jpg',
        },
      ],
    },
    {
      imgVertical: false,
      name: 'CADEFI',
      project: /* html */ `
        <h5>Date: 2020</h5>
        <h5>Highlights:</h5>
        <ul class="grid-cols-3 gap-1 mr-0">
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Update of website allowing for a user-friendly experience when doing accountant training.</span>
            </div>
          </li>
          <li class="base-card">
            <div class="base-card__content">
              <span class="text-brand-naranja font-semibold">Handling of important information for the digitalization of all the country’s Fiscal Laws where the content can be referenced.</span>
            </div>
          </li>
        </ul>
      `,
      images: [
        {
          url: 'cadefi/cadefi',
          alt: 'Video de presentación CADEFI',
          extension: '.mp4',
          poster: 'cadefi/Cadef3',
        },
        {
          url: 'cadefi/cadefi1',
          alt: 'Imagen de muestra del trabajo en CADEFI',
          extension: '.jpg',
        },
        {
          url: 'cadefi/cadefi2',
          alt: 'Imagen de muestra del trabajo en CADEFI',
          extension: '.jpg',
        },
        {
          url: 'cadefi/cadefi3',
          alt: 'Imagen de muestra del trabajo en CADEFI',
          extension: '.jpg',
        },
      ],
    },
  ],
}
export default disenoWeb
